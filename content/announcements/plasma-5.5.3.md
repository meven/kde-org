---
title: KDE Ships Plasma 5.5.3, Bugfix Release for January
description: KDE Ships Plasma 5.5.3
date: 2016-01-06
release: plasma-5.5.3
layout: plasma
changelog: plasma-5.5.2-5.5.3-changelog
---

{{<figure src="/announcements/plasma-5.5/plasma-5.5.png" alt="Plasma 5.5 " class="text-center" width="600px" caption="Plasma 5.5">}}

Tuesday, 6 January 2016.

Today KDE releases a bugfix update to Plasma 5, versioned 5.5.3. <a href='https://www.kde.org/announcements/plasma-5.5.0.php'>Plasma 5.5</a> was released in December with many feature refinements and new modules to complete the desktop experience.

This release adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Fix icon hover effect breaking after Dashboard was used. <a href="http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=c35aa318d417b4fcb5b36324635de6b8e76cbaf1">Commit.</a>
- Use Oxygen sound instead of sound from kdelibs4's kde-runtime. <a href="http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=4cac96f7834b63ced12618475afd37644ab9e243">Commit.</a>
- [notifications] Refactor the screen handling code to fix 'Notification Settings wrong default display for notifications and notifaction position'. <a href="http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=0c6b354b7e22297544f1d37608d6fdcd777c4d52">Commit.</a> Code review <a href="https://git.reviewboard.kde.org/r/126408">#126408</a>. See bug <a href="https://bugs.kde.org/353966">#353966</a>. See bug <a href="https://bugs.kde.org/356461">#356461</a>
