---
title: KDE Plasma 5.10.2, Bugfix Release for June
release: plasma-5.10.2
version: 5.10.2
description: KDE Ships 5.10.2
date: 2017-06-13
layout: plasma
changelog: plasma-5.10.1-5.10.2-changelog
---

{{%youtube id="VtdTC2Mh070"%}}

{{<figure src="/announcements/plasma-5.10/plasma-5.10.png" alt="KDE Plasma 5.10 " class="text-center" width="600px" caption="KDE Plasma 5.10">}}

Tuesday, 13 June 2017.

{{% i18n_var "Today KDE releases a %[1]s update to KDE Plasma 5, versioned %[2]s" "Bugfix" "5.10.2." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in %[2]s with many feature refinements and new modules to complete the desktop experience." "5.10" "May" %}}

{{% i18n_var "This release adds a %[1]s worth of new translations and fixes from KDE's contributors.  The bugfixes are typically small but important and include:" "a week's" %}}

- Discover: Make the PackageKit backend more resistant to crashes in PackageKit. <a href="https://commits.kde.org/discover/64c8778d4cd5180dfa13dd75fed808de9271cedc">Commit.</a> Fixes bug <a href="https://bugs.kde.org/376538">#376538</a>
- Plasma Networkmanager Openconnect: make sure the UI fits into the password dialog. <a href="https://commits.kde.org/plasma-nm/285c7ae37a3f6149b866dfb887bcb62ca6ce1046">Commit.</a> Fixes bug <a href="https://bugs.kde.org/380399">#380399</a>
- Include a header for the settings page for better consistency. <a href="https://commits.kde.org/discover/bffd42fbaac59f262f6d61b8a25cbea5abb12701">Commit.</a>

