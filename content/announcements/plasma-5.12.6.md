---
title: KDE Plasma 5.12.6 LTS, Bugfix Release for June
release: plasma-5.12.6
version: 5.12.6 LTS
description: KDE Ships 5.12.6 LTS
date: 2018-06-27
layout: plasma-5.12.5-5.12.6-changelog
---

{{%youtube id="xha6DJ_v1E4"%}}

{{<figure src="/announcements/plasma-5.12/plasma-5.12.png" alt="KDE Plasma 5.12 LTS Beta " class="text-center" width="600px" caption="KDE Plasma 5.12 LTS Beta">}}

Wednesday, 27 June 2018.

{{% i18n_var "Today KDE releases a %[1]s update to KDE Plasma 5, versioned %[2]s" "Bugfix" "5.12.6 LTS." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in %[2]s with many feature refinements and new modules to complete the desktop experience." "5.12" "February" %}}

{{% i18n_var "This release adds a %[1]s worth of new translations and fixes from KDE's contributors.  The bugfixes are typically small but important and include:" "two months" %}}

- Fix avatar picture aliasing and stretching in kickoff. <a href="https://commits.kde.org/plasma-desktop/9da4da7ea7f71dcf6da2b6b263f7a636029c4a25">Commit.</a> Fixes bug <a href="https://bugs.kde.org/369327">#369327</a>. Phabricator Code review <a href="https://phabricator.kde.org/D12469">D12469</a>
- Discover: Fix there being more security updates than total updates in notifier. <a href="https://commits.kde.org/discover/9577e7d1d333d43d899e6eca76bfb309483eb29a">Commit.</a> Fixes bug <a href="https://bugs.kde.org/392056">#392056</a>. Phabricator Code review <a href="https://phabricator.kde.org/D13596">D13596</a>
- Discover Snap support: make it possible to launch installed applications. <a href="https://commits.kde.org/discover/d9d711ff42a67a2df24317af317e6722f89bd822">Commit.</a>
