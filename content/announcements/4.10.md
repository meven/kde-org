---
title: "KDE Software Compilation 4.10"
date: "2013-02-06"
description: KDE Ships Plasma Workspaces, Applications and Platform 4.10
---

{{< figure src="/announcements/announce-4.10/plasma-4.10.png" caption="The KDE Plasma Workspaces 4.10">}}

The KDE Community proudly announces the latest releases of Plasma Workspaces, Applications and Development Platform. With the 4.10 release, the premier collection of Free Software for home and professional use makes incremental improvements to a large number of applications, and offers the latest technologies.

## <a href="./plasma"><img src="/announcements/announce-4.10/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.10" width="64" height="64" /> Plasma Workspaces 4.10 Improve Mobile Device Support and Receive Visual Refinement</a>

Several components of Plasma Workspaces have been ported to the Qt Quick/QML framework. Stability and usability have been improved. A new print manager and Color Management support have been introduced.

## <a href="./applications"><img src="/announcements/announce-4.10/images/applications.png" class="app-icon" alt="The KDE Applications 4.10" width="64" height="64" /> KDE Applications Improve Usability, Performance and Take You to Mars</a>

KDE Applications gained feature enhancements to Kate, KMail and Konsole. KDE-Edu applications saw a complete overhaul of KTouch and many other changes. KDE Games introduced the new Picmi game and improvements throughout.

## <a href="./platform"><img src="/announcements/announce-4.10/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.10"/> KDE Platform 4.10 Opens Up More APIs to Qt Quick</a>

This release makes it easier to contribute to KDE with a Plasma SDK (Software Development Kit), the ability to write Plasma widgets and widget collections in the Qt Markup Language (QML), changes in the libKDEGames library, and new scripting capabilities in window manager KWin.

The KDE Quality team <a href='http://www.sharpley.org.uk/blog/kde-testing'>organized a testing program</a> for this release, assisting developers by identifying legitimate bugs and carefully testing the applications. Thanks to their work, KDE innovation and quality go hand in hand. If you are interested in the Quality Team and their work, check out <a href='http://community.kde.org/Get_Involved/Quality'>The Quality Team wikipage</a>.<br />

## Spread the Word and See What's Happening: Tag as &quot;KDE&quot;

KDE encourages people to spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, twitter, identi.ca. Upload screenshots to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with &quot;KDE&quot;. This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for the 4.10 releases of KDE software.

## Release Parties

As usual, KDE community members organize release parties all around the world. Several have already been scheduled and more will come later. Find <a href='http://community.kde.org/Promo/Events/Release_Parties/4.10'>a list of parties here</a>. Anyone is welcome to join! There will be a combination of interesting company and inspiring conversations as well as food and drinks. It's a great chance to learn more about what is going on in KDE, get involved, or just meet other users and contributors.

We encourage people to organize their own parties. They're fun to host and open for everyone! Check out <a href='http://community.kde.org/Promo/Events/Release_Parties'>tips on how to organize a party</a>.

## About these release announcements

These release announcements were prepared by Devaja Shah, Jos Poortvliet, Carl Symons, Sebastian Kügler and other members of the KDE Promotion Team and the wider KDE community. They cover highlights of the many changes made to KDE software over the past six months.

#### Support KDE

<a href="http://jointhegame.kde.org/"><img src="/announcements/plasma2tp/images/join-the-game.png" class="img-fluid float-left mr-3" alt="Join the Game" /> </a>
KDE e.V.'s new <a href='http://jointhegame.kde.org/'>Supporting Member program</a> is now open. For &euro;25 a quarter you can ensure the international community of KDE continues to grow making world class Free Software.

&nbsp;
