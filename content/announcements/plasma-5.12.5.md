---
title: KDE Plasma 5.12.5, Bugfix Release for May
release: plasma-5.12.5
version: 5.12.5
description: KDE Ships 5.12.5
date: 2018-05-01
layout: plasma
changelog: plasma-5.12.4-5.12.5-changelog
---

{{%youtube id="xha6DJ_v1E4"%}}

{{<figure src="/announcements/plasma-5.12/plasma-5.12.png" alt="KDE Plasma 5.12 LTS Beta " class="text-center" width="600px" caption="KDE Plasma 5.12 LTS Beta">}}

Tuesday, 1 May 2018.

{{% i18n_var "Today KDE releases a %[1]s update to KDE Plasma 5, versioned %[2]s" "Bugfix" "5.12.5." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in %[2]s with many feature refinements and new modules to complete the desktop experience." "5.12" "March" %}}

{{% i18n_var "This release adds a %[1]s worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:" "a month's" %}}

- Discover: improve display of Progress element. <a href="https://commits.kde.org/discover/ff6f7b86a94c25b0a322dd5ad1d86709d54a9cd5">Commit.</a> Fixes bug <a href="https://bugs.kde.org/393323">#393323</a>
- Weather dataengine: fix BBC provider to adapt to change RSS feed. <a href="https://commits.kde.org/plasma-workspace/558a29efc4c9f055799d23ee6c75464e24489e5a">Commit.</a> Fixes bug <a href="https://bugs.kde.org/392510">#392510</a>. Phabricator Code review <a href="https://phabricator.kde.org/D11808">D11808</a>
- Fixed bug that caused power management system to not work on a fresh install. <a href="https://commits.kde.org/powerdevil/be91abe7fc8cc731b57bec4cf2c004c07b0fd79b">Commit.</a> Fixes bug <a href="https://bugs.kde.org/391782">#391782</a>
