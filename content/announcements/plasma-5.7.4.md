---
title: KDE Plasma 5.7.4, bugfix Release for August
release: plasma-5.7.4
description: KDE Ships Plasma 5.7.4.
date: 2016-08-23
layout: plasma
changelog: plasma-5.7.3-5.7.4-changelog
---

{{<figure src="/announcements/plasma-5.7/plasma-5.7.png" alt="KDE Plasma 5.7 " class="text-center mt-4" width="600px" caption="KDE Plasma 5.7">}}

Tuesday, 23 August 2016.

Today KDE releases a bugfix update to KDE Plasma 5, versioned 5.7.4. <a href='https://www.kde.org/announcements/plasma-5.7.0.php'>Plasma 5.7</a> was released in August with many feature refinements and new modules to complete the desktop experience.

This release adds three weeks' worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Fixed dragging items bug in Kickoff
- Mouse settings being applied in kdelibs4 applications
- Improved handling of screen CRTC information
