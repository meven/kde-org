---
title: KDE Plasma 5.7.2, bugfix Release for July
release: plasma-5.7.2
description: KDE Ships Plasma 5.7.2.
date: 2016-07-19
layout: plasma
changelog: plasma-5.7.1-5.7.2-changelog
---

{{%youtube id="v0TzoXhAbxg"%}}

{{<figure src="/announcements/plasma-5.7/plasma-5.7.png" alt="KDE Plasma 5.7 " class="text-center mt-4" width="600px" caption="KDE Plasma 5.7">}}

Tuesday, 19 July 2016.

Today KDE releases a bugfix update to KDE Plasma 5, versioned 5.7.2. <a href='https://www.kde.org/announcements/plasma-5.7.0.php'>Plasma 5.7</a> was released in July with many feature refinements and new modules to complete the desktop experience.

This release adds a week's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Breeze fixed kdelibs4 compilation. <a href="http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=47a397ebef7a636497e75a8da81afffbffa30dda">Commit.</a>
- Fix startup-to-window matchup based on AppName. <a href="http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=1fd011ae918d241140dcbbcddf7bde2d06f1b608">Commit.</a>
