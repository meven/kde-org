---
title: Plasma 5.8.1 Complete Changelog
version: 5.8.1
hidden: true
plasma: true
type: fulllog
---

### <a name='breeze' href='http://quickgit.kde.org/?p=breeze.git'>Breeze</a>

- Call updateButtonsGeometryDelayed on settings reconfigured, so that button sizes are updated immediately. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=b5fd63ad24bca26d3357847996e2c9ec848efc84'>Commit.</a> See bug <a href='https://bugs.kde.org/368974'>#368974</a>

### <a name='discover' href='http://quickgit.kde.org/?p=discover.git'>Discover</a>

- Focus back the applications list. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=f07ea3471ec4fa22387628037d0aeeced4d25708'>Commit.</a> See bug <a href='https://bugs.kde.org/370349'>#370349</a>
- Fix warnings. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=6c0b4bce866a7f80f969ed2485fe29dfb51dfccb'>Commit.</a>
- Fix test. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=6b6810ae1bd3808227091b627f60a3fd3fe3057f'>Commit.</a>
- UI: Make sure all the delegates have the same size. Fix scrolling. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=64c0fb5162a528871b33d6c923b46fe355a963a1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/370173'>#370173</a>
- KNS: Make sure we actually return a version. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=e9022300febf44fdb920010d2df568bc9582c495'>Commit.</a>
- KNS: Improve descriptions. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=26976ca97740f3a10939c7fb304a01f66260810b'>Commit.</a>
- KNS: If we're reusing the first line, don't include it in the summary. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=1038d9fcc88c9cfa939dec731f6aeb7251ab256e'>Commit.</a>
- Don't list plasmoids by package name but by appstream id. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=a5af089bdb0579dc811a90b1b77529de18d4abf5'>Commit.</a>
- Don't specify addons as applications. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=1a7a3e90e063cc14deab5b0c5cb6915f8cfb7c4d'>Commit.</a>
- Make it possible to filter by appstreamids instead of package name. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=8d5729f44a921b4da0052d43d8f7ee6bc4646156'>Commit.</a>
- Use the available package pkgid data as the origin. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=f2460038b9d277d0f5b96b6e7d04f336500c75fe'>Commit.</a>
- Fix deprecated test. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=e02585224e5338612bf139757963b3d016adf971'>Commit.</a>
- Make property readonly. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=9b1718f170db53aef579dd131626cac5412c8285'>Commit.</a>
- Allow PackageKit enable/disable sources. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=4b193e0d72e7e33df8b7424a14c74a13764f8190'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/327178'>#327178</a>
- Let sources emit passive error messages. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=8cba7ad4b93a2f6e619835517c8298e85113029d'>Commit.</a>
- Also use passive message for updates. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=8258d8255006700768480ecdbe787bdcc935c781'>Commit.</a>
- Use proper API. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=067672b4db4b5620ca381d9850c333ab8af5fa3b'>Commit.</a>
- Let transactions emit messages into kirigami passive notifications. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=b2cb8497818d86d2c55aa788102dc86d86200af2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369380'>#369380</a>
- Don't require the TransactionModel to proxy every Transaction signal. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=508053dbafd6ad62bb07f85eac734bee3a191e33'>Commit.</a>
- Wrap label so it can fit in different lines if required. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=0e05d03e11d67d94b2820dc27e00556fcf666b60'>Commit.</a>
- Let the parentapp be the default one. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=c5de9b8b7c58f3e616c3798f31288107600aa708'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369644'>#369644</a>
- Simplify code, ensure data comes from appstream. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=ea91a392a2978ce0d58795e82b235fb736ce3170'>Commit.</a>
- Integrate with the task manager decoration for global progress. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=e3586ad934c0a21c4114b269e0c0016c4edea5e4'>Commit.</a>
- Unused includes. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=862b736ed0e08e937c922c302fade7a989ba64a6'>Commit.</a>
- Ensure we don't crash when requesting the progress without any transactions. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=e9d0d6c962568b4edfd7347eec57627eecbf2e28'>Commit.</a>
- Make sure that the icon has the same margin on both sides. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=6b794ea1221298fa96ab56891dbce1dc60cdeecb'>Commit.</a>
- Prioritize packages from the right architecture. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=ac11ba0fed2df5eaefa6ef4c8bb41f697dcc42a2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369521'>#369521</a>
- Only show the application if resource opened explicitly. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=ae21bd9326460dceb9506a3cbcaf372962e03856'>Commit.</a>
- Use a vdg-approved shadow. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=0fefc5158c895745c6bb89144ff51679d609e8d5'>Commit.</a>
- Also notify about which appstream components are being removed. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=80974f4be58b1da37b0bbeb067aa16330b4e3655'>Commit.</a>
- Use Kirigami.Heading for titles. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=bb5df2eb388e0e5a504e97593f9691ea3c6cbc2d'>Commit.</a>
- Set a bigger size to banner.svg. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=a658b899de4599a71a413f065ae61a3d2acdfae8'>Commit.</a>
- Remove unused function. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=c87e4b41c376ee134745d0227fc5dd948ff09bb4'>Commit.</a>

### <a name='kde-gtk-config' href='http://quickgit.kde.org/?p=kde-gtk-config.git'>KDE GTK Config</a>

- More leveraging of KIconTheme for populating the icon themes model. <a href='http://quickgit.kde.org/?p=kde-gtk-config.git&amp;a=commit&amp;h=73dbdfb4c29a2f69ae1ec7a0c6e5c24c03373bb1'>Commit.</a>
- Let icon themes have cursors as well. <a href='http://quickgit.kde.org/?p=kde-gtk-config.git&amp;a=commit&amp;h=ac0fd79e06f4c9dd2baaa34f13e8bebec36ba83c'>Commit.</a>

### <a name='kdeplasma-addons' href='http://quickgit.kde.org/?p=kdeplasma-addons.git'>Plasma Addons</a>

- Quicklaunch: Listen to external configuration changes. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=0f4ec3e6da9c5bb582fa2cb92601daa8859b02f4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/370285'>#370285</a>
- Purpose is a runtime dependency, do not conditional on it at build time. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=3242dd0280da3f335a5039cbdf6e8359cef50e9b'>Commit.</a>

### <a name='kwin' href='http://quickgit.kde.org/?p=kwin.git'>KWin</a>

- Test case for global shortcut Meta+Shift+w. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e8d6008726e62c39b0ee75bb710a6671b02f3487'>Commit.</a> See bug <a href='https://bugs.kde.org/370341'>#370341</a>
- Add event filter for key press/release events while KWin grabbed keyboard on root window. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=66d1a0cc7a03b5f0e9f7a2fd3bda90c24844fdda'>Commit.</a>
- Only trigger mod-only-shortcuts if global shortcuts are enabled. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=db2ff13d4fa83307b85cbe8622a8e37cb309c4a2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/370146'>#370146</a>
- Workaround xkbcommon behavior concerning consumed modifiers. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=421824b654a5301fe1c0ed97caded66936c91385'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368989'>#368989</a>
- Only repeat one key. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3bc608939444f88dd855130982ce176d393d3ab5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369091'>#369091</a>
- Test case for mod only shortcut with global shortcuts disabled. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=26ad65b1e9875c17e59e50cf4e2f0734caeeb20d'>Commit.</a> See bug <a href='https://bugs.kde.org/370146'>#370146</a>
- [autotests/integration] Add test case for global shortcuts with Fx. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=974abbfaef7c8880980a4b8ba2388691612ea71e'>Commit.</a> See bug <a href='https://bugs.kde.org/368989'>#368989</a>
- [autotest/integration] Enable test mode for QStandardPaths. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=03b8477f2762cac287966a5a3289d33fb91b8c1d'>Commit.</a>
- Add support for resize only borders on Wayland. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=fb59b054881e9888da97ebede8564b9391756457'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364607'>#364607</a>
- Destroy DebugConsole on hide of QWindow. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=fd6e4bb0236b42d4fab2fdc856a381b6159f6605'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369858'>#369858</a>
- Fix the build. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=13991f85eabbf152434db28373b93339f04e1737'>Commit.</a>
- Check for EGL_KHR_platform_x11 in addition to EGL_EXT_platform_x11. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6199631604f9a654a2474ae55ccceafc5a87123f'>Commit.</a>
- [autotests/integration] Add test case for repeating shortcuts. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=48db671684c6257161b4a438fe3f0a7dda7ede31'>Commit.</a> See bug <a href='https://bugs.kde.org/369091'>#369091</a>
- [autotests/integration] Add a test case for closing DebugConsole. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=4d47f6d831f0216743a48c196e7f38f4806e857b'>Commit.</a> See bug <a href='https://bugs.kde.org/369858'>#369858</a>

### <a name='oxygen' href='http://quickgit.kde.org/?p=oxygen.git'>Oxygen</a>

- Fix crash on exit. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=d0cc0029d38f5cf745e32753add8f11609a67c30'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128760'>#128760</a>

### <a name='plasma-desktop' href='http://quickgit.kde.org/?p=plasma-desktop.git'>Plasma Desktop</a>

- Set window state on QEvent::PlatformSurface on Qt 5.5+. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=36e34538bc4011cb808e06b427b85819de8856e8'>Commit.</a>
- Notify prop when pager type changes. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=4cda82f10cd1a0c99fe3ce6b4e84aec836685271'>Commit.</a>
- Inherit from QQmlParserStatus and avoid busywork in Activity Pager case. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=942c3b356c035db1a0e6bfcf6b2fe5b3c0c27ca0'>Commit.</a>
- Fix moving windows to a different activity. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f169fb65356692356e62858875cf897b25cd1462'>Commit.</a>
- Fix circular logic causing Pager to remain hidden. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a109124d0ac684a52c0167e28c21025b850cc6db'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/370324'>#370324</a>
- Only switch between windows within the group when using the wheel above a group parent. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=caa85c116afaf68d86896b6dac760bd83ac75c34'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/370163'>#370163</a>
- Don't set margins by availableScreenRect in widget mode. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=150625c5a95a6968ac3315e14468fcc5517300a1'>Commit.</a>
- Don't create attached LayoutMirroring on non Item. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3dd03afd37a337cbac9c97ba8118e198ec5de783'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369836'>#369836</a>
- Image has width and height properties, not "size". <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3d11772044470154530e41dd1cfb35031344ea4f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369670'>#369670</a>
- [CompactApplet] Set expanded line visibility to false when opacity is 0. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=9ee51d14e00fb95686695468e17ccbf3dd2df852'>Commit.</a>
- Fixing loading for the wallpaper thumbnails in the activity switcher. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f49216b7211026bfb08d8152a1e5ff2beb02208f'>Commit.</a>

### <a name='plasma-sdk' href='http://quickgit.kde.org/?p=plasma-sdk.git'>Plasma SDK</a>

- Remove incorrect uses for add_feature_info(), output is already shown on cmake. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=3cbee2efb51b3a61f2d39dfc7e80760f19fb1ff6'>Commit.</a>

### <a name='plasma-workspace' href='http://quickgit.kde.org/?p=plasma-workspace.git'>Plasma Workspace</a>

- Enable all preview plugins. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=805e039e74d052439f3978ac96179a66a4bc0053'>Commit.</a>
- [DrKonqi] Update URL to Wiki page on how to create useful bug reports. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=b9a75d39cf86e57e1455daf6010311c112ea5cb9'>Commit.</a>
- [SDDM Theme] Show caps lock warning. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=4d0a0a453dbb93ce18b6917d9ff75bc04113f6a8'>Commit.</a>
- Multiscreen bugfixes. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=ca1bf0e3e4c018127d2aac0a78a2e44385b76174'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369665'>#369665</a>
- Center the notification label on Breeze's lockscreen. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e26c52414600c34078445aa2b2d3e450a2a44a6b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/370281'>#370281</a>
- Create new session if appropriate when hitting enter on lock screen. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=8aaa3e3e48fe1d43395fa734daa849b648ab63f8'>Commit.</a>
- Delete containments upon activity deletion. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=921722093048702f5e0ec3c0435f14117fcd9111'>Commit.</a>
- [shell] Fix crash when moving panel between two monitors. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=5cfb957c87994df3cb9c8aa081ad42e67038a7d8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369228'>#369228</a>
- [shell] Fix non-interactive panelview on non-primary screen. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=9c94cd136006959d881a614754a0811ae69f03cc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368790'>#368790</a>
- Fix switching between desktop and folderview. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=53b2a407af9d1d60cfbb1a42af8546ab0a156d38'>Commit.</a>
- [klipper] Move notification from tray to Klipper. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=77aaa88eb2a148f0955eb35b3043d3f0f81fae5b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368808'>#368808</a>
- [Notification Data Engine] Don't group notification if it should replace an existing one. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=fcc806c09859faee0942186dafa2904b3365cf0b'>Commit.</a>
- Make sure allTimezones is set. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=4e5d70d8b5d1d4595ad0975c77b06ef2cb6fa0e1'>Commit.</a>
- Fix kcminit phase 1 and 2. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=eb6223bbe650551157417615a5f564a2928fd0e7'>Commit.</a>
- Set explicit minimum size on panelSpacer so that AppletContainer doesn't set one. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d3fc0c18787100d1795a9115588acf91b0cd3bb3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369823'>#369823</a>
- [Digital Clock] Silence warning. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=27274d4c913d0b7b5cfece0f5e5b14c278f063f9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369734'>#369734</a>
- Always connect to "Local" source. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=3fbc9aad103d684efb0cb7b86fdc2965c4746adc'>Commit.</a>
- Fix usage of qdbus variable in startkde. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=99e5f4316c5ac1045d6a4c97e51a18a69a1ead06'>Commit.</a>
