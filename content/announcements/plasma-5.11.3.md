---
title: KDE Plasma 5.11.3, Bugfix Release for November
release: plasma-5.11.3
version: 5.11.3
description: KDE Ships 5.11.3
date: 2017-11-07
layout: plasma
changelog: plasma-5.11.2-5.11.3-changelog
---

{{%youtube id="nMFDrBIA0PM"%}}

{{<figure src="/announcements/plasma-5.11/plasma-5.11.png" alt="KDE Plasma 5.11 " class="text-center" width="600px" caption="KDE Plasma 5.11">}}

Tuesday, 7 November 2017.

{{% i18n_var "Today KDE releases a %[1]s update to KDE Plasma 5, versioned %[2]s" "Bugfix" "5.11.3." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in %[2]s with many feature refinements and new modules to complete the desktop experience." "5.11" "October" %}}

{{% i18n_var "This release adds a %[1]s worth of new translations and fixes from KDE's contributors.  The bugfixes are typically small but important and include:" "fortnight's" %}}

- Sync xwayland DPI font to wayland dpi. <a href="https://commits.kde.org/plasma-workspace/a09ddff824a076b395fc7bfa801f81eaf2b8ea42">Commit.</a>
- KDE GTK Config: Be flexible to systems without a gtkrc file in /etc. <a href="https://commits.kde.org/kde-gtk-config/952ab8f36e3c52a7ac6830ffd5b5c65f71fa0931">Commit.</a> Fixes bug <a href="https://bugs.kde.org/382291">#382291</a>
- Make sure we store password for all users when kwallet is disabled. <a href="https://commits.kde.org/plasma-nm/ead62e9582092709472e1e76eb3940d742ea808b">Commit.</a> Fixes bug <a href="https://bugs.kde.org/386343">#386343</a>
