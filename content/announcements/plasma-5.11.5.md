---
title: KDE Plasma 5.11.5, Bugfix Release for January
release: plasma-5.11.5
version: 5.11.5
description: KDE Ships 5.11.5
date: 2018-01-02
layout: plasma
changelog: plasma-5.11.4-5.11.5-changelog
---

{{%youtube id="nMFDrBIA0PM"%}}

{{<figure src="/announcements/plasma-5.11/plasma-5.11.png" alt="KDE Plasma 5.11 " class="text-center" width="600px" caption="KDE Plasma 5.11">}}

Tuesday, 2 January 2018.

{{% i18n_var "Today KDE releases a %[1]s update to KDE Plasma 5, versioned %[2]s" "Bugfix" "5.11.5." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in %[2]s with many feature refinements and new modules to complete the desktop experience." "5.11" "January" %}}

{{% i18n_var "This release adds %[1]s worth of new translations and fixes from KDE's contributors.  The bugfixes are typically small but important and include:" "three week's" %}}

- Fixed a missing icon in the appmenu applet
- Better error handling in Discover
- Improved GTK support for the Breeze theme
- Fixed a crash in the screen locker on wayland
