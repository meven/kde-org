---
title: Plasma 5.3.2 complete changelog
version: 5.3.2
hidden: true
plasma: true
type: fulllog
---

### <a name='bluedevil' href='http://quickgit.kde.org/?p=bluedevil.git'>Bluedevil</a>

- Applet: Hide device details when device changes position in model. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=259f0342c95d6009d00ca7679242234ff9608cee'>Commit.</a>

### <a name='breeze' href='http://quickgit.kde.org/?p=breeze.git'>Breeze</a>

- Remove min border size requirement on the sides, for tiny border size and above. Bottom side is kept at min 4 pixels,. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=85ada84741ddc604f697eeffc30738f1f8e6a57e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349039'>#349039</a>
- Properly calculate caption rect when button lists are empty on one of the titlebar side. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=80e931d4d212551a81a3abd3f6e08a19ab858991'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349107'>#349107</a>

### <a name='kdecoration' href='http://quickgit.kde.org/?p=kdecoration.git'>KDE Window Decoration Library</a>

- Use 0 as client height when shaded. <a href='http://quickgit.kde.org/?p=kdecoration.git&amp;a=commit&amp;h=6adb6d90256a8f5e763c1d4a0e43efcde74d5b59'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348060'>#348060</a>. Code review <a href='https://git.reviewboard.kde.org/r/123894'>#123894</a>

### <a name='kdeplasma-addons' href='http://quickgit.kde.org/?p=kdeplasma-addons.git'>Plasma Addons</a>

- Fix systemloadviewer not opening ksysguard when clicked. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=6ecc5fc44e9ffc1535bfff1157be1b3efef875e2'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124110'>#124110</a>
- Don't specify a library on pure QML applet. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=f003ad0716b18a11df9de7f08edde27e475e7d0c'>Commit.</a>

### <a name='kwin' href='http://quickgit.kde.org/?p=kwin.git'>KWin</a>

- 'Defaults' should set the title bar double-click action to 'Maximize.'. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=26ee92cd678b1e070a3bdebce100e38f74c921da'>Commit.</a>
- Align to c4140d6f4e5cd953023f2c078088d20a553ab875. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3f8a8ea32afba42b436c1e274e00d52d98d5b0c2'>Commit.</a>
- Set still required xcb properties in findRule. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6a276260906353ebeedcd07abd081c28e67a8163'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348472'>#348472</a>. Code review <a href='https://git.reviewboard.kde.org/r/123953'>#123953</a>
- Keep quick maximized w/ size restritions in screen. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1df2d5979fb25cb83442b5df74c84d4cba6029eb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348121'>#348121</a>. Code review <a href='https://git.reviewboard.kde.org/r/123910'>#123910</a>
- Show autohiding panels instantly. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c4140d6f4e5cd953023f2c078088d20a553ab875'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123904'>#123904</a>
- Maximize effect: skip crossfadign on user resizes. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=12fec3f7f5c899af4f9e7bc8f8a1a34879e9bdd1'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123901'>#123901</a>

### <a name='libkscreen' href='http://quickgit.kde.org/?p=libkscreen.git'>libkscreen</a>

- Really set the new size after the intermediate. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=a018fef80b51be156230c0f51008e4471a75cb39'>Commit.</a> See bug <a href='https://bugs.kde.org/349123'>#349123</a>

### <a name='muon' href='http://quickgit.kde.org/?p=muon.git'>Muon</a>

- Let the notifier heading adapt to the view size. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=bad45cc68d17c470c6d687155ce8226ca09c9f1b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348889'>#348889</a>
- Make sure that the backends aren't unavailable when we decide to install. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=eac54c6f314f24a085dbaa12a0b1e3d00fd934d8'>Commit.</a> See bug <a href='https://bugs.kde.org/348239'>#348239</a>
- Make sure the apt notifier gets initialized. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=a87de352a5b682c381e8c7ec78de2730e7477b44'>Commit.</a>

### <a name='oxygen' href='http://quickgit.kde.org/?p=oxygen.git'>Oxygen</a>

- Remove min border size requirement on the sides, for tiny border size and above. Bottom side is kept at min 4 pixels,. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=dd91a9a3e51488bf8c821c7f975aaad33477da55'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349039'>#349039</a>
- Properly calculate caption rect when button lists are empty on one of the titlebar side. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=4ea689ee9090dc510f74cf0932e57f6a497caab5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349107'>#349107</a>

### <a name='plasma-desktop' href='http://quickgit.kde.org/?p=plasma-desktop.git'>Plasma Desktop</a>

- Don't overwrite audio profile entries with same priority. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=4b576cd02867672069c8077a43bb7caf442de8cc'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124108'>#124108</a>
- Don't wrap too early. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=4c83adb3b9a157b4b904f6197b64707a28b3cc1b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348322'>#348322</a>
- Improve Applet Alternatives dialog. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=0e90ea5fea7947acaf56689df18bbdce14e8a35f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/345786'>#345786</a>
- Remove X-KDE-Library from the trash library. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f0d4cce819b3ce683a11b49a93ba81bafef7a4dd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349207'>#349207</a>
- Fix dropping files onto the desktop containment. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a44392b54ca3e8fa7250537c7d7a9f4004c6af03'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124055'>#124055</a>. Fixes bug <a href='https://bugs.kde.org/346867'>#346867</a>
- Fix Plasmoid.toolTipTextFormat not working. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=c534f799cace5b0511e4adf802551df17b00c3d4'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124017'>#124017</a>
- Disable mouse interaction on Text instance in the delegate. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=18cacb1093901e39b91b08c73a46b174f84bed41'>Commit.</a>
- Fix type error. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=0c4ca88e7636358ef9991b52ba8bb8dab7779949'>Commit.</a>
- Clean up state. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f0e9b330e7a679789b5fc9293a9298e2c0923e2c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348587'>#348587</a>
- Fix the panel configuration width to avoid truncated buttons for long strings. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f790473698d4ab06d25dd2b3d51683589e2b269f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123815'>#123815</a>
- Make sure panels doesn't overlap with the desktop folder view. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=1f9c20e4c4c25f5df3593792f5166af3eeee6068'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123884'>#123884</a>
- Fix build on older stacks. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=7d0b022599f950309c591b010640283b8cbe0e0f'>Commit.</a>
- Fix crash. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3f438ce0670fd681a0bb116e59818ba4de9b4457'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348354'>#348354</a>
- Require xorg-evdev >= 2.8.99.1. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=0a63e5499aa826ce5092c6e29b956caa13e481c1'>Commit.</a>

### <a name='plasma-nm' href='http://quickgit.kde.org/?p=plasma-nm.git'>Plasma Networkmanager (plasma-nm)</a>

- Make Toolbar highdpi-fit. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=a0fab0f0e91bdea9e190ae38a3f19424fcaa1025'>Commit.</a>
- OpenVPN: Do not overwrite modes already configured in .ui files. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=b06d0e74cab99efd7e6d2545e79007e3a249271b'>Commit.</a>
- PPTP: Fix storing of secret flags. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=bf917dc6539d9af7888f80b449b0292915d59608'>Commit.</a>
- OpenVPN: Do not insert translated value for remote-cert-tls. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=f489645b625877842845ac8211a508d66fe2c947'>Commit.</a>
- Adjust SSID/BSSID combobox to minimum contents length. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=ee279ea34ce7fd5f7bc58e68454230c3dafdd1e3'>Commit.</a>

### <a name='plasma-workspace' href='http://quickgit.kde.org/?p=plasma-workspace.git'>Plasma Workspace</a>

- [digital-clock] Set the proper tooltip format text. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=4e180fd443c1d80912086f9e4c3e7f546cfde712'>Commit.</a>
- [notification] Introduce a more compact notification popup when no icon is set. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=eb5693810e6d0e907071e15dc1acf8b32dac534a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124151'>#124151</a>
- [notifications] Add missing id. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=69e15486c86a0851c2978ef7e4ce0d0b73298b5a'>Commit.</a>
- [notifications] Take the height of the whole main layout as the implicit height. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f5f19c95942b5f599c2326be46d53fdd3a368bdd'>Commit.</a>
- [notifications] Rework the notifications sizing code. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=707a9b0bc74af62cdf1fe04cf11c6163a5006518'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124149'>#124149</a>. Fixes bug <a href='https://bugs.kde.org/339588'>#339588</a>. Fixes bug <a href='https://bugs.kde.org/349142'>#349142</a>
- Replace other Notifications services when Plasma's notifications are enabled. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=b533e7d13f7daac129d4f91b4cdd12d9362ff15b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124102'>#124102</a>
- Make shutdown scripts work. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=96fdec6734087e54c5eee7c073b6328b6d602b8e'>Commit.</a>
- Remove ConfigPlugins entry. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=20a5719c8ecfab24d6b006e266cd093a5dd7c006'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349250'>#349250</a>
- Implement keyboard navigation for timezone settings. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=4da49fcdd98609ca5d4c55bb20556614d5ce6353'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124052'>#124052</a>
- Silence warning. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6679ed01098093f281b51c58546e97b8930dd9f7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348794'>#348794</a>
- Fix margins. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=2c89256a46488077705e427ecdf7a1676be886b7'>Commit.</a>
- Make height dynamic again. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=b47613c0f5e2e9fac18d40d502f1976448efcf62'>Commit.</a>
- Fix minimum size calc. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=1e824011724a0e28b7634d5ffb10e1405856946c'>Commit.</a>
- Fix notification popup sizing to stop cutting off content. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=baf0e675c3e1a6a2534a892367c3f93bdc7caf8f'>Commit.</a>
- Fix shortcut activation. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=52337d6c082268aa41d5532840504d81692de6b8'>Commit.</a>
- Drop magic values and use window geometry sans frames for intersection test. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=5af70417027276e7a75fba0f632475c1470c0996'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/268259'>#268259</a>
- Unbreak Undo notifications on Plasmoid removal. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6b3900767b6c97916e0e82dd8aa8fa8029ca7616'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123926'>#123926</a>. Fixes bug <a href='https://bugs.kde.org/345149'>#345149</a>
- Fix launcher sorting the by-activity sorting strategy. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=c66f30f6ddb3420e3c2fb62e404c684d6231d264'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348324'>#348324</a>
- [notifications] Optimize sending the notification data a bit. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=c0276440ee8ca26cf79a08a1a4ac2ce12425c10b'>Commit.</a>
- [notifications] Make sure the "Open..." button on finished jobs still work. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=36bd67d2d9d9e74fda52343ae7ccc5b41e5b4498'>Commit.</a>
- Reuse the existing Notify method for Notification's DataEngine createNotification call. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a12bb76502952749c069b9ab70ac61ee1353079b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123923'>#123923</a>. Fixes bug <a href='https://bugs.kde.org/342605'>#342605</a>
- Bugzilla Integration: Look for the mappings file in the correct location. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=660eb9412a274e28ce13b7091b0e6e7620e5d68b'>Commit.</a>
- Ensure the panel's view position in the screen. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=855bd6cadab1e00eb324d6a654264549bcf400a5'>Commit.</a>
- Fallback to AttentionIcon for SNI when animations are disabled. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=ba64ae775699ae5e7d18a70e29078cc4fc5e4ead'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123381'>#123381</a>
- Adjust layout for smaller panels or vertical ones. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=c1e9715fa08d1cee6d1e953c654598d56be9af6c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347594'>#347594</a>
