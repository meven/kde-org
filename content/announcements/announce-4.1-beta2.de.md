---
title: KDE 4.1 Beta 2 Release Announcement
date: "2008-06-24"
---

<h3 align="center">
  Das KDE-Projekt veröffentlicht die zweite Beta-Version von KDE 4.1
</h3>

<p align="justify">
  <strong>
Die Gemeinschaft der KDE-Entwickler gibt die Freigabe der zweiten Beta-Version von KDE 4.1 bekannt.</strong>
</p>

<p align="justify">
Das <a href="http://www.kde.org/">KDE-Projekt</a> ist stolz die zweite Beta-Version von KDE 4.1 zur Veröffentlichung frei zu geben. Diese zweite Beta-Version richtet sich an Tester – gleichermaßen an Mitglieder der Entwicklergemeinschaft, sowie KDE-Enthusiasten – um noch vorhandene Fehler und Defizite zu erkennen, so dass die Version 4.1 dann KDE 3 bei den Endbenutzern vollständig ablösen kann. KDE 4.1 Beta 2 ist in Form von Binärpaketen für eine große Breite von Plattformen, als auch in Form von Quellpaketen verfügbar. Die endgültige Version von KDE 4.1 wird voraussichtlich Ende Juli 2008 veröffentlicht werden.
</p>

<h4>
  <a id="changes">Besonderheiten von KDE 4.1 Beta 2</a>
</h4>

<p align="justify">
Seit einem Monat ist der Funktionsumfang für KDE 4.1 eingefroren. Seitdem haben die KDE-Entwickler
an der Fertigstellung der Funktionen, der Integration in die Arbeitsfläche sowie an der Dokumentation und
Übersetzung der Pakete gearbeitet.
Es wurden mehrere Tage der Fehlerbeseitung (Bugdays der BugSquad) gewidmet.
Es gibt immer noch Fehler, welche bis zur Finalversion beseitigt werden müssen, 
jedoch zeigt sich KDE 4.1 Beta 2 schon in einem guten Zustand.
Tests und Rückmeldungen zu dieser
Veröffentlichung sind erwünscht und werden geschätzt, damit KDE 4.1 für 
Furore sorgen kann.

<div class="text-center">
	<a href="/announcements/announce-4.1-beta2/desktop-folderview.png">
	<img src="/announcements/announce-4.1-beta2/desktop-folderview_thumb.png" class="img-fluid">
	</a> <br/>
	<em>KDE 4.1 Beta 2 mit der Plasma-Arbeitsfläche</em>
</div>
<br/>

<ul>
    <li>Sprachanbindungen von KDE 4.1 für verschiedene Sprachen wie Python oder
    Ruby
    </li>
    <li>Verbesserungen der Benutzbarkeit von Dolphin
    </li>
    <li>allgemeine Verbesserungen für Gwenview
    </li>
</ul>

Ein vollständige Liste der neuen Funktionen von KDE 4.1 gibt es in der
<a href="http://techbase.kde.org/Schedules/KDE4/4.1_Release_Goals">Techbase</a>.

</p>

<h4>
  Sprachanbindungen
</h4>
<p align="justify">
Während die meisten KDE-4.1-Anwendungen in C++ geschrieben sind, decken die Sprachanbindungen die 
Funktionsmöglichkeiten für Anwendungsentwickler auf, die eine andere Programmiersprache 
bevorzugen. KDE 4.1 bringt Unterstützung für verschiedene andere Sprachen, wie 
Python oder Ruby. Das Drucker-Miniprogramm, welches in KDE 4.1 eingefügt wurde, ist in 
Python geschrieben was für den Benutzer jedoch keinen Unterschied macht.
</p>

<h4>
  Dolphin reift
</h4>
<p align="justify">
Dolphin, KDE4's Dateiverwalter bekam mehrere Verbesserungen.

<div class="text-center">
	<a href="/announcements/announce-4.1-beta2/dolphin-tagging.png">
	<img src="/announcements/announce-4.1-beta2/dolphin-tagging_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Die semantische Arbeitsfläche mit dem Tagging-System</em>
</div>
<br/>

Die ersten Bestandteile von <a href="http://nepomuk.kde.org/">NEPOMUK</a>, des "Social
Semantic Desktop" werden sichtbar und breiter anwendbar. Unterstützung für Tagging
in Dolphin zeigt die ersten Funktionen, die diese Technologie bringt.
Entwickelt wird sie im Rahmen des European Research Programme.

<div class="text-center">
	<a href="/announcements/announce-4.1-beta2/dolphin-selection.png">
	<img src="/announcements/announce-4.1-beta2/dolphin-selection_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Dateien auswählen im Ein-Klick-Modus</em>
</div>
<br/>

Das Auswählen von Dateien wurde durch einen kleinen "+"-Knopf in der linken oberen Ecke einfacher
gemacht. Der Knopf wählt die Datei aus, anstatt sie zu öffnen.
Diese Änderung verbesserte die Handhabung im Ein-Klick-Modus enorm, vermeidet versehentliches
Öffnen von Dateien und ist intuitiv und einfach.
<br />

<div class="text-center">
	<a href="/announcements/announce-4.1-beta2/dolphin-treeview.png">
	<img src="/announcements/announce-4.1-beta2/dolphin-treeview_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Neue Baumansicht in Dolphin</em>
</div>
<br/>

Viele Benutzer haben nach einer Baumansicht im detaillierten Listenmodus gefragt.
Diese Funktion wurde bearbeitet und ergibt zusammen mit dem Ein-Klick-Modus eine
Verbesserung beim Verschieben und Kopieren von Dateien.

</p>

<h4>
  Gwenview aufpoliert
</h4>
<p align="justify">
Gwenview, der Standard-Bildbetrachter in KDE 4,  wurde weiter auf Hochglanz poliert.
Optionen wie das Drehen und die Vollbildansicht wurden in direkten Kontext mit dem Bild gebracht.
Dadurch wirkt die Benutzerschnittstelle aufgeräumter, die Mausbewegungen werden 
weniger und somit die Anwendung intuitiver und einfacher zu benutzen.

</p>

<div class="text-center">
	<a href="/announcements/announce-4.1-beta2/gwenview-browse.png">
	<img src="/announcements/announce-4.1-beta2/gwenview-browse_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Übersicht eines Verzeichnisses in Gwenview</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/announce-4.1-beta2/gwenview.png">
	<img src="/announcements/announce-4.1-beta2/gwenview_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Gwenview mit seiner Vorschaubilderleiste</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/announce-4.1-beta2/systemsettings-emoticons.png">
	<img src="/announcements/announce-4.1-beta2/systemsettings-emoticons_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Das neue Systemeinstellungs-Modul für Emoticons</em>
</div>
<br/>

<h4>
  Veröffentlichung der endgültigen Version von KDE 4.1
</h4>
<p align="justify">
Die Veröffentlichung von KDE 4.1 ist für den 29. Juli 2008 geplant. Diese zeitbasierte Veröffentlichung ist genau sechs Monate nach der Veröffentlichung von KDE 4.0.
</p>

<h4>
  Holen Sie sich’s, führen Sie’s aus, testen Sie’s
</h4>
<p align="justify">
  Freiwillige und Linux- beziehungsweise Unix-Betriebsystemhersteller haben freundlicherweise Binärpakete von KDE 4.0.83 (Beta 2) für die meisten Linux-Distributionen, Mac OS X und Windows, zur Verfügung gestellt. Bitte beachten Sie, dass diese Pakete keinesfalls für den Alltagseinsatz geeignet sind. Werfen Sie einen Blick auf das Softwareverwaltungssystem Ihrer Distribution oder die nachfolgenden Links für distributionsspezifische Anweisungen:

<h4>
  KDE 4.1 Beta 2 (4.0.83) kompilieren
</h4>
<p align="justify">
  <a id="source_code"></a><em>Source Code</em>.
  Der vollständige Quelltext von KDE 4.0.83 kann <a
  href="http://www.kde.org/info/4.0.83">gratis heruntergeladen</a> werden.
Anweisungen zum Kompilieren und Installieren von KDE 4.0.83
  finden Sie auf der <a href="/info/4.0.83">KDE-4.0.83-Informationsseite</a>, oder in der <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">„TechBase“</a>.
</p>

<h4>
  KDE unterstützen
</h4>
<p align="justify">
 KDE ist ein <a href="http://www.gnu.org/philosophy/free-sw.html">freies Softwareprojekt</a>, das nur existiert und wächst, weil unzählige Freiwillige ihre Zeit und Arbeit investieren. KDE ist immer auf der Suche nach neuen Freiwilligen, die etwas beitragen möchten. Unabhängig davon ob es ums Programmieren, das Beheben oder Berichten von Fehlern, das Schreiben von Dokumentationen, Übersetzungen, Werbung und Öffentlichkeitsarbeit, Geld usw. geht. Jegliche Beiträge sind willkommen und werden mit Freuden entgegengenommen. Bitte lesen Sie die Seite <a
href="/community/donations/">„KDE unterstützen“</a> für nähere Informationen. </p>

<p align="justify">
Wir würden uns freuen, bald von Ihnen zu hören!
</p>

<h4>Über KDE 4</h4>
<p align="justify">
KDE 4.0 ist die innovative, freie Arbeitsumgebung mit zahlreichen Anwendungen, sowohl für den täglichen Gebrauch, als auch für spezielle Zwecke. Plasma ist die neue Benutzerschnittstelle für die Arbeitsoberfläche von KDE 4 und bietet eine intuitive Oberfläche um mit der Arbeitsoberfläche und Anwendungen zu interagieren. Der Konqueror-Webbrowser integriert das Internet in die Arbeitsumgebung. Der Dolphin-Dateimanager, der Okular-Dokumentbetrachter und das „Systemeinstellungen“-Kontrollzentrum komplettieren die Standard-Arbeitsumgebung.
<br />
KDE basiert auf den KDE-Bibliotheken, welche einfachen Zugriff auf Netzwerkressourcen mittels der KIO-Technologie und erweiterte, grafische Effekte durch die Möglichkeiten von Qt4 bieten. Phonon und Solid, welche auch Teil der KDE-Bibliotheken sind, erweitern alle KDE-Anwendungen um ein Multimedia-System und ermöglichen bessere Integration von Hardware.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}
