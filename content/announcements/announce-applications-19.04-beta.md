---  
title: KDE Ships Beta of KDE Applications 19.04
release: 'applications-19.03.80'
version_text: "19.04 Beta"
version_number: "19.03.80"
description: KDE Ships Applications 19.04 Beta.
date: 2019-03-22
layout: application
---

March 22, 2019. Today KDE released the beta of the new versions of KDE Applications. With dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing.

{{% i18n_var "Check the <a href='%[1]s'>community release notes</a> for information on tarballs and known issues. A more complete announcement will be available for the final release." "https://community.kde.org/Applications/19.04_Release_Notes" %}}

The KDE Applications 19.04 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the team by installing the beta <a href='https://bugs.kde.org/'>and reporting any bugs</a>.
