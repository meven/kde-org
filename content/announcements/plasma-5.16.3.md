---
title: KDE Plasma 5.16.3, Bugfix Release for June
release: "plasma-5.16.3"
version: "5.16.3"
description: KDE Ships Plasma 5.16.3.
date: 2019-07-09
layout: plasma
changelog: plasma-5.16.2-5.16.3-changelog
---

{{% youtube id="T-29hJUxoFQ" %}}

{{<figure src="/announcements/plasma-5.16/plasma_5.16.png" alt="Plasma 5.16" class="text-center" width="600px" caption="KDE Plasma 5.16">}}

Tuesday, 9 July 2019.

{{% i18n_var "Today KDE releases a Bugfix update to KDE Plasma 5, versioned %[1]s" "5.16.3." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in June with many feature refinements and new modules to complete the desktop experience." "5.16" %}}

This release adds a fortnight's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- DrKonqi will now automatically log into bugs.kde.org when possible. <a href="https://commits.kde.org/drkonqi/add2ad75e3625c5b0d772a555ecff5aa8bde5c22">Commit.</a> Fixes bug <a href="https://bugs.kde.org/202495">#202495</a>. Phabricator Code review <a href="https://phabricator.kde.org/D22190">D22190</a>
- Fix compilation without libinput. <a href="https://commits.kde.org/plasma-desktop/a812b9b7ea9918633f891dd83998b9a1f47e413c">Commit.</a>
- Keep Klipper notifications out of notification history. <a href="https://commits.kde.org/plasma-workspace/120aed57ced1530d85e4d522cfb3697fbce605fc">Commit.</a> Fixes bug <a href="https://bugs.kde.org/408989">#408989</a>. Phabricator Code review <a href="https://phabricator.kde.org/D21963">D21963</a>
