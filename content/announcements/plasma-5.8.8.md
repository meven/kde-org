---
title: KDE Plasma 5.8.8 LTS, Bugfix Release for October
release: plasma-5.8.8
version: 5.8.8
description: KDE Ships 5.8.8
date: 2017-10-25
layout: plasma
changelog: /plasma-5.8.7-5.8.8-changelog
---

{{%youtube id="LgH1Clgr-uE"%}}

{{<figure src="/announcements/plasma-5.8/plasma-5.8.png" alt="KDE Plasma 5.8 " class="text-center" width="600px" caption="KDE Plasma 5.8">}}

Wednesday, 25 October 2017.

{{% i18n_var "Today KDE releases an %[1]s update to KDE Plasma 5, versioned %[2]s" "LTS bugfix" "5.8.8 LTS." %}}

{{% i18n_var `<a href='https://www.kde.org/announcements/plasma-5.8.0.php'>Plasma %[1]s</a> was released in %[2]s with many feature refinements and new modules to complete the desktop experience.` "5.8 LTS" "October" %}}

{{% i18n_var "This release adds %[1]s worth of new translations and fixes from KDE's contributors.  The bugfixes are typically small but important and include:" "six months" %}}

- Fix crash when using Discover during updates. <a href="https://commits.kde.org/discover/aa12b3c822354e0f526dccff7f1c52c26fb35073">Commit.</a> Fixes bug <a href="https://bugs.kde.org/370906">#370906</a>
- Try to read CPU clock from cpufreq/scaling_cur_freq instead of /proc/cpuinfo. <a href="https://commits.kde.org/ksysguard/cbaaf5f4ff54e20cb8ec782737e04d540085e6af">Commit.</a> Fixes bug <a href="https://bugs.kde.org/382561">#382561</a>. Phabricator Code review <a href="https://phabricator.kde.org/D8153">D8153</a>
- DrKonqi crash handler: make attaching backtraces to existing bug reports work. <a href="https://commits.kde.org/plasma-workspace/774a23fc0a7c112f86193ee6e07947fee6282ef4">Commit.</a>
- Look and Feel/Notifications. Fix crash when removing panel with system tray in it. <a href="https://commits.kde.org/plasma-workspace/8a05294e5b3ef1df86f099edde837b8c8d28ccaf">Commit.</a> Fixes bug <a href="https://bugs.kde.org/378508">#378508</a>. Phabricator Code review <a href="https://phabricator.kde.org/D6653">D6653</a>
