---
title: KDE Plasma 5.13.5, Bugfix Release for September
release: plasma-5.13.5
version: 5.13.5
description: KDE Ships 5.13.5
date: 2018-09-04
changelog: plasma-5.13.4-5.13.5-changelog
---

{{%youtube id="C2kR1_n_d-g"%}}

{{<figure src="/announcements/plasma-5.13/plasma-5.13.png" alt="Plasma 5.13" class="text-center" width="600px" caption="KDE Plasma 5.13">}}

Tue, 4 September 2018.

{{% i18n_var "Today KDE releases a %[1]s update to KDE Plasma 5, versioned %[2]s" "Bugfix" "5.13.5." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in %[2]s with many feature refinements and new modules to complete the desktop experience." "5.13" "June" %}}

{{% i18n_var "This release adds %[1]s worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:" "a month's" %}}

- Picture of the Day: Actually update the image every day. <a href="https://commits.kde.org/kdeplasma-addons/81e89f1ea830f278fdb6f086baa4741c9606892f">Commit.</a> Fixes bug <a href="https://bugs.kde.org/397914">#397914</a>. Phabricator Code review <a href="https://phabricator.kde.org/D15124">D15124</a>
- Prevent paste in screen locker. <a href="https://commits.kde.org/kscreenlocker/1638db3fefcae76f27f889b3709521b608aa67ad">Commit.</a> Fixes bug <a href="https://bugs.kde.org/388049">#388049</a>. Phabricator Code review <a href="https://phabricator.kde.org/D14924">D14924</a>
- Fix QFileDialog not remembering the last visited directory. <a href="https://commits.kde.org/plasma-integration/b269980db1d7201a0619f3f5c6606c96b8e59d7d">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D14437">D14437</a>
