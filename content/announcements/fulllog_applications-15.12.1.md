---
title: KDE Applications 15.12.1 Full Log Page
type: fulllog
version: 15.12.1
hidden: true
---

<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Show]</a></h3>
<ul id='ulakonadi' style='display: none'>
<li>Fix -Wreorder in dbconfigpostgresql.cpp. <a href='http://commits.kde.org/akonadi/d63d9113ffea7724362f18fb8b2484e6fe32ce1c'>Commit.</a> </li>
<li>PostgreSQL: respect port setting for db connection. <a href='http://commits.kde.org/akonadi/3ea38f32260cb7ae203f6cd9926a0475b36a12c9'>Commit.</a> </li>
<li>Release idle connections to the database. <a href='http://commits.kde.org/akonadi/289c775d00e0ea70815664db47538ab80957823b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/121120'>#121120</a></li>
<li>Fix 'connection still in use' warning when using SQLITE. <a href='http://commits.kde.org/akonadi/e0963789a0265ef02589d220f9dfac205d785c7e'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-search' href='https://cgit.kde.org/akonadi-search.git'>akonadi-search</a> <a href='#akonadi-search' onclick='toggle("ulakonadi-search", this)'>[Show]</a></h3>
<ul id='ulakonadi-search' style='display: none'>
<li>Increase version. <a href='http://commits.kde.org/akonadi-search/af6408a37ca1933fd1c856877d9512f541bb46f8'>Commit.</a> </li>
<li>Search in correct directory. So completion works now. <a href='http://commits.kde.org/akonadi-search/c17141c626e411568eceaf90dc04184e71255f52'>Commit.</a> </li>
<li>Fix path. <a href='http://commits.kde.org/akonadi-search/98eea4618594fe2d28158b390aafb29209c50c03'>Commit.</a> </li>
</ul>
<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Show]</a></h3>
<ul id='ulark' style='display: none'>
<li>Make the preview window modeless. <a href='http://commits.kde.org/ark/17f91eb5b189e08777da38ab541ad6a9d6c27bde'>Commit.</a> </li>
<li>Don't run a ListJob when creating a new archive. <a href='http://commits.kde.org/ark/e5fcaedc4415a75a3c35cc665386bff518520c90'>Commit.</a> </li>
<li>Show errors from drag-and-drop extraction jobs. <a href='http://commits.kde.org/ark/344b80ff10eee987e6ad824eb889fe610a1d6507'>Commit.</a> </li>
<li>Fallback to read-only mode if there are no read-write executables. <a href='http://commits.kde.org/ark/087e5aab49c60ac5930742fe892fa930048e2f43'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357057'>#357057</a></li>
<li>Fix crash after a failed ListJob. <a href='http://commits.kde.org/ark/307a3c3295bc61dadaa2153cf029b73d03f923d4'>Commit.</a> </li>
<li>Fix ark cannout open rar file with space in archive file path. <a href='http://commits.kde.org/ark/24b0afbec552f56b30d91fc041fc727add5b6e58'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126360'>#126360</a></li>
</ul>
<h3><a name='artikulate' href='https://cgit.kde.org/artikulate.git'>artikulate</a> <a href='#artikulate' onclick='toggle("ulartikulate", this)'>[Show]</a></h3>
<ul id='ulartikulate' style='display: none'>
<li>Make the translators tab in the about dialog appear. <a href='http://commits.kde.org/artikulate/99a44824261ceab0b841840543cf20fb49f68b39'>Commit.</a> </li>
<li>Translate user interface. <a href='http://commits.kde.org/artikulate/a8afb9ecc34d294051ebb87bd0a33e4acf322c57'>Commit.</a> </li>
<li>Autotests: Use QTEST_GUILESS_MAIN. <a href='http://commits.kde.org/artikulate/bbb12a155e40ad2774c42d79c709a545925f5664'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126457'>#126457</a></li>
</ul>
<h3><a name='jovie' href='https://cgit.kde.org/jovie.git'>jovie</a> <a href='#jovie' onclick='toggle("uljovie", this)'>[Show]</a></h3>
<ul id='uljovie' style='display: none'>
<li>Use SHARE_INSTALL_PREFIX to install appstream data. <a href='http://commits.kde.org/jovie/510ad3a839bcb5836335dd52c9936e8f4f2849e7'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126483'>#126483</a></li>
<li>Fix bug causing hang when clipboard is empty and jovie tries to use an empty string. <a href='http://commits.kde.org/jovie/59576c66f9e870fea4c8d94154ad12409db31e52'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126316'>#126316</a></li>
</ul>
<h3><a name='kaccounts-providers' href='https://cgit.kde.org/kaccounts-providers.git'>kaccounts-providers</a> <a href='#kaccounts-providers' onclick='toggle("ulkaccounts-providers", this)'>[Show]</a></h3>
<ul id='ulkaccounts-providers' style='display: none'>
<li>Fix message extraction: restrict to real i18n strings. <a href='http://commits.kde.org/kaccounts-providers/9ac2152b2e5f56cc507ddf48f5ee158f83fa500b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126496'>#126496</a></li>
</ul>
<h3><a name='kalgebra' href='https://cgit.kde.org/kalgebra.git'>kalgebra</a> <a href='#kalgebra' onclick='toggle("ulkalgebra", this)'>[Show]</a></h3>
<ul id='ulkalgebra' style='display: none'>
<li>Add the namespace to the appdata file. <a href='http://commits.kde.org/kalgebra/6f22f79767ac63e2c1128b451aea3008361fb6da'>Commit.</a> </li>
</ul>
<h3><a name='kanagram' href='https://cgit.kde.org/kanagram.git'>kanagram</a> <a href='#kanagram' onclick='toggle("ulkanagram", this)'>[Show]</a></h3>
<ul id='ulkanagram' style='display: none'>
<li>Make the translators tab in the about dialog appear. <a href='http://commits.kde.org/kanagram/2c1ac9c80754f605a4e24552a13d52780ee901b6'>Commit.</a> </li>
<li>Rename appdata to match .desktop file. <a href='http://commits.kde.org/kanagram/83788e7072367c457edec5132d54bcb24c47280a'>Commit.</a> </li>
</ul>
<h3><a name='kbruch' href='https://cgit.kde.org/kbruch.git'>kbruch</a> <a href='#kbruch' onclick='toggle("ulkbruch", this)'>[Show]</a></h3>
<ul id='ulkbruch' style='display: none'>
<li>Use KLocalizedString::setApplicationDomain. <a href='http://commits.kde.org/kbruch/48dab4d6826970ea076ed15a3246357b57a7bbd9'>Commit.</a> </li>
</ul>
<h3><a name='kcalc' href='https://cgit.kde.org/kcalc.git'>kcalc</a> <a href='#kcalc' onclick='toggle("ulkcalc", this)'>[Show]</a></h3>
<ul id='ulkcalc' style='display: none'>
<li>Fix formatting of copied number. <a href='http://commits.kde.org/kcalc/d0478accc9334468490c1a2597c8dd1805ac4312'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356355'>#356355</a>. Code review <a href='https://git.reviewboard.kde.org/r/126528'>#126528</a></li>
<li>Use KLocalizedString::setApplicationDomain. <a href='http://commits.kde.org/kcalc/577abbcddef59ac4810a83629dae05a37b6f30aa'>Commit.</a> </li>
</ul>
<h3><a name='kcalutils' href='https://cgit.kde.org/kcalutils.git'>kcalutils</a> <a href='#kcalutils' onclick='toggle("ulkcalutils", this)'>[Show]</a></h3>
<ul id='ulkcalutils' style='display: none'>
<li>Don't write .pyc files when running extract_strings_ki18n. <a href='http://commits.kde.org/kcalutils/5735268aa22d8d5ba0e9a5ab057e452551dcaed3'>Commit.</a> </li>
</ul>
<h3><a name='kcharselect' href='https://cgit.kde.org/kcharselect.git'>kcharselect</a> <a href='#kcharselect' onclick='toggle("ulkcharselect", this)'>[Show]</a></h3>
<ul id='ulkcharselect' style='display: none'>
<li>Use KLocalizedString::setApplicationDomain. <a href='http://commits.kde.org/kcharselect/6b96fe879ab065d3275eade14ffa6b59c48c41ad'>Commit.</a> </li>
</ul>
<h3><a name='kcontacts' href='https://cgit.kde.org/kcontacts.git'>kcontacts</a> <a href='#kcontacts' onclick='toggle("ulkcontacts", this)'>[Show]</a></h3>
<ul id='ulkcontacts' style='display: none'>
<li>Fix Bug 357107 - After importing vCard with typeless TEL numbers, kAddressbook does not show any labels next to the phone numbers. <a href='http://commits.kde.org/kcontacts/6496c1175a0061a765489f4fb40fcb237e9d9973'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357107'>#357107</a></li>
</ul>
<h3><a name='kdelibs' href='https://cgit.kde.org/kdelibs.git'>kdelibs</a> <a href='#kdelibs' onclick='toggle("ulkdelibs", this)'>[Show]</a></h3>
<ul id='ulkdelibs' style='display: none'>
<li>Try harder hiding any mimetype definitions. <a href='http://commits.kde.org/kdelibs/56fc6168d100f2cad7db9c5cfa70e9488ea17fd0'>Commit.</a> </li>
<li>Fix typo in apidox of KWidgetItemDelegate. <a href='http://commits.kde.org/kdelibs/a5b86628debea250f6bde70156ca44482d30231c'>Commit.</a> </li>
<li>Fix CMP0064 warning by setting policy CMP0054 to NEW. <a href='http://commits.kde.org/kdelibs/826a5ff3278f492a99ac6827614e1d0ca40a45e8'>Commit.</a> </li>
<li>FindPyKDE4: Make PYKDE4_INSTALL_PYTHON_FILES use PYTHON_INSTALL. <a href='http://commits.kde.org/kdelibs/016841aeb0b180981122085e9b1d49ae66951670'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126413'>#126413</a></li>
<li>PythonMacros: specify destination directory in byte-compiled files. <a href='http://commits.kde.org/kdelibs/94f1d2fa9582a2942d5154b85c849cc3c6140e31'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126345'>#126345</a></li>
<li>Revert "backport commit b72fc5e56579035bf987075e16324ef95ef8e3d4". <a href='http://commits.kde.org/kdelibs/a02df05e4bd083f98147c86f88da2f818fc6c9f4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/355275'>#355275</a></li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Show]</a></h3>
<ul id='ulkdenlive' style='display: none'>
<li>Try to fix render resize crash with Movit. <a href='http://commits.kde.org/kdenlive/164988fd80c90bce637208b547d496a14fc7b004'>Commit.</a> See bug <a href='https://bugs.kde.org/355398'>#355398</a></li>
<li>Fix colored background not displayed in Bézier effect. <a href='http://commits.kde.org/kdenlive/dfa63991e2a59107d192983085d54da0ff8e5d37'>Commit.</a> See bug <a href='https://bugs.kde.org/357659'>#357659</a></li>
<li>Fix curves filter incorrect locale handling. <a href='http://commits.kde.org/kdenlive/5dcde6996f41b171e08b09a94766e0b277a84d11'>Commit.</a> </li>
<li>Fix Bézier spline broken on locales with numerical separator != . <a href='http://commits.kde.org/kdenlive/fe7a8586e7d65578125d40dd2cb45a867cade303'>Commit.</a> See bug <a href='https://bugs.kde.org/357659'>#357659</a></li>
<li>Make sure we can restore the entire folder hierarchy when opening a project. <a href='http://commits.kde.org/kdenlive/b0211b4cc68116cdfb369b78f25a1e15ccddab7b'>Commit.</a> </li>
<li>Fix error in subfolders loading. <a href='http://commits.kde.org/kdenlive/5ef3da9ac26b23f70d028a6850a9b22cc5d18f83'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357572'>#357572</a></li>
<li>Fix crash on opening project with subfolders, real issue still investigated. <a href='http://commits.kde.org/kdenlive/ee4f4c820012d0285b83087c3455da2842c61258'>Commit.</a> See bug <a href='https://bugs.kde.org/357572'>#357572</a></li>
<li>Use sounds from Oxygen theme instead of from kdelibs4 kde-runtime. <a href='http://commits.kde.org/kdenlive/f64f7c9fb2723f514acbf204cc82f394b0c78a16'>Commit.</a> </li>
<li>Fix deprecated color capture in color picker. <a href='http://commits.kde.org/kdenlive/0bc3942b4cef07fae47b67b001bc11d585f6c6c5'>Commit.</a> See bug <a href='https://bugs.kde.org/357516'>#357516</a></li>
<li>Fix razor cursor sometimes not appearing and not adapting to color theme. <a href='http://commits.kde.org/kdenlive/59e35b84d59e29ac4163acae5225f8c7cc4e5d9d'>Commit.</a> See bug <a href='https://bugs.kde.org/357520'>#357520</a></li>
<li>Fix offset when renaming bin item. <a href='http://commits.kde.org/kdenlive/2884e7aecc45e7ff90a08fb90a4d47daa95a512a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351712'>#351712</a></li>
<li>Warn and force project profile must be a multiple of 8. <a href='http://commits.kde.org/kdenlive/b82f237ee83558ddfb2158314e1caae37714fbc2'>Commit.</a> See bug <a href='https://bugs.kde.org/357326'>#357326</a></li>
<li>Update copyright year. <a href='http://commits.kde.org/kdenlive/e5484b9fab1b29f869d820e65cdab412a60e4470'>Commit.</a> </li>
<li>Activate breeze widget style by default if Desktop Session is not using Breeze or Oxygen style. <a href='http://commits.kde.org/kdenlive/dce76bc63f1c8aaaf36ebad03ba07c5045022aab'>Commit.</a> </li>
<li>Save widget style. <a href='http://commits.kde.org/kdenlive/a9ed1e745e51029b2cf64f89f2f490ec0db40918'>Commit.</a> </li>
<li>Backport: new config option to change widget style (useful for non KDE users). <a href='http://commits.kde.org/kdenlive/0a0bc199d4c120703ada82ec35b6e0b07be1c6f5'>Commit.</a> </li>
<li>Fix severe issues with slideshow clips. <a href='http://commits.kde.org/kdenlive/bde554125a70f0587eb0d38b423b649777d7c5be'>Commit.</a> See bug <a href='https://bugs.kde.org/357397'>#357397</a></li>
<li>Fix wrong rounding and other small issues in clip properties display. <a href='http://commits.kde.org/kdenlive/65f5f6544cff77e2016e8003e9ebe051d19dd1f2'>Commit.</a> See bug <a href='https://bugs.kde.org/357322'>#357322</a></li>
<li>Fix manage project profiles broken in project settings. <a href='http://commits.kde.org/kdenlive/7452cedabe8245f5d48f06e901aa68b343d32988'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357324'>#357324</a></li>
<li>Fix wrong bitrate fro DNxHD 115Mb/s. <a href='http://commits.kde.org/kdenlive/95cbd79d9472dd964ff6b7e7a01f9a3f86b97c4a'>Commit.</a> </li>
<li>Fix crash when creating project without audio tracks. <a href='http://commits.kde.org/kdenlive/388e534dee3d63648b91ef483fefc8b9e969c2b6'>Commit.</a> </li>
<li>Use original clip properties, not proxy for effects / display. <a href='http://commits.kde.org/kdenlive/5a6235b120993cc31908847c847bd01fdecd3818'>Commit.</a> See bug <a href='https://bugs.kde.org/355825'>#355825</a>. See bug <a href='https://bugs.kde.org/356643'>#356643</a></li>
<li>Auto-generate proxy clips if they match criteria, correctly en/disable depending on project settings. <a href='http://commits.kde.org/kdenlive/d797d77b661c4f2968e4027d2ce02b007177ef76'>Commit.</a> See bug <a href='https://bugs.kde.org/357207'>#357207</a></li>
<li>Fix monitor not updating after being hidden. <a href='http://commits.kde.org/kdenlive/196e0c94add95c04ea34bf385594b5ec5786d44c'>Commit.</a> </li>
<li>Fix some shortcuts still not working in fullscreen. <a href='http://commits.kde.org/kdenlive/6a61f22724b4d7c4b2810045cf5023cd7044984b'>Commit.</a> </li>
<li>Allow proxy profile management from project settings dialog. <a href='http://commits.kde.org/kdenlive/3895b1f54e7dbf1d93817f7ab86c10c877d9c8bb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356644'>#356644</a></li>
<li>Disable proxy action if project proxies are not enabled. <a href='http://commits.kde.org/kdenlive/923fa40dcc029d7f8e89b74fc231fe914d93fd5e'>Commit.</a> </li>
<li>Fix shortcuts broken when monitor is in fullscreen. <a href='http://commits.kde.org/kdenlive/4b67c238532d637e7c3eb39c3d4dffb60f4faaa1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356803'>#356803</a></li>
<li>Missing icons. <a href='http://commits.kde.org/kdenlive/e85541f9e37d21109be60e8e7c57a8a930046a02'>Commit.</a> </li>
<li>Icon for project settings in config dialog. <a href='http://commits.kde.org/kdenlive/87e410db946b3077c14bde738b28d571bcacab4c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356848'>#356848</a></li>
<li>Move "manage project profiles" option to better place (next to profile list in settings dialog and project settings). <a href='http://commits.kde.org/kdenlive/a71b88f6c05f435b1e7b841077319951b6a45845'>Commit.</a> </li>
<li>Shift + razor tool shows current frame while moving mouse in timeline to help precise cuts. <a href='http://commits.kde.org/kdenlive/d0ce81d070d20d7e142532be6e67829f3c7bcf4f'>Commit.</a> </li>
<li>Fix Movit GLSL crash. <a href='http://commits.kde.org/kdenlive/c870fc5fd87f12a808b0942375d75e766963402a'>Commit.</a> </li>
<li>Use mlt_type instead of string compare to identify MLT service. <a href='http://commits.kde.org/kdenlive/2496e665e256aab4a233fac766dba8667b858929'>Commit.</a> </li>
<li>Fix crash on add/delete track when frei0r cairoblend transition is not available. <a href='http://commits.kde.org/kdenlive/d38f09b584a2ab0d793b64b7fd013e6a2ab82769'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357155'>#357155</a></li>
<li>Fix crash on missing icon. <a href='http://commits.kde.org/kdenlive/f6f43f7f3e64de19cfa2eb328895c267d643a6c4'>Commit.</a> </li>
<li>Update CMake with new ui file location. <a href='http://commits.kde.org/kdenlive/8904948c2e170e382df0cd85710187dd0a8cdbe5'>Commit.</a> </li>
<li>Move Kdenlive xml ui.rc file in a Qt resource so it is found without install. <a href='http://commits.kde.org/kdenlive/a05588ba5ec5185821b637bac0013652b4cb14a8'>Commit.</a> </li>
<li>Remember last folder for dvd projects. <a href='http://commits.kde.org/kdenlive/eab3dc4b29789d0d0d452ac90c13d0a19254d19d'>Commit.</a> See bug <a href='https://bugs.kde.org/96281'>#96281</a></li>
<li>Don't accept invalid cuts. <a href='http://commits.kde.org/kdenlive/333c35148df2d2152f7d01dee2e3a681233b1f90'>Commit.</a> </li>
<li>Fix corruption and freeze on razor undo. <a href='http://commits.kde.org/kdenlive/4d4e7fd96375dd7a986968ba048ffd1e159f04ab'>Commit.</a> See bug <a href='https://bugs.kde.org/357054'>#357054</a></li>
<li>Fix transitions left after track deletion. <a href='http://commits.kde.org/kdenlive/d0a38e91fa43810b7c22082e022e9729e218fb72'>Commit.</a> See bug <a href='https://bugs.kde.org/355380'>#355380</a></li>
<li>Make monitor fullscreen go to correct screen when detached. <a href='http://commits.kde.org/kdenlive/ee9a1c8a2f9c105e920f72b9bdf9693b760eef18'>Commit.</a> </li>
<li>Fix locked track locking wrong transitions on project loading. <a href='http://commits.kde.org/kdenlive/816bccad51fbfa139c8054a1c8b86661e3e97c1d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/355936'>#355936</a></li>
<li>Fix possible crash on audio thumb display. <a href='http://commits.kde.org/kdenlive/ad0f3e58e28651872554678f4b11ab83b04faf62'>Commit.</a> See bug <a href='https://bugs.kde.org/356887'>#356887</a></li>
<li>Fix some memleak and apparently useless mem eating stuff in thumbnailer. <a href='http://commits.kde.org/kdenlive/1ffa3cd31630faa1df864e6d782e732ffcb541f4'>Commit.</a> </li>
<li>Fix missing icons in title editor. <a href='http://commits.kde.org/kdenlive/55cedc93388cfa2950477182100fef4818636dac'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356838'>#356838</a></li>
<li>Fix timeline thumbnail for image/title clips sometimes missing, small optimisations. <a href='http://commits.kde.org/kdenlive/60a5620f11550aaf79230fbbaf9cb9b0a965dddb'>Commit.</a> </li>
<li>Fix resizing on render ressetting frame rate to default 25. <a href='http://commits.kde.org/kdenlive/caf513092be17d72a7117d7dc6d60826728a1b81'>Commit.</a> </li>
<li>Make split effect button checkable to that is is easier to see if enabled. <a href='http://commits.kde.org/kdenlive/409a0b1bb0eb68e10b545342d4296cea0d2f4a2a'>Commit.</a> See bug <a href='https://bugs.kde.org/356495'>#356495</a></li>
<li>Always show the up/down arrows to move effects up and down to have consistent interface. <a href='http://commits.kde.org/kdenlive/27ce510be680ea9b8d34d0ed57b557f7c667a4d5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356766'>#356766</a></li>
<li>Minor update. <a href='http://commits.kde.org/kdenlive/3de228aaead8b3cd02757db0bd0cfb706fa84bb2'>Commit.</a> </li>
<li>Backport Use themed icons in settings dialog. <a href='http://commits.kde.org/kdenlive/40c0494a4c6da4e44a0c0f739a57b095d004d8ac'>Commit.</a> </li>
<li>Use themed icons in settings dialog, new jogshuttle icon from wegwerf. <a href='http://commits.kde.org/kdenlive/63d93bee379773adde3be01f4667a4f8e473c2ac'>Commit.</a> </li>
<li>Backport Use mono spaced font for status bar. <a href='http://commits.kde.org/kdenlive/70363f0cabcb5534ea03ab56a32a0b53603638cb'>Commit.</a> </li>
<li>Use mono spaced font for status bar timecode to avoid jitter. <a href='http://commits.kde.org/kdenlive/7ec5de094559a61b3784c798dcced885e9246bde'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356661'>#356661</a></li>
<li>Hack around undraggable effects (we must hide/show again items to make them draggable). <a href='http://commits.kde.org/kdenlive/a878ec485432dddd24fb7f2bb4c1a73691488cb1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356626'>#356626</a></li>
<li>Backport Fix effects not draggable. <a href='http://commits.kde.org/kdenlive/11066c2247837e3be0c8b7db8838684d7e68e9c0'>Commit.</a> </li>
<li>Fix effects not draggable when switching category and search is not empty. <a href='http://commits.kde.org/kdenlive/9a6291469dfd691f60c2f635129080072aaf07c7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356626'>#356626</a></li>
<li>Fix timeline effects lost on clip reload or proxy. <a href='http://commits.kde.org/kdenlive/60ca33bb013223dd01b9bb31a0514dae3d6e35f5'>Commit.</a> See bug <a href='https://bugs.kde.org/356633'>#356633</a></li>
<li>Fix reloading bin clip duplicating MLT normalizers, resulting in broken aspect ratio. <a href='http://commits.kde.org/kdenlive/0c22012eb2d8e4460a004e5831b5570a900326c4'>Commit.</a> See bug <a href='https://bugs.kde.org/356564'>#356564</a></li>
<li>Backport Fix broken move in timeline. <a href='http://commits.kde.org/kdenlive/6942be334c6e9f4c11e4694d854462f16b1e04ec'>Commit.</a> </li>
<li>Fix broken move in timeline after using context menu. <a href='http://commits.kde.org/kdenlive/fa4a2bdc6329da2aabfe6e8425076765c8105c22'>Commit.</a> See bug <a href='https://bugs.kde.org/356536'>#356536</a></li>
<li>Fix play starting with a few frames offset after pausing. <a href='http://commits.kde.org/kdenlive/0d19b1bb6a6e3d80199838db6deba0e84a0a72d1'>Commit.</a> </li>
<li>Fix default values for force aspect ratio. <a href='http://commits.kde.org/kdenlive/bbadb3b67b4542cd28dced48a944611ad6a08654'>Commit.</a> </li>
<li>Backport Fix audio settings not applied. <a href='http://commits.kde.org/kdenlive/12a6c377cbfd56696481bb6ab6292ab9dac6faef'>Commit.</a> </li>
<li>Fix audio settings not applied. <a href='http://commits.kde.org/kdenlive/9da2ce5a744d9458f4d72fd3e2b238ff0c210fd0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356579'>#356579</a></li>
<li>Backport Improve split effect behavior. <a href='http://commits.kde.org/kdenlive/63ab0bce56190c5fbe6f8abc7908587e5583c5b3'>Commit.</a> </li>
<li>Improve split effect behavior, fix bin clip effects not displayed when adding a new clip. <a href='http://commits.kde.org/kdenlive/89f7cff5ce77c0eb781ddf68f8cce061a5e851ad'>Commit.</a> </li>
<li>Allow building on ubuntu armhf (OpenGL!=OpenGLES). <a href='http://commits.kde.org/kdenlive/f9c12b96228ca397e59dc07ce8f7fbe4d70055c4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/350061'>#350061</a></li>
</ul>
<h3><a name='kdepim' href='https://cgit.kde.org/kdepim.git'>kdepim</a> <a href='#kdepim' onclick='toggle("ulkdepim", this)'>[Show]</a></h3>
<ul id='ulkdepim' style='display: none'>
<li>Fix button order. <a href='http://commits.kde.org/kdepim/86411da59ee6220e32368dce8e1ebf1ed0c4fa18'>Commit.</a> </li>
<li>Fix enable/disable button (fix crash). <a href='http://commits.kde.org/kdepim/447529036c15a70fd89450a092408a4c9c24c6b3'>Commit.</a> </li>
<li>Fix Bug 353393 - Regression kAddressbook 5: Importing vCards from kMail does not work any more. <a href='http://commits.kde.org/kdepim/a82128dffafe0abb466187136ef02ac216dd3632'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/353393'>#353393</a></li>
<li>Fix Bug 357380 - Unable to delete contacts using Del key. <a href='http://commits.kde.org/kdepim/c8dea9f88d766b0caa664711674656b38b288c78'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357380'>#357380</a></li>
<li>Remove them. <a href='http://commits.kde.org/kdepim/a474e3dcbbc30c7298a2486e58678f57387dbb98'>Commit.</a> </li>
<li>Handle signed only inline messages correctly. <a href='http://commits.kde.org/kdepim/a34229dd625ca371f8d905946bcd6106c92b826c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/333611'>#333611</a>. Code review <a href='https://git.reviewboard.kde.org/r/126454'>#126454</a></li>
<li>Remove grantlee_strings_extractor.pyc. <a href='http://commits.kde.org/kdepim/b16307893d611da4468cb481d30a955621ab7275'>Commit.</a> </li>
<li>Reduce update message. <a href='http://commits.kde.org/kdepim/364e6c9b0d9dfbded33e2b94d6fdabb624d603c8'>Commit.</a> </li>
<li>Prepare future 5.1.1. <a href='http://commits.kde.org/kdepim/47e85c313269bddbd35c9b177a69c66f39fbafd9'>Commit.</a> </li>
<li>Avoid to import all subdirectory. (BUG Bug 357060 - Import of Thunderbird mails doesn't work). <a href='http://commits.kde.org/kdepim/ae8c902b698184b62db8b82c250e7a5a779472cc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357060'>#357060</a></li>
<li>Fix Bug 357231 - Untranslatable strings in Kmail welcome message. <a href='http://commits.kde.org/kdepim/f400fe56079349f26c242c23cc83cd0f5c9c31c1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357231'>#357231</a></li>
<li>Fix show i18n. Grantlee doesn't support multi line. <a href='http://commits.kde.org/kdepim/5b8b03e40a3bb14f215a843530c1ee67993262f7'>Commit.</a> See bug <a href='https://bugs.kde.org/357231'>#357231</a></li>
<li>Don't endup in a endless loop for inline signed messages. <a href='http://commits.kde.org/kdepim/38fe083270681910035add8a03d954471160930f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356093'>#356093</a>. Code review <a href='https://git.reviewboard.kde.org/r/126451'>#126451</a></li>
<li>Fix correct signal/slot. <a href='http://commits.kde.org/kdepim/837837f581471c37dc8def6c065e56c00f153782'>Commit.</a> </li>
<li>Fix Bug 356403 - The height of the Contact's menu bar is bigger than that of the rest of the Kontact suite. <a href='http://commits.kde.org/kdepim/a744b6d37c1b72dd5e547e4cbb11cab15580cdae'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356403'>#356403</a></li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Show]</a></h3>
<ul id='ulkdepim-runtime' style='display: none'>
<li>Prepare future 5.1.1. <a href='http://commits.kde.org/kdepim-runtime/ec43e16e14db352148adf9018f2ddb274d72b7cd'>Commit.</a> </li>
</ul>
<h3><a name='kdepimlibs' href='https://cgit.kde.org/kdepimlibs.git'>kdepimlibs</a> <a href='#kdepimlibs' onclick='toggle("ulkdepimlibs", this)'>[Show]</a></h3>
<ul id='ulkdepimlibs' style='display: none'>
<li>Fix build on FreeBSD. <a href='http://commits.kde.org/kdepimlibs/50cb6dea28f4ba86df823a8af9b2c037e05990c8'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126601'>#126601</a></li>
<li>Fix row count reporting in AgentInstanceModel. <a href='http://commits.kde.org/kdepimlibs/f2bd7c14401ef605bc4b9caaec4b049f5ba3bd93'>Commit.</a> </li>
<li>Fix possible crash upon application exits (e.g. kmail). <a href='http://commits.kde.org/kdepimlibs/c6bf33a9018587e96a350bfd0b2bffde1859db27'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126395'>#126395</a></li>
</ul>
<h3><a name='kdiamond' href='https://cgit.kde.org/kdiamond.git'>kdiamond</a> <a href='#kdiamond' onclick='toggle("ulkdiamond", this)'>[Show]</a></h3>
<ul id='ulkdiamond' style='display: none'>
<li>Use sounds from Oxygen theme instead of from kdelibs4 kde-runtime. <a href='http://commits.kde.org/kdiamond/57a5e17aaa3df3cc0f8d87b2c35072e0a8a3b6d5'>Commit.</a> </li>
</ul>
<h3><a name='kfourinline' href='https://cgit.kde.org/kfourinline.git'>kfourinline</a> <a href='#kfourinline' onclick='toggle("ulkfourinline", this)'>[Show]</a></h3>
<ul id='ulkfourinline' style='display: none'>
<li>Revive status bar messages + version bump. <a href='http://commits.kde.org/kfourinline/2553671a31c6333509757607948402e447d096e7'>Commit.</a> </li>
</ul>
<h3><a name='kgpg' href='https://cgit.kde.org/kgpg.git'>kgpg</a> <a href='#kgpg' onclick='toggle("ulkgpg", this)'>[Show]</a></h3>
<ul id='ulkgpg' style='display: none'>
<li>Use SHARE_INSTALL_PREFIX to install appstream data. <a href='http://commits.kde.org/kgpg/f7f61aa2b9524829fa7253234eabb75f9e6d0738'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126470'>#126470</a></li>
</ul>
<h3><a name='khangman' href='https://cgit.kde.org/khangman.git'>khangman</a> <a href='#khangman' onclick='toggle("ulkhangman", this)'>[Show]</a></h3>
<ul id='ulkhangman' style='display: none'>
<li>Make the translators tab in the about dialog appear. <a href='http://commits.kde.org/khangman/4c0a6e660d94794e4205dd320bf21b71d0039062'>Commit.</a> </li>
<li>Rename appdata to match .desktop file. <a href='http://commits.kde.org/khangman/8e000db42bb6727edd611fb57b8d8ba32ae65669'>Commit.</a> </li>
</ul>
<h3><a name='kiriki' href='https://cgit.kde.org/kiriki.git'>kiriki</a> <a href='#kiriki' onclick='toggle("ulkiriki", this)'>[Show]</a></h3>
<ul id='ulkiriki' style='display: none'>
<li>Set window icon. <a href='http://commits.kde.org/kiriki/3ddf686f0087277add4e41b9ac1b68d3616ec780'>Commit.</a> </li>
</ul>
<h3><a name='kiten' href='https://cgit.kde.org/kiten.git'>kiten</a> <a href='#kiten' onclick='toggle("ulkiten", this)'>[Show]</a></h3>
<ul id='ulkiten' style='display: none'>
<li>Call KLocalizedString::setApplicationDomain("kiten");. <a href='http://commits.kde.org/kiten/9de0b24fa318717f83a062ca640f8d492303cbc3'>Commit.</a> </li>
</ul>
<h3><a name='klettres' href='https://cgit.kde.org/klettres.git'>klettres</a> <a href='#klettres' onclick='toggle("ulklettres", this)'>[Show]</a></h3>
<ul id='ulklettres' style='display: none'>
<li>Rename appdata to match .desktop file. <a href='http://commits.kde.org/klettres/bf6484cffaeff7ab90c3c6022c0aa199f759847a'>Commit.</a> </li>
</ul>
<h3><a name='klickety' href='https://cgit.kde.org/klickety.git'>klickety</a> <a href='#klickety' onclick='toggle("ulklickety", this)'>[Show]</a></h3>
<ul id='ulklickety' style='display: none'>
<li>Use KLocalizedString::setApplicationDomain. <a href='http://commits.kde.org/klickety/e3af84f212f1827d3d782dd1dd7b1bf92ffda50f'>Commit.</a> </li>
<li>Fix D-Bus name of KSame Mode. <a href='http://commits.kde.org/klickety/ac50f25127b4c2a490ee95c76837a6be4cafc237'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356742'>#356742</a></li>
</ul>
<h3><a name='kmplot' href='https://cgit.kde.org/kmplot.git'>kmplot</a> <a href='#kmplot' onclick='toggle("ulkmplot", this)'>[Show]</a></h3>
<ul id='ulkmplot' style='display: none'>
<li>Rename appdata to match .desktop file. <a href='http://commits.kde.org/kmplot/97916c1af73cf6e47b5ed54267972cc7854819e7'>Commit.</a> </li>
</ul>
<h3><a name='knetwalk' href='https://cgit.kde.org/knetwalk.git'>knetwalk</a> <a href='#knetwalk' onclick='toggle("ulknetwalk", this)'>[Show]</a></h3>
<ul id='ulknetwalk' style='display: none'>
<li>Fix strings not being translated. <a href='http://commits.kde.org/knetwalk/e6839fff2e43ac822dbf51b3d719b2afe294b940'>Commit.</a> </li>
</ul>
<h3><a name='kollision' href='https://cgit.kde.org/kollision.git'>kollision</a> <a href='#kollision' onclick='toggle("ulkollision", this)'>[Show]</a></h3>
<ul id='ulkollision' style='display: none'>
<li>Use KLocalizedString::setApplicationDomain. <a href='http://commits.kde.org/kollision/dfa93dc176b14085b223a2e696123a6c2a98adc5'>Commit.</a> </li>
</ul>
<h3><a name='kolourpaint' href='https://cgit.kde.org/kolourpaint.git'>kolourpaint</a> <a href='#kolourpaint' onclick='toggle("ulkolourpaint", this)'>[Show]</a></h3>
<ul id='ulkolourpaint' style='display: none'>
<li>Use SHARE_INSTALL_PREFIX to install renamed appstream data. <a href='http://commits.kde.org/kolourpaint/bd47616fa5809fef49ea18b42ac2037ef9454470'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126485'>#126485</a></li>
</ul>
<h3><a name='konsole' href='https://cgit.kde.org/konsole.git'>konsole</a> <a href='#konsole' onclick='toggle("ulkonsole", this)'>[Show]</a></h3>
<ul id='ulkonsole' style='display: none'>
<li>Use sound from Oxygen theme instead of from kde-runtime. <a href='http://commits.kde.org/konsole/2b895fb7f3f3e3654d222c90c90c42103a931726'>Commit.</a> </li>
</ul>
<h3><a name='kopete' href='https://cgit.kde.org/kopete.git'>kopete</a> <a href='#kopete' onclick='toggle("ulkopete", this)'>[Show]</a></h3>
<ul id='ulkopete' style='display: none'>
<li>Prepare for 15.12.1. <a href='http://commits.kde.org/kopete/229334aa76dad974c56304c72ca5be63b1af769b'>Commit.</a> </li>
</ul>
<h3><a name='kpat' href='https://cgit.kde.org/kpat.git'>kpat</a> <a href='#kpat' onclick='toggle("ulkpat", this)'>[Show]</a></h3>
<ul id='ulkpat' style='display: none'>
<li>Use KLocalizedString::setApplicationDomain. <a href='http://commits.kde.org/kpat/f0762745774aa30d4a5988359069a10b3c81a51c'>Commit.</a> </li>
</ul>
<h3><a name='krfb' href='https://cgit.kde.org/krfb.git'>krfb</a> <a href='#krfb' onclick='toggle("ulkrfb", this)'>[Show]</a></h3>
<ul id='ulkrfb' style='display: none'>
<li>Make translations work. <a href='http://commits.kde.org/krfb/ecb9fb8ee0c72c1c6b4a53764aa34e0cb86f47b2'>Commit.</a> </li>
<li>Remove useless I18N_NOOP. <a href='http://commits.kde.org/krfb/2c317584b9987286339dd4057dec6fad775cdb3b'>Commit.</a> </li>
</ul>
<h3><a name='kruler' href='https://cgit.kde.org/kruler.git'>kruler</a> <a href='#kruler' onclick='toggle("ulkruler", this)'>[Show]</a></h3>
<ul id='ulkruler' style='display: none'>
<li>Make the translators tab in the about dialog appear. <a href='http://commits.kde.org/kruler/3546e5322c3824b518b4fab71d4f5e0cbab57bd7'>Commit.</a> </li>
</ul>
<h3><a name='kshisen' href='https://cgit.kde.org/kshisen.git'>kshisen</a> <a href='#kshisen' onclick='toggle("ulkshisen", this)'>[Show]</a></h3>
<ul id='ulkshisen' style='display: none'>
<li>TRANSLATION_DOMAIN is for libraries. <a href='http://commits.kde.org/kshisen/45f6d2d8c509705005b8d73e5bef3916e6df3df6'>Commit.</a> </li>
</ul>
<h3><a name='kstars' href='https://cgit.kde.org/kstars.git'>kstars</a> <a href='#kstars' onclick='toggle("ulkstars", this)'>[Show]</a></h3>
<ul id='ulkstars' style='display: none'>
<li>Use python2 explicitly. Patch by Andreas Sturmlechner. <a href='http://commits.kde.org/kstars/29600383e4605542a497a4702c840f67d47802fd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348503'>#348503</a>. Code review <a href='https://git.reviewboard.kde.org/r/126217'>#126217</a></li>
</ul>
<h3><a name='kteatime' href='https://cgit.kde.org/kteatime.git'>kteatime</a> <a href='#kteatime' onclick='toggle("ulkteatime", this)'>[Show]</a></h3>
<ul id='ulkteatime' style='display: none'>
<li>Make the translators tab in the about dialog appear. <a href='http://commits.kde.org/kteatime/841d885655a49c40041f2857d058f2a70e548839'>Commit.</a> </li>
<li>Use sounds from Oxygen theme instead of from kdelibs4 kde-runtime. <a href='http://commits.kde.org/kteatime/305b9e74bba43724f368a5446a9787ffb3b293c3'>Commit.</a> </li>
</ul>
<h3><a name='ktp-common-internals' href='https://cgit.kde.org/ktp-common-internals.git'>ktp-common-internals</a> <a href='#ktp-common-internals' onclick='toggle("ulktp-common-internals", this)'>[Show]</a></h3>
<ul id='ulktp-common-internals' style='display: none'>
<li>Use sounds from Oxygen theme instead of from kdelibs4 kde-runtime. <a href='http://commits.kde.org/ktp-common-internals/2e2a8f8d3da1569bfd20090cf78f8d4fb1a2781c'>Commit.</a> </li>
</ul>
<h3><a name='ktp-desktop-applets' href='https://cgit.kde.org/ktp-desktop-applets.git'>ktp-desktop-applets</a> <a href='#ktp-desktop-applets' onclick='toggle("ulktp-desktop-applets", this)'>[Show]</a></h3>
<ul id='ulktp-desktop-applets' style='display: none'>
<li>Remove Raised style for hover. <a href='http://commits.kde.org/ktp-desktop-applets/f1cfde4f787d5a31f3d54a9b7b8e6618d116d262'>Commit.</a> </li>
</ul>
<h3><a name='ktp-text-ui' href='https://cgit.kde.org/ktp-text-ui.git'>ktp-text-ui</a> <a href='#ktp-text-ui' onclick='toggle("ulktp-text-ui", this)'>[Show]</a></h3>
<ul id='ulktp-text-ui' style='display: none'>
<li>Fix using alternate shortcut for send message action. <a href='http://commits.kde.org/ktp-text-ui/c85115e9e781504b983ede9ac2a5f209e82dcbfb'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126611'>#126611</a></li>
</ul>
<h3><a name='ktuberling' href='https://cgit.kde.org/ktuberling.git'>ktuberling</a> <a href='#ktuberling' onclick='toggle("ulktuberling", this)'>[Show]</a></h3>
<ul id='ulktuberling' style='display: none'>
<li>Set window icon. <a href='http://commits.kde.org/ktuberling/240d000e50a70ae1bb3516c819ad26b8bc2b28f2'>Commit.</a> </li>
</ul>
<h3><a name='kwalletmanager' href='https://cgit.kde.org/kwalletmanager.git'>kwalletmanager</a> <a href='#kwalletmanager' onclick='toggle("ulkwalletmanager", this)'>[Show]</a></h3>
<ul id='ulkwalletmanager' style='display: none'>
<li>Use KLocalizedString::setApplicationDomain. <a href='http://commits.kde.org/kwalletmanager/0be6950d93ac60de163079c89cc046fbb4537ea9'>Commit.</a> </li>
</ul>
<h3><a name='kwordquiz' href='https://cgit.kde.org/kwordquiz.git'>kwordquiz</a> <a href='#kwordquiz' onclick='toggle("ulkwordquiz", this)'>[Show]</a></h3>
<ul id='ulkwordquiz' style='display: none'>
<li>Install knsrc and notifyrc to the right place(s). <a href='http://commits.kde.org/kwordquiz/8c5b74e12d3e453d5e9f8417c35cfaae34c2a7c3'>Commit.</a> </li>
<li>Rename appdata to match .desktop basename. <a href='http://commits.kde.org/kwordquiz/d163805f4bbfe0fd79ce2b63f8135c0f1257c420'>Commit.</a> </li>
</ul>
<h3><a name='libkmahjongg' href='https://cgit.kde.org/libkmahjongg.git'>libkmahjongg</a> <a href='#libkmahjongg' onclick='toggle("ullibkmahjongg", this)'>[Show]</a></h3>
<ul id='ullibkmahjongg' style='display: none'>
<li>Remove the second install call for the library. <a href='http://commits.kde.org/libkmahjongg/96f04f8e5bdb2bb4409d240573ad47cd9889d444'>Commit.</a> </li>
</ul>
<h3><a name='marble' href='https://cgit.kde.org/marble.git'>marble</a> <a href='#marble' onclick='toggle("ulmarble", this)'>[Show]</a></h3>
<ul id='ulmarble' style='display: none'>
<li>Fixing build error. <a href='http://commits.kde.org/marble/538ab4b92f51dc18fc42138c08f42c7be7c96f72'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126310'>#126310</a></li>
<li>Silence failing unit test for now. <a href='http://commits.kde.org/marble/2f2683a63200def9d6de328375733cde90249490'>Commit.</a> See bug <a href='https://bugs.kde.org/357540'>#357540</a></li>
</ul>
<h3><a name='parley' href='https://cgit.kde.org/parley.git'>parley</a> <a href='#parley' onclick='toggle("ulparley", this)'>[Show]</a></h3>
<ul id='ulparley' style='display: none'>
<li>Rename appdata to match .desktop basename. <a href='http://commits.kde.org/parley/330629376fafe21a4e01a0ac729ebc131c53c2ee'>Commit.</a> </li>
</ul>
<h3><a name='picmi' href='https://cgit.kde.org/picmi.git'>picmi</a> <a href='#picmi' onclick='toggle("ulpicmi", this)'>[Show]</a></h3>
<ul id='ulpicmi' style='display: none'>
<li>Set window icon. <a href='http://commits.kde.org/picmi/0331f86761168ce317e9acf219ea9c819da7eb21'>Commit.</a> </li>
</ul>
<h3><a name='spectacle' href='https://cgit.kde.org/spectacle.git'>spectacle</a> <a href='#spectacle' onclick='toggle("ulspectacle", this)'>[Show]</a></h3>
<ul id='ulspectacle' style='display: none'>
<li>Make the translators tab in the about dialog appear. <a href='http://commits.kde.org/spectacle/320b0e953bc22461a95aef30771561d7fbaf66bc'>Commit.</a> </li>
<li>Properly support QT_DEVICE_PIXEL_RATIO. <a href='http://commits.kde.org/spectacle/3484b8fb09872a0c9a4754c473e11b46544e847e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126479'>#126479</a>. Fixes bug <a href='https://bugs.kde.org/357022'>#357022</a></li>
<li>Remove non-functional send-to-clipboard in background mode. <a href='http://commits.kde.org/spectacle/805d49abf6d3536fa32b9d2eb22342009f5b14e5'>Commit.</a> </li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Show]</a></h3>
<ul id='ulumbrello' style='display: none'>
<li>Add Package path select in Enum properties dialog. <a href='http://commits.kde.org/umbrello/381fd8d27864ac61b90f191040bff90ac96648c6'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126609'>#126609</a>. Fixes bug <a href='https://bugs.kde.org/357442'>#357442</a></li>
<li>Add Associations list page in Enum properties dialog. <a href='http://commits.kde.org/umbrello/67a5df53d21516fc1396c8d199fe719202450f4d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126608'>#126608</a>. Fixes bug <a href='https://bugs.kde.org/357441'>#357441</a></li>
<li>Make string representation of Association more clear. <a href='http://commits.kde.org/umbrello/7ccb26503386bcd5426429a20941feced7e472b3'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126607'>#126607</a>. Fixes bug <a href='https://bugs.kde.org/357439'>#357439</a></li>
<li>Fix compilation warning in TEST_pythonwriter.cpp. <a href='http://commits.kde.org/umbrello/832f076d3b78dc0d43faad1fb9b439a30377907f'>Commit.</a> </li>
<li>Fix krazy2 issue 'Check for C++ ctors that should be declared 'explicit'. <a href='http://commits.kde.org/umbrello/37bb18b02b384d98e5e113f14dee6b46487258e1'>Commit.</a> </li>
<li>Add arcanist config. <a href='http://commits.kde.org/umbrello/98b837812fd7b5b05879086bfd12a9a32df0f4d6'>Commit.</a> </li>
<li>Revert "Fix compilation for Frameworks". <a href='http://commits.kde.org/umbrello/09d733802aa2a546b997c04a1802df384e8ba282'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126547'>#126547</a></li>
<li>Main.cpp: Drop unnecessary header. <a href='http://commits.kde.org/umbrello/4260a2f010ef72afb3bf02ba70183d88844914df'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126351'>#126351</a></li>
<li>Add kde reviewboard config. <a href='http://commits.kde.org/umbrello/0316efe0d3d1dc0f990552e414ba4f8a7bdcdbf4'>Commit.</a> </li>
<li>Fix 'No encoding specified in generated python files'. <a href='http://commits.kde.org/umbrello/f1e25ab4afb56a363ec25500c0aacbfa203e3fef'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356949'>#356949</a></li>
<li>Try to fix bug Qt5 based compiling test case problems on jenkins CI system by using QVERIFY instead of QCOMPARE. <a href='http://commits.kde.org/umbrello/f996593e44f11901b63c0d913c28b59886a48a0c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351525'>#351525</a></li>
<li>Display used Qt version on configure time. <a href='http://commits.kde.org/umbrello/466f5583eddd62a067dd77d2525e966611427892'>Commit.</a> </li>
<li>Another try to fix jenkins CI build issue. <a href='http://commits.kde.org/umbrello/637efcae299d96a4131dfca86402f30493b200d0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351525'>#351525</a></li>
<li>Generic fix for compiling test apps on kde jenkins CI. <a href='http://commits.kde.org/umbrello/132d4498dcc73b25f95045b8fa0d9daff22a3977'>Commit.</a> </li>
<li>Fix compilation for Frameworks. <a href='http://commits.kde.org/umbrello/f181430988752c2c9c0782f7d0383a027ebe0128'>Commit.</a> </li>
<li>Fix icon installation properly for KDE4. <a href='http://commits.kde.org/umbrello/82a2ed3a7b1a8aa73c10f5e5ba47de28dca4434d'>Commit.</a> </li>
</ul>

