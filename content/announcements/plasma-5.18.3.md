---
title: KDE Plasma 5.18.3, Bugfix Release for March
release: "plasma-5.18.3"
version: "5.18.3"
description: KDE Ships Plasma 5.18.3
date: 2020-03-10
layout: plasma
changelog: plasma-5.18.2-5.18.3-changelog
---

{{< peertube "cda402b5-2bcb-4c0c-b232-0fa5a4dacaf5" >}}

{{<figure src="/announcements/plasma-5.18/plasma-5.18.png" alt="Plasma 5.18" class="text-center" width="600px" caption="KDE Plasma 5.18">}}

Tuesday, 10 March 2020.

{{% i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s" "5.18.3." %}}

{{% i18n_var "<a href='https://kde.org/announcements/plasma-%[1]s.0'>Plasma %[1]s</a> was released in February 2020 with many feature refinements and new modules to complete the desktop experience." "5.18" %}}

This release adds a fortnight's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- libkscreen: handle when backend fails to load/initialize. <a href="https://commits.kde.org/libkscreen/ff98585ea5541012b68604e34b7fec383a487cd9">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D27625">D27625</a>
- Discover Flatpak: build with older libflatpaks. <a href="https://commits.kde.org/discover/5cae00d1dbd94a584c9c63f7ff7fb5f893b228e4">Commit.</a>
- Discover: Make sure we don't crash. <a href="https://commits.kde.org/discover/1b0992a5375f2243d1c8fdb2ac5efdb991d619bd">Commit.</a>
