---
title: KDE Ships KDE Applications 16.08.3
description: KDE Ships KDE Applications 16.08.3
date: 2016-11-10
version: 16.08.3
layout: application
changelog: fulllog_applications-16.08.3
---

{{% i18n_var "November 10, 2016. Today KDE released the third stability update for <a href='%[1]s'>KDE Applications 16.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../announce-applications-16.08.0" %}}

More than 20 recorded bugfixes include improvements to kdepim, ark, okteta, umbrello, kmines, among others.

{{% i18n_var "This release also includes Long Term Support version of KDE Development Platform %[1]s." "4.14.26" %}}
