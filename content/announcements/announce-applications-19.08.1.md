---
title: KDE Ships Applications 19.08.1
major_version: "19.08"
version: '19.08.1'
release: 'applications-19.08.1'
description: KDE Ships Applications 19.08.1.
date: 2019-09-05
layout: application
changelog: fulllog_applications-19.08.1
---

September 05, 2019.

{{% i18n_var "Today KDE released the first stability update for <a href='%[1]s'>KDE Applications %[2]s</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../announce-applications-19.08.0" "19.08" %}}

More than twenty recorded bugfixes include improvements to Kontact, Dolphin, Kdenlive, Konsole, Step, among others.

Improvements include:

- Several regressions in Konsole's tab handling have been fixed
- Dolphin again starts correctly when in split-view mode
- Deleting a soft body in the Step physics simulator no longer causes a crash
