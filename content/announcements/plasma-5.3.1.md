---
title: KDE Ships Plasma 5.3.1, Bugfix Release for May
description: KDE Ships Plasma 5.3.1
date: 2015-05-26
release: plasma-5.3.1
layout: plasma
changelog: plasma-5.3.0-5.3.1-changelog
---

{{<figure src="/announcements/plasma-5.3/plasma-5.3.png" alt="Plasma 5.3 " class="text-center" width="600px" caption="Plasma 5.3">}}

Tuesday, 26 May 2015.

Today KDE releases a bugfix update to Plasma 5, versioned 5.3.1. <a href='https://www.kde.org/announcements/plasma-5.3.0.php'>Plasma 5.3</a> was released in January with many feature refinements and new modules to complete the desktop experience.

This release adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important for example:

- <a href="https://sessellift.wordpress.com/2015/05/14/in-free-software-its-okay-to-be-imperfect-as-long-as-youre-open-and-honest-about-it/">show desktop</a> has now been fixed
