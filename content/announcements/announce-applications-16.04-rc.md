---
title: KDE Ships Release Candidate of KDE Applications 16.04
release: "applications-16.03.90"
description: KDE Ships Plasma 5.6.2.
date: 2016-04-05
---

April 7, 2016. Today KDE released the release candidate of the new versions of KDE Applications. With dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing.

With the various applications being based on KDE Frameworks 5, the KDE Applications 16.04 the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the team by installing the release <a href='https://bugs.kde.org/'>and reporting any bugs</a>.

#### Installing KDE Applications 16.04 Release Candidate Binary Packages

<em>Packages</em>.
Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications 16.04 Release Candidate (internally 16.03.90) for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.

<em>Package Locations</em>.
For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages'>Community Wiki</a>.

#### Compiling KDE Applications 16.04 Release Candidate

The complete source code for KDE Applications 16.04 Release Candidate may be <a href='http://download.kde.org/unstable/applications/16.03.90/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='../../info/applications-16.03.90'>KDE Applications Release Candidate Info Page</a>.

#### Supporting KDE

KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}
