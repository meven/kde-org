---
title: Release of KDE Frameworks 5.30.0
description: KDE Ships Frameworks 5.30.0
date: 2017-01-14
version: 5.30.0
layout: framework
---

{{<figure src="/announcements/frameworks5TP/KDE_QT.jpg" >}}
January 14, 2017. KDE today announces the release of KDE Frameworks 5.30.0.

KDE Frameworks are 70 addon libraries to Qt which provide a wide variety of commonly needed functionality in mature, peer reviewed and well tested libraries with friendly licensing terms. For an introduction see <a href='http://kde.org/announcements/kde-frameworks-5.0.php'>the Frameworks 5.0 release announcement</a>.

This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.

## New in this Version

### Breeze Icons

- add Haguichi icons from Stephen Brandt (thanks)
- finalize calligra icon support
- add cups icon thanks colin (bug 373126)
- update kalarm icon (bug 362631)
- add krfb app icon (bug 373362)
- add r mimetype support (bug 371811)
- and more

### Extra CMake Modules

- appstreamtest: handle non-installed programs
- Enable colored warnings in ninja's output
- Fix missing :: in API docs to trigger code styling
- Ignore host libs/includes/cmakeconfig files in Android toolchain
- Document usage of gnustl_shared with Android toolchain
- Never use -Wl,--no-undefined on Mac (APPLE)

### Framework Integration

- Improve KPackage KNSHandler
- Search for the more precise required version of AppstreamQt
- Add KNewStuff support for KPackage

### KActivitiesStats

- Make compile with -fno-operator-names
- Do not fetch more linked resources when one of them goes away
- Added model role to retrieve the list of linked activities for a resource

### KDE Doxygen Tools

- add Android to the list of available platforms
- include ObjC++ source files

### KConfig

- Generate an instance with KSharedConfig::Ptr for singleton and arg
- kconfig_compiler: Use nullptr in generated code

### KConfigWidgets

- Hide the "Show Menu Bar" action if all the menubars are native
- KConfigDialogManager: drop kdelibs3 classes

### KCoreAddons

- Return stringlist/boolean types in KPluginMetaData::value
- DesktopFileParser: Honor ServiceTypes field

### KDBusAddons

- Add python bindings for KDBusAddons

### KDeclarative

- Introduce org.kde.kconfig QML import with KAuthorized

### KDocTools

- kdoctools_install: match the full path for the program (bug 374435)

### KGlobalAccel

- [runtime] Introduce a KGLOBALACCEL_TEST_MODE env variable

### KDE GUI Addons

- Add Python bindings for KGuiAddons

### KHTML

- Set plugin data so that the Embeddable Image Viewer works

### KIconThemes

- Inform QIconLoader also when the desktop icon theme is changed (bug 365363)

### KInit

- Take X-KDE-RunOnDiscreteGpu property into account when starting app using klauncher

### KIO

- KIO::iconNameForUrl will now return special icons for xdg locations like the user's Pictures folder
- ForwardingSlaveBase: fix passing of Overwrite flag to kio_desktop (bug 360487)
- Improve and export KPasswdServerClient class, the client API for kpasswdserver
- [KPropertiesDialog] Kill "Place in system tray" option
- [KPropertiesDialog] Don't change "Name" of "Link" .desktop files if file name is read-only
- [KFileWidget] Use urlFromString instead of QUrl(QString) in setSelection (bug 369216)
- Add option to run an app on a discrete graphics card to KPropertiesDialog
- terminate DropJob properly after dropping to an executable
- Fix logging category usage on Windows
- DropJob: emit started copy job after creation
- Add support for calling suspend() on a CopyJob before it gets started
- Add support for suspending jobs immediately, at least for SimpleJob and FileCopyJob

### KItemModels

- Update proxies for recently realised class of bugs (layoutChanged handling)
- Make it possible for KConcatenateRowsProxyModel to work in QML
- KExtraColumnsProxyModel: Persist model indexes after emitting layoutChange, not before
- Fix assert (in beginRemoveRows) when deselecting an empty child of a selected child in korganizer

### KJobWidgets

- Don't focus progress windows (bug 333934)

### KNewStuff

- [GHNS Button] Hide when KIOSK restriction applies
- Fix set up of the ::Engine when created with an absolute config file path

### KNotification

- Add a manual test for Unity launchers
- [KNotificationRestrictions] Let user can specify restriction reason string

### KPackage Framework

- [PackageLoader] Don't access invalid KPluginMetadata (bug 374541)
- fix description for option -t in man page
- Improve error message
- Fix the help message for --type
- Improve installation process of KPackage bundles
- Install a kpackage-generic.desktop file
- Exclude qmlc files from installation
- Don't list separately plasmoids from metadata.desktop and .json
- Additional fix for packages with different types but same ids
- Fix cmake failure when two packages with different type have same id

### KParts

- Call the new checkAmbiguousShortcuts() from MainWindow::createShellGUI

### KService

- KSycoca: don't follow symlink to directories, it creates a risk of recursion

### KTextEditor

- Fix: Forward dragging text results in wrong selection (bug 374163)

### KWallet Framework

- Specify minimum required GpgME++ version 1.7.0
- Revert "If Gpgmepp is not found, try to use KF5Gpgmepp"

### KWidgetsAddons

- Add python bindings
- Add KToolTipWidget, a tooltip that contains another widget
- Fix KDateComboBox checks for valid entered dates
- KMessageWidget: use darker red color when type is Error (bug 357210)

### KXMLGUI

- Warn on startup about ambiguous shortcuts (with an exception for Shift+Delete)
- MSWin and Mac have similar autosave windowsize behaviour
- Display application version in about dialog header (bug 372367)

### Oxygen Icons

- sync with breeze icons

### Plasma Framework

- Fix the renderType properties for various components
- [ToolTipDialog] Use KWindowSystem::isPlatformX11() which is cached
- [Icon Item] Fix updating implicit size when icon sizes change
- [Dialog] Use setPosition / setSize instead of setting everything individually
- [Units] Make iconSizes property constant
- There is now a global "Units" instance reducing memory consumption and creation time of SVG items
- [Icon Item] Support non-square icons (bug 355592)
- search/replace old hardcoded types from plasmapkg2 (bug 374463)
- Fix X-Plasma-Drop* types (bug 374418)
- [Plasma ScrollViewStyle] Show scroll bar background only on hover
- Fix #374127 misplacement of popups from dock wins
- Deprecate Plasma::Package API in PluginLoader
- Recheck which representation we should using in setPreferredRepresentation
- [declarativeimports] Use QtRendering on phone devices
- consider an empty panel always "applets loaded" (bug 373836)
- Emit toolTipMainTextChanged if it changes in response to a title change
- [TextField] Allow disabling reveal password button through KIOSK restriction
- [AppletQuickItem] Support launch error message
- Fix logic for arrow handling in RTL locales (bug 373749)
- TextFieldStyle: Fix implicitHeight value so the text cursor is centered

### Sonnet

- cmake: look for hunspell-1.6 as well

### Syntax Highlighting

- Fix highlighting of Makefile.inc by adding priority to makefile.xml
- Highlight SCXML files as XML
- makefile.xml: many improvements, too long to list here
- python syntax: added f-literals and improved string handling

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.

