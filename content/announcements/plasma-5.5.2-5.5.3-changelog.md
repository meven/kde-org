---
title: Plasma 5.5.3 Complete Changelog
version: 5.5.3
hidden: true
plasma: true
type: fulllog
---

### <a name='breeze' href='http://quickgit.kde.org/?p=breeze.git'>Breeze</a>

- For standard configuration, rely on KCoreConfigSkeleton::load and ::save. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=7395b3ac024d8390af69acea94c673388a9e729a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357135'>#357135</a>
- Call updateButtonGeometry after decorationButtonsLeftChanged and decorationButtonsRightChanged. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=768ae398483060740137331d4483aa4ecd4e005e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356869'>#356869</a>

### <a name='breeze-gtk' href='http://quickgit.kde.org/?p=breeze-gtk.git'>Breeze GTK</a>

- Relax cmake version requirement to rest of plasma. <a href='http://quickgit.kde.org/?p=breeze-gtk.git&amp;a=commit&amp;h=28b715309d1d602bce24e48732bd74f3a762f2eb'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126477'>#126477</a>

### <a name='khotkeys' href='http://quickgit.kde.org/?p=khotkeys.git'>KDE Hotkeys</a>

- Add vertical stretch to prevent DBus shortcut configuration from expanding. <a href='http://quickgit.kde.org/?p=khotkeys.git&amp;a=commit&amp;h=bdbe86aeb09ea1ac0f76104ec24fa61a69972bb2'>Commit.</a>
- Fix comparison of QString to QKeySequence. <a href='http://quickgit.kde.org/?p=khotkeys.git&amp;a=commit&amp;h=41697631fcca2be04598cf7ed8c0ce8a926fa1ad'>Commit.</a>

### <a name='oxygen' href='http://quickgit.kde.org/?p=oxygen.git'>Oxygen</a>

- Fixed compilation for kde4. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=1b892a2bf8ec2181bb9b45b321c5c0aacc57c615'>Commit.</a>
- Removed useless oxygenutil. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=8793c22d3e658f2102f94d14a5d6504aaaebc7c6'>Commit.</a>
- For standard configuration, rely on KCoreConfigSkeleton::load and ::save. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=1dcaee368040692bf6de840465d40fe00a76f899'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357135'>#357135</a>
- Call updateButtonGeometry after decorationButtonsLeftChanged and decorationButtonsRightChanged. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=6afe7689a030c00351fb80cffacbf8c63691fee8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356869'>#356869</a>

### <a name='plasma-desktop' href='http://quickgit.kde.org/?p=plasma-desktop.git'>Plasma Desktop</a>

- Fix icon hover effect breaking after Dashboard was used. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=c35aa318d417b4fcb5b36324635de6b8e76cbaf1'>Commit.</a>
- Use Oxygen sound instead of sound from kdelibs4's kde-runtime. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=4cac96f7834b63ced12618475afd37644ab9e243'>Commit.</a>
- Use the right graphics for min and max sliders. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=426a24c051d187875abc0e373cdc252f151b1d21'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356790'>#356790</a>. Code review <a href='https://git.reviewboard.kde.org/r/126498'>#126498</a>
- Fix bug 356946 - user avatar in Phonon isn't shrinked. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=7a7f171cdd677c433b7c0966550badbfeb3d6b1e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126438'>#126438</a>. Fixes bug <a href='https://bugs.kde.org/356946'>#356946</a>
- Don't instanciate thumbnail delegates when tooltips are off. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ae00bbea02dd4b245efc00571840ecdbc7d3da4c'>Commit.</a>

### <a name='plasma-workspace' href='http://quickgit.kde.org/?p=plasma-workspace.git'>Plasma Workspace</a>

- [KUIserver] Return the proper value. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=80ccb1c88d41752fb41032e9c3f88360281571a3'>Commit.</a>
- [notifications] Refactor the screen handling code. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=0c6b354b7e22297544f1d37608d6fdcd777c4d52'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126408'>#126408</a>. See bug <a href='https://bugs.kde.org/353966'>#353966</a>. See bug <a href='https://bugs.kde.org/356461'>#356461</a>
- Set root context properties on the right context. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=c692bc53a8c5cf5802084085b9aed11203998d38'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356545'>#356545</a>. Fixes bug <a href='https://bugs.kde.org/355885'>#355885</a>. Fixes bug <a href='https://bugs.kde.org/356916'>#356916</a>. Code review <a href='https://git.reviewboard.kde.org/r/126491'>#126491</a>
- Use proper deleters for libxcb structs. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a14a7d39c41bd3280dcc56dcd97e846e0278e812'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126512'>#126512</a>
- Guard against applet failing to load in systemtray task. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e8c6426ac12c14be68a68b9492274140c28f3285'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356470'>#356470</a>. Code review <a href='https://git.reviewboard.kde.org/r/126497'>#126497</a>
- Destroy the container window we create. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=198400e3bdaaeb82403ee3c90c45bf3eae3ad5c2'>Commit.</a>
