---
title: KDE Plasma 5.18.2, bugfix Release for February
release: "plasma-5.18.2"
version: "5.18.2"
description: KDE Ships Plasma 5.18.2
date: 2020-02-25
layout: plasma
changelog: plasma-5.18.1-5.18.2-changelog
---

{{< peertube "cda402b5-2bcb-4c0c-b232-0fa5a4dacaf5" >}}

{{<figure src="/announcements/plasma-5.18/plasma-5.18.png" alt="Plasma 5.18" class="text-center" width="600px" caption="KDE Plasma 5.18">}}

Tuesday, 25 February 2020.

{{% i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s" "5.18.2." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in February 2020 with many feature refinements and new modules to complete the desktop experience." "5.18" %}}

This release adds a week's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Discover: fix build on old flatpak versions. <a href="https://commits.kde.org/discover/7ff2de8d54ae749a142856c440816e764bfe5628">Commit.</a>
- Unify KSysGuard cpu clock speed names. <a href="https://commits.kde.org/ksysguard/4e656a45df16565e4273ae67d8dc4d530b5ca488">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D26857">D26857</a>
- Emojier: improve the fallback mechanism to detect languages. <a href="https://commits.kde.org/plasma-desktop/358e98a75a946abe76ffdfeddd0156483a66d4b3">Commit.</a> Fixes bug <a href="https://bugs.kde.org/417713">#417713</a>
