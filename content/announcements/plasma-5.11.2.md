---
title: KDE Plasma 5.11.2, Bugfix Release for October
release: plasma-5.11.2
version: 5.11.2
description: KDE Ships 5.11.2
date: 2017-10-24
layout: plasma
changelog: plasma-5.11.1-5.11.2-changelog
---

{{%youtube id="nMFDrBIA0PM"%}}

{{<figure src="/announcements/plasma-5.11/plasma-5.11.png" alt="KDE Plasma 5.11 " class="text-center" width="600px" caption="KDE Plasma 5.11">}}

Thursday, 24 October 2017.

{{% i18n_var "Today KDE releases a %[1]s update to KDE Plasma 5, versioned %[2]s" "Bugfix" "5.11.2." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in %[2]s with many feature refinements and new modules to complete the desktop experience." "5.11" "October" %}}

{{% i18n_var "This release adds a %[1]s worth of new translations and fixes from KDE's contributors.  The bugfixes are typically small but important and include:" "week's" %}}

- Fix colours not updating in systemsettings. <a href="https://commits.kde.org/systemsettings/5f9243a8bb9f7dccc60fc1514a866095c22801b8">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D8399">D8399</a>
- Default X font DPI to 96 on wayland. <a href="https://commits.kde.org/plasma-desktop/fae658ae90bf855b391061a5332a1a964045e914">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/8287">8287</a>
- Kcm baloo: Fix extraction of folder basename for error message. <a href="https://commits.kde.org/plasma-desktop/5a8691900fea2b77ae194f509d17e7368235b4c1">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D8325">D8325</a>
- Properly access the system's GTK settings. <a href="https://commits.kde.org/kde-gtk-config/efa8c4df5b567d382317bd6f375cd1763737ff95">Commit.</a> Fixes bug <a href="https://bugs.kde.org/382291">#382291</a>.
