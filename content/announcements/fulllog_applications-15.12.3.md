---
title: KDE Applications 15.12.3 Full Log Page
type: fulllog
version: 15.12.3
hidden: true
---

<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Show]</a></h3>
<ul id='ulakonadi' style='display: none'>
<li>Clean orphaned relations when upgrading DB schema to r31. <a href='http://commits.kde.org/akonadi/989f7eba0d1b093e00730a8444a9c088a3805ffd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354536'>#354536</a></li>
<li>Drop mysql.conf option removed in MySQL 5.7. <a href='http://commits.kde.org/akonadi/9a9f7eaa38023f70c6fa85a87359a487ccf7a48c'>Commit.</a> </li>
</ul>
<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Show]</a></h3>
<ul id='ulark' style='display: none'>
<li>Use containing-folder name when packing multiple selected files. <a href='http://commits.kde.org/ark/80080c9e8eb2d4c51a1195190182026e0a78a035'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/279861'>#279861</a></li>
<li>Bring back the quick-extract menu. <a href='http://commits.kde.org/ark/0dbbf028c665e775601328b3f0cdbd9c0fcd1e94'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357225'>#357225</a></li>
<li>Cli7z: Fix detection of password prompt for p7zip 15.09. <a href='http://commits.kde.org/ark/26cf274c5bae04b90f526005a2514b16564c6df3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358675'>#358675</a></li>
<li>Fixuifiles. <a href='http://commits.kde.org/ark/b63561e5994e130ad82c26cbb77b2ac4e69f9d07'>Commit.</a> </li>
<li>Fix regressions with extraction of selected entries. <a href='http://commits.kde.org/ark/952c82da933f0aff3b53846194e1d229cd542f9c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359628'>#359628</a></li>
<li>Properly restore window size of CreateDialog. <a href='http://commits.kde.org/ark/6f9e1d11333a9bd71be1170b4fd0923c5517e995'>Commit.</a> </li>
<li>Fix stale tooltip in CreateDialog. <a href='http://commits.kde.org/ark/fff01c83f9ab1e7c0a7c83e0d1694a6cd805b09d'>Commit.</a> </li>
</ul>
<h3><a name='dragon' href='https://cgit.kde.org/dragon.git'>dragon</a> <a href='#dragon' onclick='toggle("uldragon", this)'>[Show]</a></h3>
<ul id='uldragon' style='display: none'>
<li>Guard against unexpected sample size when drawing the analyzer. <a href='http://commits.kde.org/dragon/edae15e4920fa181e8f7f8ecc38b338b7b5a9c92'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127097'>#127097</a></li>
<li>On inactive state changes switch away from loadview and vice versa. <a href='http://commits.kde.org/dragon/f047b3579bc3bfee2959078ba5c89d7538cbc364'>Commit.</a> </li>
<li>Make sure to force dragon out of fullscreen on track ends. <a href='http://commits.kde.org/dragon/dcd72df864b423093e9c5dda18c1d673dc53f021'>Commit.</a> </li>
<li>Restore override cursor. <a href='http://commits.kde.org/dragon/5a1b0547c47660836038e7b3245d8c90b3f65e15'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127095'>#127095</a></li>
<li>Repair unique app argument parsing by assuming input referes to local. <a href='http://commits.kde.org/dragon/e904ce4973db6896842d751d1af379171be14dac'>Commit.</a> </li>
<li>Multiple instances support. <a href='http://commits.kde.org/dragon/5b6e4fb1fa63ac38e4076a2fd1dea9a9abd4c0f7'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127082'>#127082</a></li>
<li>Seekable, prev, next actions. <a href='http://commits.kde.org/dragon/1d5d130ecf47a01f9620faac1e7d2b3fc2940ad6'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127081'>#127081</a></li>
<li>Analyzer in audio view. <a href='http://commits.kde.org/dragon/a46548d3a7b4f7835409681f8800a3e99b48d003'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127065'>#127065</a></li>
</ul>
<h3><a name='kate' href='https://cgit.kde.org/kate.git'>kate</a> <a href='#kate' onclick='toggle("ulkate", this)'>[Show]</a></h3>
<ul id='ulkate' style='display: none'>
<li>Ctags plugin: Fix wrong porting code. <a href='http://commits.kde.org/kate/46d3446c04d9054e8ad6a568ee3b60ba9ff51e4c'>Commit.</a> </li>
<li>Fix mouse support in quick open: Always use column 0 to read model data. <a href='http://commits.kde.org/kate/6f1967bfb7634d2634a87b3b843ead64e8a44089'>Commit.</a> </li>
</ul>
<h3><a name='kblocks' href='https://cgit.kde.org/kblocks.git'>kblocks</a> <a href='#kblocks' onclick='toggle("ulkblocks", this)'>[Show]</a></h3>
<ul id='ulkblocks' style='display: none'>
<li>Fix unplayable game when changing difficulty. <a href='http://commits.kde.org/kblocks/145820a70ce12409873e9326c8c232a790c76081'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/339038'>#339038</a>. Code review <a href='https://git.reviewboard.kde.org/r/127171'>#127171</a></li>
</ul>
<h3><a name='kcalc' href='https://cgit.kde.org/kcalc.git'>kcalc</a> <a href='#kcalc' onclick='toggle("ulkcalc", this)'>[Show]</a></h3>
<ul id='ulkcalc' style='display: none'>
<li>Fix click event for constant buttons. <a href='http://commits.kde.org/kcalc/a4dfbe234f8a6d1a052c36e535be60f030bf35ca'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/353644'>#353644</a>. Code review <a href='https://git.reviewboard.kde.org/r/127017'>#127017</a></li>
</ul>
<h3><a name='kde-baseapps' href='https://cgit.kde.org/kde-baseapps.git'>kde-baseapps</a> <a href='#kde-baseapps' onclick='toggle("ulkde-baseapps", this)'>[Show]</a></h3>
<ul id='ulkde-baseapps' style='display: none'>
<li>Docu update: point to the non-deprecated replacement. <a href='http://commits.kde.org/kde-baseapps/0e373505d88cf178cdc2aa0cec62a6df91ae23e0'>Commit.</a> </li>
</ul>
<h3><a name='kdelibs' href='https://cgit.kde.org/kdelibs.git'>kdelibs</a> <a href='#kdelibs' onclick='toggle("ulkdelibs", this)'>[Show]</a></h3>
<ul id='ulkdelibs' style='display: none'>
<li>Limit string memory use. <a href='http://commits.kde.org/kdelibs/1d247d8cd19f3cdd72baf610bd7c7cdc8f83a1de'>Commit.</a> </li>
<li>Don't pass a negative number to dbus, it asserts in libdbus. <a href='http://commits.kde.org/kdelibs/e4ba2e7735201bc253056e366149e4bd773271c2'>Commit.</a> </li>
<li>KWallet::openWallet(Synchronous) : don't time out after 25 seconds. <a href='http://commits.kde.org/kdelibs/2073ebd052a75cfadcd924a4824795968cd568a6'>Commit.</a> </li>
<li>Disable test that fails in CI. It's the test that has a problem, not the underlying code anyway. <a href='http://commits.kde.org/kdelibs/0a3ae80297861573b125ab831b7a49f49bedef0b'>Commit.</a> </li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Show]</a></h3>
<ul id='ulkdenlive' style='display: none'>
<li>Fix typo x246 > x264, patch by Nicolas F. <a href='http://commits.kde.org/kdenlive/ea07ce9d67d7049b2b4aeb4e75b84fe953008243'>Commit.</a> </li>
<li>Fix inverted colors in scopes. <a href='http://commits.kde.org/kdenlive/ad4f2bd2e51338c17c896ce4712d6c6795ac67a5'>Commit.</a> See bug <a href='https://bugs.kde.org/359541'>#359541</a></li>
</ul>
<h3><a name='kdepim' href='https://cgit.kde.org/kdepim.git'>kdepim</a> <a href='#kdepim' onclick='toggle("ulkdepim", this)'>[Show]</a></h3>
<ul id='ulkdepim' style='display: none'>
<li>Don't share mailto. <a href='http://commits.kde.org/kdepim/92295d5b4116db0ed8296cee503846b1fbb5920b'>Commit.</a> </li>
<li>Fix reactivate quotelevel. <a href='http://commits.kde.org/kdepim/4fedf87cd267db36ad49fce0254442abd2cbd81f'>Commit.</a> </li>
<li>Remove double margin. <a href='http://commits.kde.org/kdepim/be93c90046e53700b788a4d8f5339d3f85dbf417'>Commit.</a> </li>
<li>Remove margin here too. <a href='http://commits.kde.org/kdepim/0c48a14dc2fbeb9f6bda73a3b004bbcbc2405e5d'>Commit.</a> </li>
<li>Remove double margin. <a href='http://commits.kde.org/kdepim/fdf71718a19276795118a77ff7eff2dc875353e3'>Commit.</a> </li>
<li>Remove margin. <a href='http://commits.kde.org/kdepim/c7ff0c6deaabbff8e796cdcb3fb6e5e4b766853f'>Commit.</a> </li>
<li>Remove duplicate margin. <a href='http://commits.kde.org/kdepim/ffcf4a2deebd3e3905bb36f02bd756c2e324a89d'>Commit.</a> </li>
<li>Fix enable/disable button when we initialize translator widget. <a href='http://commits.kde.org/kdepim/0365694947c10aa031fe395c46e80111995b4c01'>Commit.</a> </li>
<li>Remove double margin. <a href='http://commits.kde.org/kdepim/46a49f4a8c46aaac5e1a61365f1731e7c6d1bc89'>Commit.</a> </li>
<li>Fix Bug 359895 - Labels are misaligned in the Redirect Message dialog. <a href='http://commits.kde.org/kdepim/ab5debc292e3b8f1d2c740b0ee1d3e0f78712371'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359895'>#359895</a></li>
<li>Remove this unused action. Fix action name. <a href='http://commits.kde.org/kdepim/278e261bc3a7016de16489ca6a132bbcba1dbb2f'>Commit.</a> See bug <a href='https://bugs.kde.org/358840'>#358840</a></li>
<li>Fix Bug 359872 - Big icon when trying to drag a attached pdf file. <a href='http://commits.kde.org/kdepim/e98c642987e8d9b45e6b08d325a1ef202e116ff3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359872'>#359872</a></li>
<li>Fix Bug 358855 - message list column widths lost when program closed. <a href='http://commits.kde.org/kdepim/2b118bef6de176464e4838b56e16e6d6c77a6fe4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358855'>#358855</a></li>
<li>Remove double margin. <a href='http://commits.kde.org/kdepim/b96036a85f0cbad7f5458b9f63a0f01f06765228'>Commit.</a> </li>
<li>Don't download all the time list. <a href='http://commits.kde.org/kdepim/31c0741a7f3f2633cbf3ab77b09a4106ca4f690c'>Commit.</a> </li>
<li>Backport fix google translator. <a href='http://commits.kde.org/kdepim/8d56426265dca0884d5dbeac03a99eb84f991082'>Commit.</a> </li>
<li>Oops fix logic. <a href='http://commits.kde.org/kdepim/5ae332117b972f43a051575daf01288b40993cbf'>Commit.</a> </li>
<li>Fix Bug 359698 - Wastebin does not get emptied when config set to empty on exit. <a href='http://commits.kde.org/kdepim/d7f066f3d19b6861b5adfaefe40b4ba86cda2394'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359698'>#359698</a></li>
<li>Use qtpaths directly. <a href='http://commits.kde.org/kdepim/fa0e4ccd36b4a5691f3bc05b26580bd8c6677f39'>Commit.</a> </li>
<li>Test if file exists before to execute sed. <a href='http://commits.kde.org/kdepim/a3858378b9a4ac7d148d963313637ec79694187e'>Commit.</a> </li>
<li>Increase version as 5.1.2 was released. <a href='http://commits.kde.org/kdepim/b5e14c8d7b8f151405c878522f5087a0a1c926e0'>Commit.</a> </li>
<li>Apply patchs from allen:. <a href='http://commits.kde.org/kdepim/aaa5a5784b7e78bae747f0706cd258f4562f52a3'>Commit.</a> </li>
<li>Fix Bug 359377 - KNotes crashed after deleting selected note. <a href='http://commits.kde.org/kdepim/f6bbc995710ec041ce3f71bb4f6c56fd5d99420a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359377'>#359377</a></li>
<li>Add missing line. <a href='http://commits.kde.org/kdepim/9214173dbf713c1236cd1daac3a675ac8e4033e5'>Commit.</a> </li>
<li>Backport: fix from sergio. <a href='http://commits.kde.org/kdepim/1553c0b79b0d2972adc53c058fa890e15cb397fd'>Commit.</a> </li>
<li>Calendarsupport/utils.cpp - support kolab_resource and kolabproxy. <a href='http://commits.kde.org/kdepim/07652c9ce0971dcb4d234021493c47bdc2d58408'>Commit.</a> </li>
<li>Fix Bug 359361 - Account Wizard - untranslatable strings. <a href='http://commits.kde.org/kdepim/ae2d3944ef4deb83da73c0e7dfe87ecd4e3beef6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359361'>#359361</a></li>
<li>FIX Bug 357374 - Huge Skype and Google Talk icons in Contacts panel view. <a href='http://commits.kde.org/kdepim/878d7ca649178d72c599384a79d160df52e225dd'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Show]</a></h3>
<ul id='ulkdepim-runtime' style='display: none'>
<li>Increase version. <a href='http://commits.kde.org/kdepim-runtime/d9a60c0a96e2108909f24d3c189c40036eeceb77'>Commit.</a> </li>
</ul>
<h3><a name='kdepimlibs' href='https://cgit.kde.org/kdepimlibs.git'>kdepimlibs</a> <a href='#kdepimlibs' onclick='toggle("ulkdepimlibs", this)'>[Show]</a></h3>
<ul id='ulkdepimlibs' style='display: none'>
<li>Akonadi::SpecialCollectionsRequestJob: increase timeout from 10s to 30s. <a href='http://commits.kde.org/kdepimlibs/12f594dc74d4e7169497c7ee2286fdf113d36c5f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/246027'>#246027</a>. Code review <a href='https://git.reviewboard.kde.org/r/127175'>#127175</a></li>
<li>Increase version. <a href='http://commits.kde.org/kdepimlibs/aa5e44e23eb573c9339eb6c5b0a64b7c3cf4c27b'>Commit.</a> </li>
<li>Remove it. We need to have a key for using this service. <a href='http://commits.kde.org/kdepimlibs/fb74865d3b218cc2122a2f143d57591ffb65e591'>Commit.</a> </li>
</ul>
<h3><a name='kgeography' href='https://cgit.kde.org/kgeography.git'>kgeography</a> <a href='#kgeography' onclick='toggle("ulkgeography", this)'>[Show]</a></h3>
<ul id='ulkgeography' style='display: none'>
<li>Fix memory leak when dealing with duplicate maps (not common). <a href='http://commits.kde.org/kgeography/6d8bea6bbb2006f15ee961c8217494fab949af34'>Commit.</a> </li>
<li>Coding improvement: qDeleteAll already deletes values. <a href='http://commits.kde.org/kgeography/51126538b4790059b476386b8df9878ca6c0397a'>Commit.</a> </li>
</ul>
<h3><a name='klickety' href='https://cgit.kde.org/klickety.git'>klickety</a> <a href='#klickety' onclick='toggle("ulklickety", this)'>[Show]</a></h3>
<ul id='ulklickety' style='display: none'>
<li>Fix background config dialog page. <a href='http://commits.kde.org/klickety/0f5042c3fdaa058186314b8ad91daf76dc28fade'>Commit.</a> </li>
</ul>
<h3><a name='konsole' href='https://cgit.kde.org/konsole.git'>konsole</a> <a href='#konsole' onclick='toggle("ulkonsole", this)'>[Show]</a></h3>
<ul id='ulkonsole' style='display: none'>
<li>Restore KDE4 behavior - do not ask for confirmation on logout. <a href='http://commits.kde.org/konsole/1771d9adbcaeb7299f86dabb9c47d03e2519ca92'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127128'>#127128</a></li>
</ul>
<h3><a name='kopete' href='https://cgit.kde.org/kopete.git'>kopete</a> <a href='#kopete' onclick='toggle("ulkopete", this)'>[Show]</a></h3>
<ul id='ulkopete' style='display: none'>
<li>Prepare for 15.12.3. <a href='http://commits.kde.org/kopete/f176733edb26848d41bd0dab38bdf2bd0c0b6419'>Commit.</a> </li>
</ul>
<h3><a name='kstars' href='https://cgit.kde.org/kstars.git'>kstars</a> <a href='#kstars' onclick='toggle("ulkstars", this)'>[Show]</a></h3>
<ul id='ulkstars' style='display: none'>
<li>Fixuifiles. <a href='http://commits.kde.org/kstars/de9392a420b71a949b758fbf51d3073d0d4f3ae0'>Commit.</a> </li>
</ul>
<h3><a name='ktouch' href='https://cgit.kde.org/ktouch.git'>ktouch</a> <a href='#ktouch' onclick='toggle("ulktouch", this)'>[Show]</a></h3>
<ul id='ulktouch' style='display: none'>
<li>Remove not so nice wording from German Neo Lesson. <a href='http://commits.kde.org/ktouch/a1b2fa5eead2abef3692b57909dfadcb18d02a21'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359796'>#359796</a>. Code review <a href='https://git.reviewboard.kde.org/r/127184'>#127184</a></li>
</ul>
<h3><a name='ktp-text-ui' href='https://cgit.kde.org/ktp-text-ui.git'>ktp-text-ui</a> <a href='#ktp-text-ui' onclick='toggle("ulktp-text-ui", this)'>[Show]</a></h3>
<ul id='ulktp-text-ui' style='display: none'>
<li>Logviewer: Fix signals of search field. <a href='http://commits.kde.org/ktp-text-ui/31565791d248e39ace22e12c25df6bb52a26b364'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127007'>#127007</a></li>
</ul>
<h3><a name='libkeduvocdocument' href='https://cgit.kde.org/libkeduvocdocument.git'>libkeduvocdocument</a> <a href='#libkeduvocdocument' onclick='toggle("ullibkeduvocdocument", this)'>[Show]</a></h3>
<ul id='ullibkeduvocdocument' style='display: none'>
<li>Fix Save-As functionality. <a href='http://commits.kde.org/libkeduvocdocument/8c8cf0290332c0c6c0bf5b1c7248ff066aa25022'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356233'>#356233</a></li>
</ul>
<h3><a name='lokalize' href='https://cgit.kde.org/lokalize.git'>lokalize</a> <a href='#lokalize' onclick='toggle("ullokalize", this)'>[Show]</a></h3>
<ul id='ullokalize' style='display: none'>
<li>Improve navigation with up/down arrows, workaround Qt bug for completion box positioning. <a href='http://commits.kde.org/lokalize/3844980dbf4eca46e2e36dfcd4d6fa786c81ae2c'>Commit.</a> </li>
</ul>
<h3><a name='parley' href='https://cgit.kde.org/parley.git'>parley</a> <a href='#parley' onclick='toggle("ulparley", this)'>[Show]</a></h3>
<ul id='ulparley' style='display: none'>
<li>Fix erroneous line when executing via command line. <a href='http://commits.kde.org/parley/229247aa312a1cf6c7b054906bc0936dea799caa'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127109'>#127109</a></li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Show]</a></h3>
<ul id='ulumbrello' style='display: none'>
<li>Fix 'Crash on export'. <a href='http://commits.kde.org/umbrello/79f0530e4c26fd75b651db5c3747656969987c2a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360172'>#360172</a></li>
<li>Fix bug showing huge zoom values in case diagrams are empty or contains only a few items. <a href='http://commits.kde.org/umbrello/221067793e32f9e122927b12a74af83850c4d056'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359774'>#359774</a></li>
</ul>


