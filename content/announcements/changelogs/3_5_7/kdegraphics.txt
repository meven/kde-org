2007-01-16 15:37 +0000 [r624163]  mlaurent

	* branches/KDE/3.5/kdegraphics/kamera/kcontrol/kamera.cpp: Fix mem
	  leak

2007-01-16 15:45 +0000 [r624165]  mlaurent

	* branches/KDE/3.5/kdegraphics/kamera/kcontrol/kameraconfigdialog.cpp,
	  branches/KDE/3.5/kdegraphics/kamera/kcontrol/kameraconfigdialog.h:
	  I didn't think that kameraconfigdialog works a day... ok action
	  was never call

2007-01-23 18:40 +0000 [r626574]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/core/document.cpp: The mimetype
	  system is returning that the file at
	  http://www.phydid.de/showpdf.php?artikel_id=91 is an octet-stream
	  for some strange reason. As kpdf is a pdf viewer assume
	  octet-stream are pdf. This is a quick fix for KDE 3.5.7 but for
	  KDE 4 and okular this fix is not admisible. :-/ BUGS: 140482

2007-01-24 14:39 +0000 [r626751]  mueller

	* branches/KDE/3.5/kdegraphics/kghostview/kpswidget.h,
	  branches/KDE/3.5/kdegraphics/kghostview/dscparse.h,
	  branches/KDE/3.5/kdegraphics/kghostview/dscparse_adapter.h,
	  branches/KDE/3.5/kdegraphics/kghostview/kgvdocument.h,
	  branches/KDE/3.5/kdegraphics/kghostview/kgv_miniwidget.h: remove
	  min() (the usual daily unbreak compilation)

2007-01-25 11:09 +0000 [r626997]  mueller

	* branches/KDE/3.5/kdegraphics/kghostview/dscparse.cpp: no idea why
	  it compiled yesterday

2007-01-25 22:02 +0000 [r627182]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/part.h,
	  branches/KDE/3.5/kdegraphics/kpdf/core/document.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/part.cpp: With dfaures and mine
	  changes on kdelibs this is the proper fix for 140482

2007-01-26 09:26 +0000 [r627349]  mueller

	* branches/KDE/3.5/kdegraphics/kghostview/dscparse.cpp: 3rd try is
	  a charm

2007-01-31 19:22 +0000 [r628869]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/part.cpp: Fix Keyboard
	  navigation in kpdf in konq only possible after focusing it with
	  the mouse after changing tab BUGS: 136702

2007-02-11 17:56 +0000 [r632587]  mkoller

	* branches/KDE/3.5/kdegraphics/libkscan/kscandevice.cpp: take care
	  of img 0-pointer (it's 0 when one hits stop during scan)

2007-02-15 21:08 +0000 [r633952]  mkoller

	* branches/KDE/3.5/kdegraphics/kolourpaint/kolourpaintui.rc,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow.cpp,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow.h,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow_file.cpp:
	  GUI: Add the ability to acquire an image from a scanner

2007-02-24 10:47 +0000 [r636821]  mueller

	* branches/KDE/3.5/kdegraphics/kghostview/dscparse.cpp: never get
	  this right ;(

2007-03-04 22:45 +0000 [r639350]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/generator_pdf.cpp:
	  keep the QCString around, otherwise the const char * contents get
	  lost

2007-03-04 22:59 +0000 [r639356]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/generator_pdf.cpp:
	  Use printer::setDocName to set the document name

2007-03-11 11:38 +0000 [r641456]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/FontEncodingTables.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/goo/gfile.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/PreScanOutputDev.h
	  (added),
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/JPXStream.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/JPXStream.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashPattern.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Function.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Dict.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashState.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Function.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Stream.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Parser.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashFTFontEngine.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/OutputDev.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/fofi/FoFiBase.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/UGString.cc
	  (removed),
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashFTFontFile.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashFTFont.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/PSOutputDev.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/XRef.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/TextOutputDev.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/goo/gmem.cc (added),
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Page.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/GfxFont.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashT1Font.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/fofi/FoFiType1C.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Parser.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/PDFDoc.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashFontEngine.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashT1FontEngine.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/SecurityHandler.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashFTFont.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashFTFontFile.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/XRef.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/SecurityHandler.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashT1FontEngine.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/README.xpdf,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/FlateStream.h
	  (removed),
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/JBIG2Stream.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Catalog.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Page.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Annot.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/CharCodeToUnicode.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/UnicodeMap.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Makefile.am,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashXPath.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/JArithmeticDecoder.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Error.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashClip.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Stream.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashPath.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashXPathScanner.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashTypes.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashT1Font.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashScreen.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Gfx.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/error.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/GfxState.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/UnicodeMap.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/GfxState.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/aconf.h,
	  branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/gp_outputdev.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/fofi/FoFiType1C.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashMath.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/UGString.h (removed),
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/FlateStream.cc
	  (removed),
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashFontFile.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashFont.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Decrypt.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/SplashOutputDev.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/goo/gfile.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Annot.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashBitmap.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashPattern.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/Splash.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Gfx.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/fofi/FoFiType1.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashClip.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/BuiltinFont.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/BuiltinFont.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashFontEngine.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/goo/GString.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/NameToUnicodeTable.h,
	  branches/KDE/3.5/kdegraphics/kpdf/configure.in.in,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/fofi/FoFiTrueType.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/FontEncodingTables.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Outline.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashT1FontFile.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashScreen.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/xpdf_config.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashFTFontEngine.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashPath.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/fofi/FoFiEncodings.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/PSOutputDev.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/GlobalParams.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/goo/gmem.c (removed),
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Object.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/JBIG2Stream.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/goo/gmem.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/PreScanOutputDev.cc
	  (added),
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/NameToCharCode.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/generator_pdf.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Dict.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/Splash.h,
	  branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/gp_outputdev.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/DCTStream.h
	  (removed),
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/OutputDev.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/GlobalParams.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Object.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/fofi/FoFiTrueType.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/goo/Makefile.am,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Catalog.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/GfxFont.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashFontFile.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashFont.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Decrypt.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/SplashOutputDev.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/goo/GHash.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/PSTokenizer.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/NameToCharCode.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashT1FontFile.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/goo/GHash.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Lexer.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashBitmap.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashState.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/DCTStream.cc
	  (removed),
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/fofi/FoFiType1.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashXPath.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Link.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Link.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/fofi/FoFiEncodings.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/UnicodeTypeTable.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/goo/GString.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/PDFDoc.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashXPathScanner.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/TextOutputDev.cc:
	  Merge xpdf 3.02 changes, this is a quite big patch but fixes lots
	  of problems so i'm commiting it. Pino, cartman and me have been
	  working on it for around a week and we have found no noticeable
	  regression and lots of improvements. Obviously more testing is
	  welcome. Ok'ed by coolo on k-c-d

2007-03-11 12:36 +0000 [r641462]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/PSOutputDev.cc:
	  removed this while merging, bad bad bad

2007-03-11 13:55 +0000 [r641479]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Lexer.cc: fix
	  regression on 132042

2007-03-11 14:33 +0000 [r641490]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashXPath.cc:
	  protect against bad pdf, for example one in bug 132042

2007-03-11 14:54 +0000 [r641497]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Stream.cc: Convert
	  that Gulong t; to Guint t; to fix printing images on 64 bit
	  failing.

2007-03-13 21:12 +0000 [r642287]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/part.h,
	  branches/KDE/3.5/kdegraphics/kpdf/shell/shell.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/part.cpp: Fix emision of window
	  caption for remote files and for kpdf windows restored by the
	  session manager BUGS: 142906

2007-03-22 20:42 +0000 [r645533]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Gfx.cc: Accept Width
	  and Height as reals too BUGS: 143322

2007-03-24 16:16 +0000 [r646111]  philrod

	* branches/KDE/3.5/kdegraphics/kuickshow/src/kuickshow.cpp: Don't
	  try to delete an item if there isn't one. BUG:143412

2007-03-30 17:29 +0000 [r648173]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/core/document.h,
	  branches/KDE/3.5/kdegraphics/kpdf/core/document.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/generator_pdf.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/part.cpp: Give KMessageBoxes a
	  parent widget. BUGS: 143612

2007-03-30 21:14 +0000 [r648272]  pino

	* branches/KDE/3.5/kdegraphics/kpdf/core/link.h,
	  branches/KDE/3.5/kdegraphics/kpdf/ui/presentationwidget.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/core/link.cpp: Add tooltips for
	  links in presentation mode. (Not yet in the contents area.)
	  CCBUGS: 131361 CCMAIL: kde-i18n-doc@kde.org

2007-03-30 21:34 +0000 [r648280]  pino

	* branches/KDE/3.5/kdegraphics/kpdf/ui/pageview.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/ui/pageview.h: Show a tooltip
	  of the link under the mouse even in the content area. BUG: 131361

2007-04-02 17:52 +0000 [r649459]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/SplashOutputDev.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashFontEngine.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashFontEngine.h:
	  lost this while doing the 3.02 merging, "thanks" that i'm redoing
	  the merge again in poppler and found the problem :D

2007-04-04 20:29 +0000 [r650560]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/generator_pdf.cpp:
	  the types enum changed when merging xpdf 3.02

2007-04-04 20:33 +0000 [r650562]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/generator_pdf.cpp:
	  next time i'll compile before commiting, i promise

2007-04-05 13:05 +0000 [r650801]  scripty

	* branches/KDE/3.5/kdegraphics/kpdf/hisc-app-kpdf.svgz: Remove
	  svn:executable (as it is a graphic file) (goutte)

2007-04-09 16:25 +0000 [r651906]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/generator_pdf.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/part.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/PSOutputDev.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/PSOutputDev.h: Add an
	  option to force the printing of the page rasterized as image
	  instead of converting PDF commands to PS commands. This
	  "decreases" printing quality as the thing you print is no longer
	  vectorial, but it is useful for printing files that would fail
	  otherwise because our PDF->PS converter is not as perfect as it
	  should be.

2007-04-10 21:55 +0000 [r652398]  aacid

	* branches/KDE/3.5/kdegraphics/kfile-plugins/pdf/kfile_pdf.cpp,
	  branches/KDE/3.5/kdegraphics/kfile-plugins/pdf/kfile_pdf.h:
	  Delete the doc once the metadata has been read. Problem and patch
	  provided by Raúl Sánchez Siles Thanks! CCMAIL: rasasi78@gmail.com

2007-04-16 19:12 +0000 [r654682]  pino

	* branches/KDE/3.5/kdegraphics/kpdf/ui/toc.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/generator_pdf.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/part.cpp: React to the commands
	  for opening the TOC pane, and for expanding the subtrees of the
	  TOC. BUG: 104081

2007-04-17 11:19 +0000 [r654926]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/NEWS,
	  branches/KDE/3.5/kdegraphics/kolourpaint/README,
	  branches/KDE/3.5/kdegraphics/kolourpaint/VERSION: Prematurely up
	  ver to 1.4.7_relight (KDE 3.5.7)

2007-04-17 11:42 +0000 [r654935]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/kolourpaint.cpp,
	  branches/KDE/3.5/kdegraphics/kolourpaint/AUTHORS: Add credit

2007-04-17 11:49 +0000 [r654938]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/README: Missed
	  kolourpaint.sourceforge.net -> www.kolourpaint.org

2007-04-19 21:34 +0000 [r655997]  jriddell

	* branches/KDE/3.5/kdegraphics/kpovmodeler/pics/crystalsvg/cr22-action-pmconfigureopengl.png:
	  replace opengl logo, which is restricted by copyright and
	  trademark http://pusling.com/cr48-app-kcmopengl.png

2007-04-20 23:19 +0000 [r656308]  pino

	* branches/KDE/3.5/kdegraphics/kpdf/part.h,
	  branches/KDE/3.5/kdegraphics/kpdf/part.cpp: Restore the
	  previously open pane in the side toolbox after reloading a
	  watched document. BUG: 116771

2007-04-21 20:16 +0000 [r656601]  scripty

	* branches/KDE/3.5/kdegraphics/kolourpaint/tests/depth1.bmp: BMP
	  files should not be executable (goutte)

2007-04-23 18:37 +0000 [r657287]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/shell/main.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/VERSION: update version number
	  for 3.5.7 release

2007-04-28 11:41 +0000 [r658739]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/kolourpaintui.rc,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow.cpp,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow.h,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow_file.cpp:
	  Follow up the scanning support added -r633952: fix style without
	  changing any code in any actual way.

2007-04-28 13:55 +0000 [r658776-658774]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow_file.cpp:
	  Some more scanning work: * Use standard KDE device-scanner icon
	  -- instead of nothing -- for scanning action *
	  kpMainWindow::slotScan() - End current shape before any dialogs
	  come up - Disable action the first time the scan dialog cannot be
	  created * kpMainWindow::slotScanned() - Don't set wait cursor as
	  open() doesn't (I think the justification was to avoid a wait
	  cursor for when the lossy dialog comes up - slotScanned() will
	  invoke this dialog in the near future) * Whitebox test and
	  +debug, +comments, +TODO

	* branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow_file.cpp:
	  style

2007-04-28 14:05 +0000 [r658778]  dang

	* branches/KDE/3.5/kdegraphics/libkscan/kscanoption.cpp: Use
	  correct "delete [] buffer", not "delete buffer" ("buffer" is a
	  private field set by "allocBuffer()". "allocBuffer()" uses "new
	  char []"). Thanks to valgrind.

2007-04-29 08:42 +0000 [r659016]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/kpdocument.cpp,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpdocument.h,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow_file.cpp:
	  Fix scanning support: * Pass scanned QImage through proper
	  converter to QPixmap instead of through QPixmap::QPixmap(const
	  QImage&) - Split
	  kpDocument::convertToPixmapAsLosslessAsPossible() out of
	  kpDocument::getPixmapFromFile(), which wraps
	  kpPixmapFX::convertToPixmapAsLosslessAsPossible() and returns
	  meta information. - Also make kpDocument::getPixmapFromFile()
	  call removeTempFile() as quickly as possible, instead of being
	  called from lots of return paths. * Write meta info of scanned
	  QImage to kpDocument

2007-04-29 10:38 +0000 [r659048]  pino

	* branches/KDE/3.5/kdegraphics/kolourpaint/kpdocument.h: remove
	  extra qualification

2007-04-29 11:41 +0000 [r659069]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow.h,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow_file.cpp: *
	  Factor out setDocumentChoosingWindow() from duplicated code in
	  open() and slotScanned() - Group shouldOpenInNewWindow() with it
	  * Disable scanning debug * +comments

2007-05-03 07:53 +0000 [r660614]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow_file.cpp: *
	  If disabling scan action, disable it in every window, not just
	  the current one [don't forward port since we already have a
	  proper dialog in branches/work/~dang/kdegraphics/kolourpaint/ -
	  no need for disabling action there] * +comment on speed of
	  KScanDialog::getScanDialog() [already in
	  branches/work/~dang/kdegraphics/kolourpaint/]

2007-05-03 11:30 +0000 [r660672]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow_file.cpp:
	  For probably-bogus out-of-QPixmap-memory case, make sure we close
	  the scan dialog. Closing the scan dialog early also means no
	  nested dialogs. [BACKPORT of part of -r660670
	  branches/work/~dang/kdegraphics/kolourpaint/mainwindow/kpMainWindow_File.cpp]

2007-05-03 12:58 +0000 [r660696]  mueller

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/SplashOutputDev.cc:
	  fix array overrun

2007-05-03 20:59 +0000 [r660816]  mm

	* branches/KDE/3.5/kdegraphics/kamera/kioslave/kamera.cpp: bug
	  113648 fixed movie download. The KIO connection silently (!!!!!)
	  discards data() larger than 16MB.

2007-05-05 12:02 +0000 [r661377]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/kpdocument.cpp,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpdocument.h: Factor
	  savePixmapToQFile() out of savePixmapToFile() in preparation for
	  some refactoring, which in itself is preparation for introducing
	  KSaveFile.

2007-05-05 12:16 +0000 [r661382]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/kpdocument.cpp: Refactor
	  to not split up remote file writing path in savePixmapToFile(). *
	  Handle local file case first, fixing a confusing "if" * +TODO *
	  +comments

2007-05-06 11:49 +0000 [r661689-661688]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/kpdocument.cpp: Make
	  kpDocument::savePixmapToFile() save local files atomically using
	  KSaveFile. KolourPaint will no longer truncate an existing file
	  if the KImageIO library for the file format is missing or if you
	  run out of disk space. Kill recently introduced
	  savePixmapToQFile() since it does not work with KSaveFile. Note
	  that the local file code path no longer checks for
	  QFile::status() after closing the file (we should check for
	  write-behind filesystems - see Qt 3.3 QFile::close() docs).
	  KSaveFile::close() should do that, although it doesn't. In
	  reality, I doubt there are any such filesystems where it makes a
	  difference. Remote files are still not saved atomically :(

	* branches/KDE/3.5/kdegraphics/kolourpaint/kpdocument.cpp,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpdocument.h: missed
	  part of last commit: remove prototype for dead
	  savePixmapToQFile(); space

2007-05-06 11:54 +0000 [r661691]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/NEWS,
	  branches/KDE/3.5/kdegraphics/kolourpaint/README: Document atomic
	  save feature; move freeze to new May 14

2007-05-07 19:25 +0000 [r662276-662275]  mm

	* branches/KDE/3.5/kdegraphics/kamera/AUTHORS: updated

	* branches/KDE/3.5/kdegraphics/kamera/README: updated

2007-05-08 07:09 +0000 [r662446]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/kpdocument.cpp: +comment
	  about remote KSaveFile

2007-05-09 21:18 +0000 [r663027]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/generator_pdf.cpp:
	  Improve handling of page sizes when printing. Thanks a lot for
	  testing the patch BUGS: 144935

2007-05-11 11:49 +0000 [r663466-663465]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/kolourpaint.cpp,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow.cpp,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow.h,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpdefs.h,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow_file.cpp:
	  Implement rudimentary global session management. It saves the
	  URLs, which is the most I dare implement in the stable branch so
	  as to not break anything. Local session save/restore is not
	  implemented, although some psuedo-code exists for it. I don't
	  intend to implement this since I don't think any other KDE app
	  does. CCMAIL: 94651@bugs.kde.org

	* branches/KDE/3.5/kdegraphics/kolourpaint/NEWS: Add session
	  management news, +missing hash

2007-05-11 12:40 +0000 [r663478-663477]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/kolourpaint.cpp,
	  branches/KDE/3.5/kdegraphics/kolourpaint/README,
	  branches/KDE/3.5/kdegraphics/kolourpaint/BUGS: Change
	  kolourpaint-support@lists.sourceforge.net to
	  http://www.kolourpaint.org/support (or nothing, due to string
	  freeze). Rationale: Permanently listed email addresses are always
	  spammed and must be changed every few years. Hence, should not
	  list email in program in first place. Website content can always
	  be changed - installed programs cannot be.

	* branches/KDE/3.5/kdegraphics/kolourpaint/README: consistency

2007-05-12 08:42 +0000 [r663751]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/NEWS,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow.h,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow_file.cpp:
	  "File / Open Recent" fixes: 1. Make "File / Open Recent" work
	  consistently when multiple windows are open. Works around
	  KRecentFilesAction::setItems() not working by using loadConfig()
	  instead. IMHO, as of r436207 2005-07-19 09:49:26 UTC,
	  KRecentFilesAction::setItems() does not work since only its
	  superclass, KSelectAction, implements setItems() and can't update
	  KRecentFilesAction's d->m_urls map nor insert the square brackets
	  around the URLs in the submenu. The bug can be triggered in
	  KolourPaint: File / Open Recent, followed by another File / Open
	  Recent, results in File / Open Recent not working in the new
	  window. It's a bit more difficult to reproduce before the recent
	  kpMainWindow::open() changes. However, some variant of the bug
	  has been around ever since the KRecentFilesAction commit (i.e.
	  start of KDE 3.5). 2. Don't select a URL in the "Recent Files"
	  submenu after opening it has failed.

2007-05-12 09:33 +0000 [r663763]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/kolourpaint.cpp,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kolourpaintui.rc,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpdocumentsaveoptionswidget.cpp,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpdocumentsaveoptions.cpp,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow_file.cpp:
	  Check all SYNC's before KDE 3.5.7 release (see
	  branches/kolourpaint/control/TODO for what this means). No
	  functional changes where made: * Change some SYNC to STRING *
	  Comments * +TODO

2007-05-12 09:55 +0000 [r663771-663770]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/kpview.cpp: Explain why
	  kpView is using guardded pointers to the document and friends

	* branches/KDE/3.5/kdegraphics/kolourpaint/kpview.cpp: Explain some
	  more

2007-05-12 10:36 +0000 [r663785]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/NEWS,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow_edit.cpp:
	  Make KolourPaint less annoying: * CTRL+C'ing a text box also
	  places the text in the middle-mouse-button clipboard, in lieu of
	  being able to highlight the text to do this

2007-05-12 11:51 +0000 [r663822]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/NEWS,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpview.cpp: * Change
	  minimum allowed zoom level for the grid from 600% to 400% Years
	  ago, people on kde-apps.org thought that the zoom did not work
	  since they were expecting it at 400%.

2007-05-12 13:05 +0000 [r663850]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/kolourpaint.cpp: Use
	  default 'Please use http://bugs.kde.org to report bugs' message
	  in About Dialog, instead of nothing

2007-05-14 07:15 +0000 [r664515]  binner

	* branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/arts/1.5/arts/configure.in.in,
	  branches/KDE/3.5/kdelibs/README,
	  branches/KDE/3.5/kdelibs/kdecore/ksycoca.h,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdevelop/kdevelop.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.lsm,
	  branches/KDE/3.5/kdebase/startkde,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdelibs/kdecore/kdeversion.h,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdewebdev/kdewebdev.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdebase/konqueror/version.h,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.h,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: bump version numbers for
	  3.5.7

