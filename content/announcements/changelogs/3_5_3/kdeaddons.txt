2006-04-01 23:42 +0000 [r525413]  henrique

	* branches/KDE/3.5/kdeaddons/konq-plugins/arkplugin/arkplugin.cpp,
	  branches/KDE/3.5/kdeaddons/konq-plugins/arkplugin/arkplugin.h: *
	  Fix a crash caused by caching KFileItems BUG: 113585 CCMAIL:
	  haris@mpa.gr

2006-04-13 16:35 +0000 [r529517]  mueller

	* branches/KDE/3.5/kdeaddons/kicker-applets/ktimemon/sample.h,
	  branches/KDE/3.5/kdeaddons/kicker-applets/ktimemon/timemon.cc,
	  branches/KDE/3.5/kdeaddons/kicker-applets/ktimemon/README,
	  branches/KDE/3.5/kdeaddons/kicker-applets/ktimemon/timemon.h,
	  branches/KDE/3.5/kdeaddons/kicker-applets/ktimemon/confdlg.cc,
	  branches/KDE/3.5/kdeaddons/kicker-applets/ktimemon/confdlg.h,
	  branches/KDE/3.5/kdeaddons/kicker-applets/ktimemon/sample.cc: -
	  added CPU iowait support. patch by Michael Blakeley, thanks! -
	  added kernel slab memory support. Finally :) - some cleanups.

2006-04-14 23:55 +0000 [r529986]  mueller

	* branches/KDE/3.5/kdeaddons/kicker-applets/ktimemon/timemon.cc:
	  backports for the iowait support

2006-04-18 18:03 +0000 [r531207]  roth

	* branches/KDE/3.5/kdeaddons/konq-plugins/sidebar/metabar/src/default.css,
	  branches/KDE/3.5/kdeaddons/konq-plugins/sidebar/metabar/src/metabarwidget.cpp,
	  branches/KDE/3.5/kdeaddons/konq-plugins/sidebar/metabar/src/settingsplugin.cpp,
	  branches/KDE/3.5/kdeaddons/konq-plugins/sidebar/metabar/src/defaultplugin.cpp:
	  Fixed problem with <nobr> tags (Thanks to Michael Kreitzer for
	  the patch) Some other minor changes and fixes

2006-04-21 14:55 +0000 [r532281]  bram

	* branches/KDE/3.5/kdeaddons/konq-plugins/README: Added description
	  of the minitools plugin.

2006-05-15 12:04 +0000 [r541035]  charles

	* branches/KDE/3.5/kdeaddons/konq-plugins/webarchiver/plugin_webarchiver.cpp:
	  this isn't DOS

2006-05-17 12:17 +0000 [r541824]  dfaure

	* branches/KDE/3.5/kdeaddons/konq-plugins/domtreeviewer/plugin_domtreeviewer.cpp,
	  branches/KDE/3.5/kdeaddons/konq-plugins/domtreeviewer/plugin_domtreeviewer.h,
	  branches/KDE/3.5/kdeaddons/konq-plugins/domtreeviewer/domtreewindow.cpp,
	  branches/KDE/3.5/kdeaddons/konq-plugins/domtreeviewer/domtreewindow.h:
	  Found the reason why the QObject::destroyed() signal wasn't
	  emitted when closing the window: missing if(part_manager) before
	  disconnect(part_manager). So disconnect(0) was called and
	  -everything- got disconnected from DOMTreeWindow. Nasty, heh?
	  This also fixes the crash when "reopening the dialog when it's
	  already up" BUG: 127405 CCMAIL: Christian Spiel
	  <e9800675@stud4.tuwien.ac.at>, Leo Savernik <l.savernik@aon.at>

2006-05-23 10:34 +0000 [r543997]  binner

	* branches/KDE/3.5/kdevelop/configure.in.in,
	  branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/KDE/3.5/kdepim/kontact/src/main.cpp,
	  branches/arts/1.5/arts/configure.in.in,
	  branches/KDE/3.5/kdepim/akregator/src/aboutdata.h,
	  branches/KDE/3.5/kdepim/kmail/kmversion.h,
	  branches/KDE/3.5/kdelibs/README,
	  branches/KDE/3.5/kdepim/kaddressbook/kabcore.cpp,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdeutils/kcalc/version.h,
	  branches/KDE/3.5/kdevelop/kdevelop.lsm,
	  branches/KDE/3.5/kdebase/startkde,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdelibs/kdecore/kdeversion.h,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdebase/konqueror/version.h,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: bump version for KDE
	  3.5.3

