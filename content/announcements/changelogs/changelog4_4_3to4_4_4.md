---
title: "KDE 4.4.4 Changelog"
hidden: true
---

  <h2>Changes in KDE 4.4.4</h2>
    <h3 id="kdebase"><a name="kdebase">kdebase</a><span class="allsvnchanges"> [ <a href="4_4_4/kdebase.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="dolphin">dolphin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix sorting issues, if "natural sorting" is enabled. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=237551">237551</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=237541">237541</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1126664&amp;view=rev">1126664</a>. </li>
        <li class="bugfix ">Do not reorder items with the same name (in upper case and lower case) when hovering one of them with the mouse. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=217048">217048</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1127782&amp;view=rev">1127782</a>. </li>
        <li class="bugfix ">Clear the previous selection when pasting or dropping items. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=223905">223905</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1131231&amp;view=rev">1131231</a>. </li>
        <li class="bugfix ">Do not change the default view properties if the view mode is changed in a remote folder. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=234852">234852</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1128622&amp;view=rev">1128622</a>. </li>
        <li class="bugfix ">Do not show svn:ignores as versioned files. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=227373">227373</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1122762&amp;view=rev">1122762</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegames"><a name="kdegames">kdegames</a><span class="allsvnchanges"> [ <a href="4_4_4/kdegames.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="libkdegames">libkdegames</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix ace of spades in dondorf carddeck. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=180186">180186</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1123632&amp;view=rev">1123632</a>. </li>
      </ul>
      </div>
      <h4><a name="kmines">kmines</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Dynamically calculate the maximum number of mines in the custom config. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=163806">163806</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1123625&amp;view=rev">1123625</a>. </li>
      </ul>
      </div>
      <h4><a name="knetwalk">knetwalk</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fixed swapped rotating directions in keyboard shortcuts. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=170066">170066</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1123623&amp;view=rev">1123623</a>. </li>
      </ul>
      </div>
      <h4><a name="lskat">lskat</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Remember the state of the toolbar and statusbar between sessions. See SVN commit <a href="http://websvn.kde.org/?rev=1123628&amp;view=rev">1123628</a>. </li>
      </ul>
      </div>
      <h4><a name="kspaceduel">kspaceduel</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Allow more than 99 wins to be shown. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=172930">172930</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1123626&amp;view=rev">1123626</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdetoys"><a name="kdetoys">kdetoys</a><span class="allsvnchanges"> [ <a href="4_4_4/kdetoys.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kteatime">kteatime</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix "--time" command line option to work properly now. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=230149">230149</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1121549&amp;view=rev">1121549</a>. </li>
        <li class="bugfix ">Fix possible unwanted double notifications when using the reminder functionality. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=190182">190182</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1122245&amp;view=rev">1122245</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeutils"><a name="kdeutils">kdeutils</a><span class="allsvnchanges"> [ <a href="4_4_4/kdeutils.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://utils.kde.org/projects/ark/" name="ark">ark</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix extraction and deletion of files with characters such as [, ], ^, *, !, - and ? in ZIP archives. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=208091">208091</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1124327&amp;view=rev">1124327</a>. </li>
        <li class="bugfix ">Fix files with same names but different paths in an archive being previewed as the same file. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=231151">231151</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1124370&amp;view=rev">1124370</a>. </li>
        <li class="bugfix ">Fix extraction with drag'n'drop: the files are extracted to the current directory and do not recreate their full path inside the archive. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=208384">208384</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1124379&amp;view=rev">1124379</a>. </li>
        <li class="bugfix ">Fix the listing of 7zip archives after a file is updated or added to it. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=238044">238044</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1128702&amp;view=rev">1128702</a>. </li>
      </ul>
      </div>
      <h4><a href="http://utils.kde.org/projects/kgpg/" name="kgpg">KGpg</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix crash when trying to sign keys for the second time. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=235913">235913</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1124523&amp;view=rev">1124523</a>. </li>
        <li class="bugfix ">Do not show message that password was changed when password entry was cancelled. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=235586">235586</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1125149&amp;view=rev">1125149</a>. </li>
        <li class="bugfix ">When doing the same action multiple times the result was sometimes reported once for every previous action (especially in key properties dialog). See SVN commit <a href="http://websvn.kde.org/?rev=1125164&amp;view=rev">1125164</a>. </li>
        <li class="bugfix crash">Fix crash when first time wizard was run and a default key was set. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=238517">238517</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1129769&amp;view=rev">1129769</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdenetwork"><a name="kdenetwork">kdenetwork</a><span class="allsvnchanges"> [ <a href="4_4_4/kdenetwork.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kopete">kopete</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Correctly calculate the height of the identity / account list. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=234002">234002</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1127601&amp;view=rev">1127601</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdepim"><a name="kdepim">kdepim</a><span class="allsvnchanges"> [ <a href="4_4_4/kdepim.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://www.astrojar.org.uk/kalarm" name="kalarm">KAlarm</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix alarm edit dialog not saving changes when invoked from the Edit button in an alarm message window. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=237822">237822</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1128669&amp;view=rev">1128669</a>. </li>
        <li class="bugfix ">Fix main window close action not working when system tray icon is not shown. See SVN commit <a href="http://websvn.kde.org/?rev=1128936&amp;view=rev">1128936</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeedu"><a name="kdeedu">kdeedu</a><span class="allsvnchanges"> [ <a href="4_4_4/kdeedu.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://edu.kde.org/khangman" name="khangman">KHangMan</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fixe corrupted SVG background in Desert theme. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=230380">230380</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=11132310&amp;view=rev">11132310</a>. </li>
      </ul>
      </div>
    </div>
