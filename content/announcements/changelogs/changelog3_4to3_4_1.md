---
title: "KDE 3.4 to KDE 3.4.1 Changelog"
hidden: true
---

<p>
This page tries to present as much as possible the additions
and corrections that occurred in KDE between the 3.4 and 3.4.1 releases.
Nevertheless it should be considered as incomplete.
</p>

<h3><a name="arts">arts</a> <font size="-2">[<a href="3_4_1/arts.txt">all SVN changes</a>]</font></h3><ul>
  <li></li>
</ul>

<h3><a name="kdelibs">kdelibs</a> <font size="-2">[<a href="3_4_1/kdelibs.txt">all SVN changes</a>]</font></h3><ul>
<li>kdecore: Fix in command-line argument handler for clicking on mailto urls while kontact was started with arguments</li>
<li>kdecore: Fix the support for Dante proxies (<a href="http://bugs.kde.org/show_bug.cgi?id=98018">#98018</a>)</li>
<li>kdecore: Fix the "random resolver failure" problem (<a href="http://bugs.kde.org/show-bug.cgi?id=94703">#94703</a>)</li>
<li>kdecore: Fix the support for the IPv6 Blacklist in ioslaves</li>
<li></li>
<li>kdeui: Fix "Change Icon" in the toolbar editor</li>
<li>khtml: Fix submitted position for scrolled imagemaps (<a href="http://bugs.kde.org/show_bug.cgi?id=59701">#59701</a>)</li>
<li>khtml: Load external CSS style-sheet with correct charset (<a href="http://bugs.kde.org/show_bug.cgi?id=79065">#79065</a>)</li>
<li>khtml: Apply CSS padding to tables(<a href="http://bugs.kde.org/show_bug.cgi?id=50235">#50235</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=90711">#90711</a>)</li>
<li>khtml: Improve PRE parsing (<a href="http://bugs.kde.org/show_bug.cgi?id=82393">#82393</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=93025">#93025</a>)</li>
<li>khtml: Fix links with WBR tags (<a href="http://bugs.kde.org/show_bug.cgi?id=101678">#101678</a>)</li>
<li>khtml: Parse CSS3 pseudo-classes and pseudo-elements more strict</li>
<li>khtml: Fix document.all.item</li>
<li>khtml: Return meaningful HTMLTableCellElement::cellIndex() values (<a href="http://bugs.kde.org/58799">#58799</a>)</li>
<li>khtml: Unbreak setting of location.href on windows opened by windows.open() (<a href="http://bugs.kde.org/101178">#101178</a>)</li>
<li>khtml: Various crash fixes (<a href="http://bugs.kde.org/show_bug.cgi?id=78205">#78205</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=84173">#84173</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=86973">#86973</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=97085">#97085</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=98973">#98973</a>)</li>
<li>khtml: Fixed infinite loop due to onChange from textareas (<a href="http://bugs.kde.org/show_bug.cgi?id=100963">bug #100963</a>)</li>
<li>khtml: Fix crash in window.setTimeout when Javascript is disabled</li>
<li>khtml: Fix server-side-push handler to accept bugzilla sending "text/html; charset=utf-8" as Content-Type (<a href="http://bugs.kde.org/show_bug.cgi?id=77333">bug #77333</a>)</li>
<li>khtml: Fix for MMB-pasting onto an empty konqueror window (<a href="http://bugs.kde.org/show_bug.cgi?id=61179">bug #61179</a>)</li>
<li>kjs: Allow non-ascii letters in identifiers (<a href="http://bugs.kde.org/102793">#102793</a>)</li>
<li>kjs: More flexible date string parsing (<a href="http://bugs.kde.org/98027">#98027</a>)</li>
<li>kjs: Fixes in escape() and unescape() functions</li>
<li>kjs: Fixes in RegExp constructor</li>
<li>kssl: store SSL passwords in the KDE wallet and reuse as needed</li>
<li>kio: KZip: Fixed double-deletion bug when trying to write to a non-writable directory.</li>
<li>kio: Fix for print:/manager not embedding the right part in Konqueror</li>
<li>kio: Use correct window icon in Konqueror when showing trash:/ (<a href="http://bugs.kde.org/show_bug.cgi?id=100321">bug #100321</a>)</li>
<li>kfile: Don't show edit dialog when drop-adding items to sidebar</li>
<li>kfile: If Shift is pressed when menu opens show 'Delete' instead of 'Trash' (<a href="http://bugs.kde.org/show_bug.cgi?id=100394">#100394</a>)</li>
<li>kio-http: Prevent endless busy loop (<a href="http://bugs.kde.org/show_bug.cgi?id=102925">bug #102925</a>)</li>
<li>kio-http: Massive speed improvement for deleting Webdav folders</li>
<li>kio-ftp: Try non-passive mode if passive mode fails</li>
<li>kio-ftp: Fix parsing of Netware FTP servers, so that files don't all appear with a lock (no permissions) (<a href="http://bugs.kde.org/show_bug.cgi?id=76442">bug #76442</a>)</li>
<li>kutils: Fixed support for back ref \0 (entire matched text)(<a href="http://bugs.kde.org/show_bug.cgi?id=102495">bug #102495</a>)</li>
</ul>

<h3><a name="kdeaccessibility">kdeaccessibility</a> <font size="-2">[<a href="3_4_1/kdeaccessibility.txt">all SVN changes</a>]</font></h3><ul>
  <li></li>
</ul>

<h3><a name="kdeaddons">kdeaddons</a> <font size="-2">[<a href="3_4_1/kdeaddons.txt">all SVN changes</a>]</font></h3><ul>
   <li>Rellinks: Disable the toolbar by default (<a href="http://bugs.kde.org/show_bug.cgi?id=83514">#83514</a>)</li>
</ul>

<h3><a name="kdeadmin">kdeadmin</a> <font size="-2">[<a href="3_4_1/kdeadmin.txt">all SVN changes</a>]</font></h3><ul>
   <li>kuser: Fix crash when adding user (<a href="http://bugs.kde.org/show_bug.cgi?id=100443">#100443</a>)</li>
</ul>

<h3><a name="kdeartwork">kdeartwork</a> <font size="-2">[<a href="3_4_1/kdeartwork.txt">all SVN changes</a>]</font></h3><ul>
  <li></li>
</ul>

<h3><a name="kdebase">kdebase</a> <font size="-2">[<a href="3_4_1/kdebase.txt">all SVN changes</a>]</font></h3><ul>
   <li>Konsole: Allow xterm resize ESC code to work (<a href="http://bugs.kde.org/show_bug.cgi?id=95932">#95932</a>)</li>
   <li>Konsole: Fix incorrect schema in detached sessions. (<a href="http://bugs.kde.org/show_bug.cgi?id=98472">#98472</a>)</li>
   <li>Konsole: Fix compile errors on amd64 with gcc4 (<a href="http://bugs.kde.org/show_bug.cgi?id=101559">#101559</a>)</li>
   <li>Konsole: Expand ~ in sessions' Exec= (<a href="http://bugs.kde.org/show_bug.cgi?id=102941">#102941</a>)</li>
   <li>Konsole: Fix &quot;Monitor for Activity&quot; and &quot;Monitor for Silence&quot; icons are the same. (<a href="http://bugs.kde.org/show_bug.cgi?id=103554">#103554</a>)</li>
   <li>kinfocenter: Fix OpenGL graphics card detection (<a href="http://bugs.kde.org/show_bug.cgi?id=105963">#105963</a>)</li>
   <li>kicker: Fixed K menu entries sort logic</li>
   <li>kicker: Hide sort buttons in systemtray configuration</li>
   <li>kcmbackground: Fix SVG files unavailable in slide show selector (<a href="http://bugs.kde.org/show_bug.cgi?id=102244">#102244</a>)</li>
   <li>kcmfonts: Tooltips under font preview text areas with '&amp;' (<a href="http://bugs.kde.org/show_bug.cgi?id=101563">bug #101563</a>)</li>
   <li>kcontrol: "Clear History" button crashes KControl's "History Sidebar" module (<a href="http://bugs.kde.org/show_bug.cgi?id=100348">bug #100348</a>)</li>
   <li>kdesktop: Fix SVG images don't have 'set as wallpaper' entry in context menu when dragged to desktop (<a href="http://bugs.kde.org/show_bug.cgi?id=100597">#100597</a>)</li>
   <li>kdesktop: Don't execute command immediately when selecting a recent command from the "Run Command" dialog (<a href="http://bugs.kde.org/show_bug.cgi?id=100733">#100733</a>)</li>
   <li>konqueror: Enable "Move To Trash" inside media:/ (<a href="http://bugs.kde.org/show_bug.cgi?id=101839">#101839</a>)</li>
   <li>konqueror: If Shift is pressed when menu opens show 'Delete' instead of 'Trash' (<a href="http://bugs.kde.org/show_bug.cgi?id=100394">#100394</a>)</li>
   <li>konqueror: Fix address bar encryption color stays when using back/forward (<a href="http://bugs.kde.org/show_bug.cgi?id=104227">bug #104227</a>)</li>
   <li>konqueror: Added hidden option to not show archives as folder in sidebar (<a href="http://bugs.kde.org/show_bug.cgi?id=98070">bug #98070</a>)</li>
   <li>konqueror: Make list view obey preview settings in tooltips (<a href="http://bugs.kde.org/show_bug.cgi?id=100601">bug #100601</a>)</li>
   <li>konqueror: Allow standard tooltips for truncated items in non-executable columns</li>
   <li>konqueror: Fix for wrong favicon being shown while typing a URL (<a href="http://bugs.kde.org/show_bug.cgi?id=100256">bug #100256</a>)</li>
   <li>konqueror: Fix for losing meta data in Info List View when adding a file (<a href="http://bugs.kde.org/show_bug.cgi?id=101052">bug #101052</a>)</li>
   <li>konqueror: Allow sidebar history to be disabled <a href="http://bugs.kde.org/show_bug.cgi?id=103941">bug #103941</a>)</li>
   <li>konqueror: Reload plugins after enabling/disabling them so that no restart is necessary, and show plugins for the active part, instead of always khtml.</li>
   <li>kthememanager: Don't scale up preview (<a href="http://bugs.kde.org/show_bug.cgi?id=100781">bug #100781</a>)</li>
   <li>khelpcenter: Added KDE artwork to index pages (<a href="http://bugs.kde.org/show_bug.cgi?id=100380">bug #100380</a>)</li>
   <li>kio-fish: Fixed mimetype determination over fish: URLs, so that e.g. KOffice documents can be opened (<a href="http://bugs.kde.org/show_bug.cgi?id=69333">bug #69333</a>)</li>
   <li>kio-tar: Fix for extracting large files from tar and zip archives (<a href="http://bugs.kde.org/show_bug.cgi?id=101486">bug #101486</a>)</li>
   <li>kio-trash: Fix trashing from USB keys (<a href="http://bugs.kde.org/show_bug.cgi?id=99847">bug #99847</a>)</li>
   <li>kio-trash: Don't mount all automounted directories when starting kio_trash to save time (<a href="http://bugs.kde.org/show_bug.cgi?id=101116">bug #101116</a>)</li>
</ul>

<h3><a name="kdebindings">kdebindings</a> <font size="-2">[<a href="3_4_1/kdebindings.txt">all SVN changes</a>]</font></h3><ul>
  <li></li>
</ul>

<h3><a name="kdeedu">kdeedu</a> <font size="-2">[<a href="3_4_1/kdeedu.txt">all SVN changes</a>]</font></h3><ul>
  <li>KLettres: fix Z in English data (<a href="http://bugs.kde.org/show_bug.cgi?id=102320">#102320</a>)</li>
  <li>KLettres: Romanized Hindi and Spanish support added</li>
  <li>KLettres: fix empty line in txt file that generated an empty special char</li>
  <li>KHangMan: Slovak support added and bug fix in case language is empty in kdeglobals</li>
  <li>KHangMan: fix empty line in txt file that generated an empty special char</li>
  <li>KHangMan: fix German first capital letter (<a href="http://bugs.kde.org/show_bug.cgi?id=104502">#104502</a>)</li>
  <li>Kig: check if the file name is a temporary one (<a href="http://bugs.kde.org/show_bug.cgi?id=98142">#98142</a>)</li>
  <li>Kig: don't crash when loading documents with incorrect object types (<a href="http://bugs.kde.org/show_bug.cgi?id=100292">#100292</a>)</li>
  <li>Kig: better size for some menu item icons</li>
  <li>KmPlot: Print header table correctly when automatic scaling is chosen.</li>
  <li>Kalzium: Fix drawing of the atom-hulls of elements 87-114 (<a href="http://bugs.kde.org/show_bug.cgi?id=98142">#104990</a>)</li>
  <li>Kalzium: Fix a wrong unit (used cm^2 instead of cm^3 for the density) (<a href="http://bugs.kde.org/show_bug.cgi?id=98142">#103296</a>)</li>
  <li>Kalzium: Fix translation-problem (<a href="http://bugs.kde.org/show_bug.cgi?id=98142">#101741</a>)</li>
</ul>

<h3><a name="kdegames">kdegames</a> <font size="-2">[<a href="3_4_1/kdegames.txt">all SVN changes</a>]</font></h3><ul>
  <li></li>
</ul>

<h3><a name="kdegraphics">kdegraphics</a> <font size="-2">[<a href="3_4_1/kdegraphics.txt">all SVN changes</a>]</font></h3><ul>
<li>kpdf: Change atan2f to atan2 so it compiles on Solaris 2.8 <a href="http://bugs.kde.org/show_bug.cgi?id=100778">#100778</a> </li>
<li>kpdf: Show context menu when in FullScreen even if no document is open <a href="http://bugs.kde.org/show_bug.cgi?id=100881">#100881</a> </li>
<li>kpdf: Do not leak memory when reloading a document <a href="http://bugs.kde.org/show_bug.cgi?id=101192">#101192</a> </li>
<li>kpdf: Fix mouse wheel rotation in presentation mode <a href="http://bugs.kde.org/show_bug.cgi?id=101519">#101519</a> </li>
<li>kpdf: Do not assume only there's only a ObjectRect in a given point <a href="http://bugs.kde.org/show_bug.cgi?id=101648">#101648</a> </li>
<li>kpdf: Fix properties dialog when there are very large texts <a href="http://bugs.kde.org/show_bug.cgi?id=102117">#102117</a> </li>
<li>kpdf: Compile when Xft2 headers are in a non-standard path <a href="http://bugs.kde.org/show_bug.cgi?id=102119">#102119</a> </li>
<li>kpdf: Make Page Up and Page Down work on presentation mode <a href="http://bugs.kde.org/show_bug.cgi?id=102565">#102565</a> </li>
<li>kpdf: Ask when overwriting files <a href="http://bugs.kde.org/show_bug.cgi?id=102332">#102332</a> </li>
<li>kpdf: Solaris compile fix <a href="http://bugs.kde.org/show_bug.cgi?id=102337">#102337</a> </li>
<li>kpdf: Do not move to top of the page when reloading the document <a href="http://bugs.kde.org/show_bug.cgi?id=102869">#102869</a> </li>
<li>kpdf: Add another path where to look for ghostscript fonts <a href="http://bugs.kde.org/show_bug.cgi?id=103891">#103891</a> </li>
<li>kpdf: Workaround weakness in gnome handling of desktop files <a href="http://bugs.kde.org/show_bug.cgi?id=104471">#104471</a> </li>
<li>kpdf: Don't assume Encoding array of Type1 fonts end in "foo def" <a href="http://bugs.kde.org/show_bug.cgi?id=104786">#104786</a> </li>
<li>kpdf: Fix text copying on some documents <a href="http://bugs.kde.org/show_bug.cgi?id=105630">#105630</a> </li>
<li>kpdf: Parse Light and Condensed in font name</li>
<li>kpdf: Fix the allocation size of libgoo on 64bit architectures</li>
<li>kpdf: Fix problems with non-standard styles</li>
<li>kpdf: Fix disabled icons</li>
<li>ksnapshot: Rescale screenshot preview when resizing window (<a href="http://bugs.kde.org/show_bug.cgi?id=104895">bug #104895</a>)</li>
</ul>

<h3><a name="kdemultimedia">kdemultimedia</a> <font size="-2">[<a href="3_4_1/kdemultimedia.txt">all SVN changes</a>]</font></h3><ul>
<li>akodelib: Avoid endless loops when paused in JuK (<a href="http://bugs.kde.org/show_bug.cgi?id=98223">#98223</a>)</li>
<li>akode_plugins: Fix crash when playing musepack(MPC) files on AMD64 (<a href="http://bugs.kde.org/show_bug.cgi?id=102105">#102105</a>)</li>
<li>akode_plugins: Fix some cases where streaming of Ogg Vorbis failed</li>
<li>JuK: Selecting the undo command now undoes only the last change again (<a href="http://bugs.kde.org/show_bug.cgi?id=105725">#105725</a>)</li>
</ul>

<h3><a name="kdenetwork">kdenetwork</a> <font size="-2">[<a href="3_4_1/kdenetwork.txt">all SVN changes</a>]</font></h3><ul>
<li>Kopete: Fix crash when KDE logout(<a href="http://bugs.kde.org/91288">#91288</a>)</li>
<li>Kopete: Fix crash when drag&amp;drop a temporary contact to the list. (<a href="http://bugs.kde.org/103248">#103248</a>)</li>
<li>Kopete: Fix crash when ignoring messages from non-buddies (<a href="http://bugs.kde.org/100196">#100196</a>)</li>
<li>Kopete: Resize correctly wide photos (<a href="http://bugs.kde.org/101665">#101665</a>)</li>
<li>Kopete: Change the effect applied on offline status icon in order to let see the color (<a href="http://bugs.kde.org/103278">#103278</a>)</li>
<li>Kopete: Update the contactlist whan KDE (fonts) settings changes.(<a href="http://bugs.kde.org/102288">#102288</a>)</li>
<li>Kopete: Don't send empty messages when holding enter(<a href="http://bugs.kde.org/100334">#100334</a>)</li>
<li>Kopete: MSN: Don't dissconnect people using webmessenger.msn.com when sending message with default font(<a href="http://bugs.kde.org/102371">#102371</a>)</li>
<li>Kopete: MSN: Make the timeout option for away message works. (<a href="http://bugs.kde.org/104040">#104040</a>)</li>
<li>Kopete: MSN: Fix the reverse list button.(<a href="http://bugs.kde.org/104214">#104214</a>)</li>
<li>Kopete: MSN: Fix connections problems(<a href="http://bugs.kde.org/105929">#105929</a>)</li>
<li>Kopete: MSN: Avatars downloaded correctly by third client like Gaim (<a href="http://bugs.kde.org/">#97589</a>)</li>
<li>Kopete: MSN: Show font and colors settings(<a href="http://bugs.kde.org/100577">#100577</a>)</li>
<li>Kopete: MSN: Scale the display picture (<a href="http://bugs.kde.org/104445">#104445</a>)</li>
<li>Kopete: Yahoo: Fix big fonts(<a href="http://bugs.kde.org/100617">#100617</a>)</li>
<li>Kopete: Yahoo: fix problems when sending messages(<a href="http://bugs.kde.org/102274">#102274</a>)</li>
<li>Kopete: ICQ: Display only one error when we hit an unknown error(<a href="http://bugs.kde.org/100064">#100064</a>)</li>
<li>Kopete: ICQ: fix "Online Sice" (<a href="http://bugs.kde.org/102460">#102460</a>)</li>
<li>Kopete: ICQ and AIM: Fix typing notifycation (<a href="http://bugs.kde.org/101037">#101037</a>)</li>
<li>Kopete: ICQ and AIM: Escape HTML correctly (<a href="http://bugs.kde.org/">#</a>)</li>
<li>Kopete: Latex: Blacklist some latex command (<a href="http://bugs.kde.org/103026">#103026</a>)</li>
<li>Kopete: GroupWise: copy and paste in contact property dialog (<a href="http://bugs.kde.org/99463">#99463</a>)</li>
</ul>

<h3><a name="kdepim">kdepim</a> <font size="-2">[<a href="3_4_1/kdepim.txt">all SVN changes</a>]</font></h3><ul>
<li>KMail: Fix compilation on IRIX (<a href="http://bugs.kde.org/100725">#100725</a>)</li>
<li>KMail: Send redirected messages to the correct address even if sending failed at first (<a href="http://bugs.kde.org/101190">#101190</a>)</li>
<li>KMail: Fix crash when canceling KWallet password prompt (<a href="http://bugs.kde.org/101847">#101847</a>)</li>
<li>KMail: Reduce memory consumption when importing folders</li>
<li>KMail: Honor the character encoding when loading a message from a file (<a href="http://bugs.kde.org/100834">#100834</a>)</li>
<li>KMail: Fix problem with per-identity BCC addresses (<a href="http://bugs.kde.org/102101">#102101</a>)</li>
<li>KMail: Fix problem with inline forwarding of CC'ed messages (<a href="http://bugs.kde.org/100249">#100249</a>)</li>
<li>KMail: Allow thread-related actions for single messages in threaded message list (<a href="http://bugs.kde.org/102351">#102351</a>)</li>
<li>KMail: Show correct attachment status of imported message (<a href="http://bugs.kde.org/102766">#102766</a>)</li>
<li>KMail: Fix attachment detection (<a href="http://bugs.kde.org/102663">#102663</a>)</li>
<li>KMail: Fix problem with context menu for email addresses (<a href="http://bugs.kde.org/103493">#103493</a>)</li>
<li>KMail: Show categories in the recipients picker</li>
<li>KMail: Fix problem with Apply button staying disabled (<a href="http://bugs.kde.org/86269">#86269</a>)</li>
<li>KMail: Correctly update filters filtering by status(<a href="http://bugs.kde.org/101001">#101001</a>)</li>
<li>KMail: Fix for deletion of folders with subfolders using cached imap</li>
<li>KMail: Rename temporary file used for compaction so that it doesn't risk appearing when kmail is killed and restarted</li>
<li>KNode: Don't create empty reference headers when superseding articles (<a href="http://bugs.kde.org/58290">#58290</a>)</li>
<li>KNode: Cleanly terminate network threads (<a href="http://bugs.kde.org/77381">#77381</a>)</li>
<li>KNode: Respect custom color settings in the folder tree and source viewer (<a href="http://bugs.kde.org/100533">#100533</a>)</li>
<li>KNode: More robust handling of optional headers in XOVER response (<a href="http://bugs.kde.org/101354">#101354</a>)</li>
<li>KNode: Don't toggle 'sort by thread change date' when restoring header view layout (<a href="http://bugs.kde.org/102574">#102574</a>)</li>
<li>KNode: Show error message on write errors (<a href="http://bugs.kde.org/103973">#103973</a>)</li>
<li>KNode: Remove linebreaks from subject (<a href="http://bugs.kde.org/104788">#104788</a>)</li>
<li>KNode: Don't break inline GPG signatures when forwarding messages (<a href="http://bugs.kde.org/104790">#104790</a>)</li>
<li>KNode: Fix drag&amp;drop crash in folder tree</li>
<li>karm: Don't crash when deleting a task. (<a href="http://bugs.kde.org/show_bug.cgi?id=100391">#100391</a>)</li>
<li>Akregator: Don't crash when closing tab before the page is loaded. (<a href="http://bugs.kde.org/show_bug.cgi?id=101413">#101413</a>)</li>
<li>Akregator: Show icons on status filter combobox KMail style</li>
<li>Akregator: Make reset filter button to reset status filter</li>
<li>Akregator: Show window with size it was closed or with one from command line (<a href="http://bugs.kde.org/102359">#102359</a>)</li>
<li>Akregator: Remove block around "Read more" link (<a href="http://bugs.kde.org/103665">#103665</a>)</li>
<li>Kalarm: Fix failure to enable "Reminder for first recurrence only" checkbox.</li>
<li>Kalarm: Don't ignore Sound setting in Preferences dialog Edit tab.</li>
<li>Kalarm: Reset sound volume (if it was set) as soon as audio file playing is complete.</li>
<li>KAlarm: Don't start KMix when an alarm is displayed if no sound volume is specified. (<a href="http://bugs.kde.org/102315">#102315</a>)</li>
<li>KAlarm: Don't play the audio file if the sound volume is set to zero. (<a href="http://bugs.kde.org/104218">#104218</a>)</li>
<li>Kalarm: Add command script and execute-in-terminal options to DCOP interface.</li>
</ul>

<h3><a name="kdesdk">kdesdk</a> <font size="-2">[<a href="3_4_1/kdesdk.txt">all SVN changes</a>]</font></h3><ul>
<li>cervisia: Fix statusbar when embedded in konqueror (<a href="http://bugs.kde.org/97664">#97664</a>)</li>
<li>kio_svn (subversion) : Various bugfixes (import, checkout and memleaks fixed)</li>
<li>umbrello: Fix crash on deleting attribute or enum literal</li>
<li>umbrello: Fix crash in UMLView::createAutoAttributeAssociations()</li>
<li>umbrello: Fix failure to import C++ enum type with comment on last literal</li>
<li>umbrello: Fix non-Latin1 characters in diagram names</li>
<li>umbrello: Generate missing "static" keyword in new C++ code generator</li>
<li>umbrello: Make messages refer to operations rather than being dumb text (<a href="http://bugs.kde.org/53376">#53376</a>)</li>
<li>umbrello: Make drag-to-folder behave (<a href="http://bugs.kde.org/57667">#57667</a>)</li>
<li>umbrello: Fix label placement in sequence diagram (<a href="http://bugs.kde.org/57875">#57875</a>)</li>
<li>umbrello: Allow copy/paste of attributes, operations (<a href="http://bugs.kde.org/70924">#70924</a>)</li>
<li>umbrello: Stop canvas from centering to moved class (<a href="http://bugs.kde.org/89691">#89691</a>)</li>
<li>umbrello: Fix entity relationship association attachments (<a href="http://bugs.kde.org/95353">#95353</a>)</li>
<li>umbrello: Add actions in context menu of ERM (<a href="http://bugs.kde.org/96372">#96372</a>)</li>
<li>umbrello: Stop uncontrolled scrolling in large class diagram (<a href="http://bugs.kde.org/97599">#97599</a>)</li>
<li>umbrello: Fix display of same-type attributes (<a href="http://bugs.kde.org/100290">#100290</a>)</li>
<li>umbrello: Recognize classes in packages in add-attribute dialog (<a href="http://bugs.kde.org/100307">#100307</a>)</li>
<li>umbrello: Fix performance problem with large models (<a href="http://bugs.kde.org/101148">#101148</a>)</li>
<li>umbrello: Add more options to select action in sequence diagram (<a href="http://bugs.kde.org/101541">#101541</a>)</li>
<li>umbrello: Fix changing the order of operations in class diagram (<a href="http://bugs.kde.org/103123">#103123</a>)</li>
<li>umbrello: Reduce sensitivity to element order when loading xmi files (<a href="http://bugs.kde.org/103133">#103133</a>)</li>
<li>umbrello: Fix code generation for interfaces in various programming languages (<a href="http://bugs.kde.org/103728">#103728</a>)</li>
<li>umbrello: Fix dimensioning of interfaces in EPS exported class diagrams (<a href="http://bugs.kde.org/104637">#104637</a>)</li>
</ul>

<h3><a name="kdetoys">kdetoys</a> <font size="-2">[<a href="3_4_1/kdetoys.txt">all SVN changes</a>]</font></h3><ul>
  <li></li>
</ul>

<h3><a name="kdeutils">kdeutils</a> <font size="-2">[<a href="3_4_1/kdeutils.txt">all SVN changes</a>]</font></h3><ul>
  <li>Ark: Dead lock in "Add to archive" action when providing invalid archive format (<a href="http://bugs.kde.org/show_bug.cgi?id=99279">#99279</a>)</li>
  <li>Ark: Mark "Selected Files" as default in the extraction dialog if more than one file is selected (<a href="http://bugs.kde.org/show_bug.cgi?id=105787">#105787</a>)</li>
  <li>KFloppy: better error messages during low-level formatting (Linux only)
      (including <a href="http://bugs.kde.org/show_bug.cgi?id=87637">bug #87637</a>
      and partially <a href="http://bugs.kde.org/show_bug.cgi?id=103954">bug #103954</a>)</li>
  <li>KFloppy: disable "Auto-Detect" on BSD, as it is not supported (BSD Only)</li>
  <li>kcharselect: Removed "Configure Toolbars..." menu item (<a href="http://bugs.kde.org/show_bug.cgi?id=105430">bug #105430</a>)</li>
  <li>kgpg: No "Import Missing Signatures From Keyserver" for own key (<a href="http://bugs.kde.org/show_bug.cgi?id=101245">bug #101245</a>)</li>
</ul>

<h3><a name="kdevelop">kdevelop</a> <font size="-2">[<a href="3_4_1/kdevelop.txt">all SVN changes</a>]</font></h3><ul>
<li>Subversion: many bugfixes (see kio_svn section)</li>
</ul>

<h3><a name="kdewebdev">kdewebdev</a> <font size="-2">[<a href="3_4_1/kdewebdev.txt">all SVN changes</a>]</font></h3>
<h4>Quanta Plus</h4>
<ul>
<li>read/write the correct entry from description.rc in the editor</li>
<li>fill only the attributes with source="selection" with the selection in the tag editing dialog</li>
<li>fix matching of excluded files from a project</li>
<li>don't crash when Selected is pressed and nothing is selected (in the CSS editor) (<a href="http://bugs.kde.org/show_bug.cgi?id=101919">bug 101919</a>)</li>
<li>try to fix the crash caused by incompatible changes between KDE 3.3.x and 3.4.x libcvsservice (<a href="http://bugs.kde.org/show_bug.cgi?id=102996">bug #102996</a>)</li>
<li>show the correct relative paths in URL autocompletion</li>
<li>fix crash when unsetting breakpoints</li>
<li>nicer processing of CVS output</li>
<li>read the image sizes for remote images as well</li>
<li>insert an img tag for remote images as well when using Drag and Drop</li>
<li>better context sensitiv documentation (needs updated documentation and DTEP	packages)</li>
</ul>

<h4>Kommander</h4>
<ul>
<li>fix detection of scripts that are run from a temporary directory</li>
</ul>
