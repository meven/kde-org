------------------------------------------------------------------------
r1085104 | scripty | 2010-02-04 11:21:15 +0000 (Thu, 04 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1086275 | pletourn | 2010-02-06 19:29:48 +0000 (Sat, 06 Feb 2010) | 2 lines

Send point in content area coordinates

------------------------------------------------------------------------
r1086378 | scripty | 2010-02-07 05:00:45 +0000 (Sun, 07 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1087855 | lueck | 2010-02-09 17:21:50 +0000 (Tue, 09 Feb 2010) | 1 line

doc backport from trunk
------------------------------------------------------------------------
r1088868 | jlayt | 2010-02-11 20:26:02 +0000 (Thu, 11 Feb 2010) | 3 lines

Improve CUPS detection to always match Qt, including working with remote CUPS
servers.

------------------------------------------------------------------------
r1088912 | gateau | 2010-02-11 22:02:30 +0000 (Thu, 11 Feb 2010) | 1 line

Force horizontal scrollbar to behave the same whether it's in LTR or RTL mode BUG: 210058
------------------------------------------------------------------------
r1090202 | gateau | 2010-02-14 21:50:26 +0000 (Sun, 14 Feb 2010) | 1 line

Fix stupid layout bug on "save as" bubble
------------------------------------------------------------------------
r1090811 | scripty | 2010-02-16 04:32:43 +0000 (Tue, 16 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1091887 | rkcosta | 2010-02-17 20:02:48 +0000 (Wed, 17 Feb 2010) | 9 lines

Backport r1091886.

Ignore spurious EINTR errors in waitpid when creating the PS preview.

Patch by Andriy Gapon, thanks!

CCMAIL: avg@icyb.net.ua
CCBUG: 226034

------------------------------------------------------------------------
r1093115 | pino | 2010-02-20 10:20:24 +0000 (Sat, 20 Feb 2010) | 4 lines

allow TIFF files
will be in kde 4.4.1
CCBUG: 227784

------------------------------------------------------------------------
r1093333 | aacid | 2010-02-20 17:11:05 +0000 (Sat, 20 Feb 2010) | 4 lines

Backport r1093331 | aacid | 2010-02-20 17:08:13 +0000 (Sat, 20 Feb 2010) | 3 lines

Close annotation windows on document change/reload

------------------------------------------------------------------------
r1094019 | gateau | 2010-02-21 22:33:11 +0000 (Sun, 21 Feb 2010) | 3 lines

Fix "File > open" action

BUG:227934
------------------------------------------------------------------------
r1094025 | gateau | 2010-02-21 22:57:46 +0000 (Sun, 21 Feb 2010) | 1 line

Prevent word-wrap in thumbnail tooltip
------------------------------------------------------------------------
r1094618 | pino | 2010-02-23 01:27:46 +0000 (Tue, 23 Feb 2010) | 2 lines

bump version to 0.1.3 for kde 4.4.1

------------------------------------------------------------------------
r1094620 | pino | 2010-02-23 01:28:37 +0000 (Tue, 23 Feb 2010) | 2 lines

bump version to 0.10.1 (kde 4.4.1)

------------------------------------------------------------------------
r1095360 | scripty | 2010-02-24 04:26:11 +0000 (Wed, 24 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
