Dir: kdeutils
----------------------------
new version numbers



Dir: kdeutils/kdf
----------------------------
Enable translation in Kwikdisk.
----------------------------
BACKPORT:
Hide /dev/shm and sysfs filesystems



Dir: kdeutils/khexedit
----------------------------
Backport from HEAD:
QListViewItems are sorted by comparing the text without leading whitespaces
(since when?).
This fails for aligned numbers. Fixed q'n'd by reimplementing the comparison
for the items.

BUG:92249
----------------------------
Backport from HEAD:
files with "?" and else in the filename can now be opened also when given as
app parameters

BUG:91635
----------------------------
Backport from HEAD: fix: success of remote saving was not tested



Dir: kdeutils/klaptopdaemon
----------------------------
Don't detach KDED's dcop server connection!!
BUG: 75067



Dir: kdeutils/ksim/library
----------------------------
Backport fix mem leak



Dir: kdeutils/kwallet
----------------------------
backport fixes for #90949
----------------------------
backport fix for incorrect error message
