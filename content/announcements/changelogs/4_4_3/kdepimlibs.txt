------------------------------------------------------------------------
r1108291 | tokoe | 2010-03-29 03:08:21 +1300 (Mon, 29 Mar 2010) | 6 lines

Don't jump to the last row of the editor after
deleting a contact, but to the first column of the
current row.

BUG: 228023

------------------------------------------------------------------------
r1109668 | tokoe | 2010-04-01 06:25:28 +1300 (Thu, 01 Apr 2010) | 2 lines

Backport of bugfix #232873

------------------------------------------------------------------------
r1110148 | scripty | 2010-04-02 15:02:10 +1300 (Fri, 02 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1110480 | tokoe | 2010-04-03 19:36:24 +1300 (Sat, 03 Apr 2010) | 4 lines

Let the notes widget take all the available space

BUG: 233101

------------------------------------------------------------------------
r1118224 | scripty | 2010-04-24 14:03:55 +1200 (Sat, 24 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1119399 | scripty | 2010-04-27 14:16:44 +1200 (Tue, 27 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1119520 | nlecureuil | 2010-04-27 22:11:55 +1200 (Tue, 27 Apr 2010) | 4 lines

Backport revisions 1098941 and 1098945
CCBUG: 226984


------------------------------------------------------------------------
r1120172 | wstephens | 2010-04-29 02:46:41 +1200 (Thu, 29 Apr 2010) | 21 lines

Backport ContactGroupSearchJob changes from trunk and
NepomukFeederAgentBase fixes enabling successful feeding
of contact groups into Nepomuk and their querying later
by kmail when expanding distribution lists.

Add setLimit(1) on the group search.

Also backport prerequisite changes
including #include fixes, ItemSearchJob::akonadiItemIdUri() and
ContactGroupSearchJob::setLimit()

Backported commits to NepomukFeederAgentBase:
1095503
1109633
1109634

This does not allow selecting distribution lists in the recipients
picker, but they can be entered by name in the To:, Cc: etc fields.

CCBUG: 223034

------------------------------------------------------------------------
r1120718 | mueller | 2010-04-30 07:51:38 +1200 (Fri, 30 Apr 2010) | 2 lines

4.4.3

------------------------------------------------------------------------
