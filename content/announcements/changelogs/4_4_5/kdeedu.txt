------------------------------------------------------------------------
r1131600 | lueck | 2010-05-29 02:13:31 +1200 (Sat, 29 May 2010) | 1 line

add missing i18n() call
------------------------------------------------------------------------
r1132023 | lueck | 2010-05-29 23:18:22 +1200 (Sat, 29 May 2010) | 1 line

remove duclicated and confusing whatsthishelp text
------------------------------------------------------------------------
r1132310 | annma | 2010-05-30 21:22:04 +1200 (Sun, 30 May 2010) | 4 lines

repaired Desert background, thanks to Deri
4.4 branch commit
CCBUG=230380

------------------------------------------------------------------------
r1133059 | gladhorn | 2010-06-01 13:40:37 +1200 (Tue, 01 Jun 2010) | 1 line

backport 1133054 from 4.5 - create new word types with the same grammar type as their parent word type
------------------------------------------------------------------------
r1133899 | nienhueser | 2010-06-03 07:52:47 +1200 (Thu, 03 Jun 2010) | 3 lines

Revert commit 1118101. The WATCH_NMEA flag often prevents a correct startup, while it works reliable without it (tested again with libgps 2.92 and 2.94).
Backport of commit 1133895.

------------------------------------------------------------------------
r1133938 | nienhueser | 2010-06-03 10:06:28 +1200 (Thu, 03 Jun 2010) | 7 lines

Don't leave the method early when no drag operation happens: The cursor shape still needs to be adjusted.
Needs regression testing and a backport to 4.5 and 4.4 afterwards.
BUG: 239056
Fix minor logic error introduced in rev. 1132367.
Backport of commits 1132367 and 1133908


------------------------------------------------------------------------
r1135619 | annma | 2010-06-08 07:20:20 +1200 (Tue, 08 Jun 2010) | 2 lines

backport of r1135597 to 4.4 branch

------------------------------------------------------------------------
r1136082 | jmhoffmann | 2010-06-09 09:59:15 +1200 (Wed, 09 Jun 2010) | 5 lines

Author: Modestas Vainius <modax@debian.org>
Description: do not ship marble plugins with useless R(UN)PATH=/usr
Bug-Debian: http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=582139
Last-Update: 2010-06-07

------------------------------------------------------------------------
r1136083 | jmhoffmann | 2010-06-09 10:09:13 +1200 (Wed, 09 Jun 2010) | 2 lines

Bump Marble version number for KDE 4.4.5 release.

------------------------------------------------------------------------
r1141622 | adamrakowski | 2010-06-23 15:45:16 +1200 (Wed, 23 Jun 2010) | 1 line

GUI:XML themes added CC:annma@kde.org
------------------------------------------------------------------------
r1141759 | annma | 2010-06-24 00:29:13 +1200 (Thu, 24 Jun 2010) | 4 lines

Revert 1141622 as it was intented to trunk
Please Adam commit to trunk, switch your local repository if necessary
CCMAIL=foo-script@o2.pl

------------------------------------------------------------------------
