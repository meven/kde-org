2007-05-17 09:52 +0000 [r665582]  pino

	* branches/KDE/3.5/kdeedu/kig/configure.in.in: Apply patch by Sune
	  Vuorela to make kig find the new name of Boost.Python library.
	  Thanks! CCMAIL: Sune Vuorela <Sune@vuorela.dk>

2007-06-06 20:38 +0000 [r672358]  mlaurent

	* branches/KDE/3.5/kdeedu/kmplot/kmplot/kmplot.cpp,
	  branches/KDE/3.5/kdeedu/kstars/kstars/main.cpp: Backport fix mem
	  leak

2007-06-16 15:31 +0000 [r676298]  annma

	* branches/KDE/3.5/kdeedu/kturtle/src/kturtle.cpp: Fix critical bug
	  when the user language is UTF8 Special thanks to Andrey and
	  Bozidar for the time they spent on that. BUG=82462

2007-06-16 18:51 +0000 [r676365]  annma

	* branches/KDE/3.5/kdeedu/kturtle/src/parser.cpp: fix "else" bug
	  BUG=115555

2007-06-17 08:13 +0000 [r676589]  annma

	* branches/KDE/3.5/kdeedu/kturtle/src/kturtle.cpp: sorry Andrey,
	  applied patch totally (my system is not UTF-8 so it works OK
	  without the patch) BUG=82462

2007-06-21 16:08 +0000 [r678532]  binner

	* branches/KDE/3.5/kdeedu/keduca/resources/keduca.desktop,
	  branches/KDE/3.5/kdeedu/kmplot/kmplot/kmplot.desktop,
	  branches/KDE/3.5/kdeedu/keduca/resources/keducabuilder.desktop:
	  fix invalid .desktop files

2007-06-28 03:13 +0000 [r681148-681146]  iip

	* branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/ukraine.kgm (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/ukraine.png (added):
	  Added map of Ukraine and flags of provinces from trunk

	* branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine/Sumi.png
	  (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine/Chernigiv.png
	  (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine/Chernivtsi.png
	  (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine/Lviv.png
	  (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine/Kyiv.png
	  (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine/Rivne.png
	  (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine/Mykolayiv.png
	  (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine/Kyiv-city.png
	  (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine/CMakeLists.txt
	  (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine/Odesa.png
	  (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine/Ternopil.png
	  (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine/Kharkiv.png
	  (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine/Crimea.png
	  (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine/Zaporizhya.png
	  (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine/Khmelnitsky.png
	  (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine/Lugansk.png
	  (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine/Zhitomir.png
	  (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine/Sevastopol-city.png
	  (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine/Poltava.png
	  (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine/Zakarpatya.png
	  (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine/Volyn.png
	  (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine/Dnipopetrovsk.png
	  (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine/Donetsk.png
	  (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine/Ivano-Frankivsk.png
	  (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine/Kirovohrad.png
	  (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine/Kherson.png
	  (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine/Cherkasy.png
	  (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine/Vinnytsa.png
	  (added): Added map of Ukraine and flags of provinces from trunk

2007-06-28 03:24 +0000 [r681150]  iip

	* branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine/Makefile.am
	  (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine/CMakeLists.txt
	  (removed):

2007-06-28 19:10 +0000 [r681382]  aacid

	* branches/KDE/3.5/kdeedu/kgeography/data/flags/ukraine (removed),
	  branches/KDE/3.5/kdeedu/kgeography/data/ukraine.kgm (removed),
	  branches/KDE/3.5/kdeedu/kgeography/data/ukraine.png (removed):
	  Reverting unwanted and not allowed change by Ivan Petrouchtchak
	  KDE 3.5 branch is frozen CCMAIL: ivanpetrouchtchak@yahoo.com

2007-07-16 21:15 +0000 [r688804]  nlecureuil

	* branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/Makefile.am: Install
	  existing x-kvtml mimetype

2007-07-16 21:35 +0000 [r688807]  nlecureuil

	* branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/x-kvtml.desktop: Fix
	  mimetype and patterns

2007-08-19 14:47 +0000 [r701849]  pino

	* branches/KDE/3.5/kdeedu/kig/kig/kig_part.cpp: If the loading of
	  the document fails, completely cleanup ourselves to be able to
	  open a new document in the same (untouched) window.

2007-08-27 14:10 +0000 [r705228]  harris

	* branches/KDE/3.5/kdeedu/kstars/kstars/tools/astrocalc.cpp: Fixing
	  bug #149261. I don't know if there will be another 3.5 release,
	  but if you have the source code, you can fix this very easily:
	  Open the file kdeedu/kstars/kstars/tools/astrocalc.cpp. Search
	  for the word "Cooordinates" and change it to "Coordinates". This
	  issue doesn't exist in trunk. BUG: 149261

2007-09-16 20:16 +0000 [r713250]  gladhorn

	* branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/query-dialogs/MCQueryDlgForm.ui:
	  Enable word wrap for the labels in multiple choice query. Thanks
	  to Rouslan Solomakhin! BUG: 149921

2007-09-18 20:10 +0000 [r714140]  hedlund

	* branches/KDE/3.5/kdeedu/kwordquiz/src/qaview.cpp: Fix handling of
	  hints. BUG:147503

2007-09-18 22:12 +0000 [r714203]  hedlund

	* branches/KDE/3.5/kdeedu/kwordquiz/src/kwordquizdoc.cpp: Fix
	  loading of remote files. BUG:147535

2007-09-18 22:22 +0000 [r714209]  hedlund

	* branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/kvt-core/kvoctraindoc.cpp:
	  Fix loading of remote files. BUG:147534

2007-09-19 10:56 +0000 [r714359]  gladhorn

	* branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/main.cpp: Update
	  about data.

2007-09-21 18:29 +0000 [r715287]  pino

	* branches/KDE/3.5/kdeedu/kig/objects/object_imp_factory.cc,
	  branches/KDE/3.5/kdeedu/kig/modes/construct_mode.cc,
	  branches/KDE/3.5/kdeedu/kig/misc/special_constructors.cc: fix
	  some memory leaks

2007-09-26 20:53 +0000 [r717408]  gladhorn

	* branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/kvt-core/kvoctraindoc.cpp:
	  Make search search in all columns. In KDE4 this has been fixed
	  for quite some time and search is overall much improved. BUG:
	  93449

2007-10-06 22:01 +0000 [r722206]  pino

	* branches/KDE/3.5/kdeedu/ktouch/keyboards/Makefile.am: install the
	  italian keyboard layout, as it seems more or less valid

2007-10-08 11:05 +0000 [r722964]  coolo

	* branches/KDE/3.5/kdeedu/kdeedu.lsm: updating lsm

