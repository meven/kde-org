---
title: Plasma 5.13.2 Complete Changelog
version: 5.13.2
hidden: true
plasma: true
type: fulllog
---

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- Simplify flatpak initialization process. <a href='https://commits.kde.org/discover/87e45bd58011b24e2ab7a1f8f4ae9e711bdea04f'>Commit.</a>
- Include the donation URL from KNS. <a href='https://commits.kde.org/discover/edeacba8db7a56bc4d0f053f10febffebcb3f683'>Commit.</a>
- When installing a resource with several payloads, fetch all of them. <a href='https://commits.kde.org/discover/ed54e6fbc5e73d7fc2dcbdec766e386d86c7e9b7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395720'>#395720</a>
- Improve how progress is processed when installing one application. <a href='https://commits.kde.org/discover/bc71b6874dd9c42af03a42cd9ead38d8f918e7c0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/391058'>#391058</a>
- PK: Don't show an error message when simulating. <a href='https://commits.kde.org/discover/09bf743ac3456e3d3e9c9a17e9353dfc12d30004'>Commit.</a>
- Fix warnings. <a href='https://commits.kde.org/discover/5e08b2f5337e3732f7d2d052874313f164559974'>Commit.</a>
- Fix crash. <a href='https://commits.kde.org/discover/30ce73bdcfd70b0514642786eba0c1973ab0e8cc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395676'>#395676</a>
- Properly remove the items from the FeaturedModel. <a href='https://commits.kde.org/discover/d20d2a8296e07db89b8662822a3402f4ba04b0af'>Commit.</a>
- Flatpak: Make sure we release every time we aquire. <a href='https://commits.kde.org/discover/538489ec680dc08d20fc00b1dc21763f5d891eac'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394558'>#394558</a>
- Fix there being more security updates than total updates in notifier. <a href='https://commits.kde.org/discover/9577e7d1d333d43d899e6eca76bfb309483eb29a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/392056'>#392056</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13596'>D13596</a>

### <a name='ksysguard' href='https://commits.kde.org/ksysguard'>KSysGuard</a>

- Fix leak of pipe FDs in MD RAID code. <a href='https://commits.kde.org/ksysguard/d0287c1dea39f5d3b8993ddfc38e483a048a4333'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378268'>#378268</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13664'>D13664</a>
- Explicitly include &lt;QDoubleValidator&gt; to fix build with Qt 5.11. <a href='https://commits.kde.org/ksysguard/093de8d449b47a34dc1b65a2245f93cdc8d62aa0'>Commit.</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- Fix tooltip woes. <a href='https://commits.kde.org/plasma-desktop/1e218b405bee4d75a5d26a4c28da2724d967953a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382571'>#382571</a>. Fixes bug <a href='https://bugs.kde.org/385947'>#385947</a>. Fixes bug <a href='https://bugs.kde.org/389469'>#389469</a>. Fixes bug <a href='https://bugs.kde.org/388749'>#388749</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13602'>D13602</a>
- Remove unused entry X-KDE-DBus-ModuleName from kded plugins' metadata. <a href='https://commits.kde.org/plasma-desktop/76b4bdae39c53da9cb2abf8043fbe14bdb0e7855'>Commit.</a>
- [kded kcm] Fix estimating dbusModuleName of kded plugins. <a href='https://commits.kde.org/plasma-desktop/6f491642e9b940d1f084b491408398a1df28027e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13623'>D13623</a>
- Set X-KDE-PluginInfo-Name=touchpad in KDED desktop file. <a href='https://commits.kde.org/plasma-desktop/f040cdb399b10df8a50c17ac13e4fd44693d3057'>Commit.</a>
- Revert "Touchpad KDED module: Convert to JSON metadata". <a href='https://commits.kde.org/plasma-desktop/3432c3342b1f8014cf59f39565aca53d280c7f86'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395622'>#395622</a>
- [Fonts KCM] disable "to" label when its companion controls are also disabled. <a href='https://commits.kde.org/plasma-desktop/27acf95acf61692ace2eff971b536ddc2ab3c22f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13611'>D13611</a>

### <a name='plasma-pa' href='https://commits.kde.org/plasma-pa'>Plasma Audio Volume Control</a>

- SinkModel: Ignore virtual sinks for preferredSink. <a href='https://commits.kde.org/plasma-pa/974bd408f719aeb7640b522d2813f314b5535306'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395466'>#395466</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13568'>D13568</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- Remove unused entry X-KDE-DBus-ModuleName from kded plugins' metadata. <a href='https://commits.kde.org/plasma-workspace/1894d888f726842d1e998c1db14b3dcce32d09ea'>Commit.</a>

### <a name='xdg-desktop-portal-kde' href='https://commits.kde.org/xdg-desktop-portal-kde'>xdg-desktop-portal-kde</a>

- Stop streaming when session is closed and allow some stream negotiation. <a href='https://commits.kde.org/xdg-desktop-portal-kde/eac0062220bb653a1d5be0f282f85e19abc12ad1'>Commit.</a>
