---
title: KDE Ships KDE Applications 15.12.1
description: KDE Ships KDE Applications 15.12.1
date: 2016-01-12
version: 15.12.1
---

{{% i18n_var "January 12, 2016. Today KDE released the first stability update for <a href='%[1]s'>KDE Applications 15.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../announce-applications-15.12.0" %}}

More than 30 recorded bugfixes include improvements to kdelibs, kdepim, kdenlive, marble, konsole, spectacle, akonadi, ark and umbrello, among others.

{{% i18n_var "This release also includes Long Term Support version of KDE Development Platform %[1]s." "4.14.16"%}}

{{% i18n_var "You can find the full list of changes <a href='%[1]s'>here</a>." "../fulllog_applications-$release" %}}

#### Spread the Word

{{% i18n_var "Non-technical contributors are an important part of KDE’s success. While proprietary software companies have huge advertising budgets for new software releases, KDE depends on people talking with other people. Even for those who are not software developers, there are many ways to support the KDE Applications %[1]s release. Report bugs. Encourage others to join the KDE Community. Or <a href='%[2]s'>support the nonprofit organization behind the KDE community</a>." "15.12.0" "https://relate.kde.org/civicrm/contribute/transact?reset=1&amp;id=5" %}}

#### {{% i18n_var "Installing KDE Applications %[1]s Binary Packages" "15.12.0" %}}

##### Packages

{{% i18n_var "Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications %[1]s for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks." "15.12.0" %}}

##### Package Locations

For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_Applications/Binary_Packages'>Community Wiki</a>.

#### {{% i18n_var "Compiling KDE Applications %[1]s" "15.12.0" %}}

{{% i18n_var "The complete source code for KDE Applications %[1]s may be <a href='%[2]s'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='%[3]s'>KDE Applications %[1]s Info Page</a>." "15.12.0" "http://download.kde.org/stable/applications/15.12.0/src/" "/info/applications-15.12.0.php" %}}

#### Supporting KDE

KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}
