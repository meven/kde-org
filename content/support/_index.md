---
title: "Support & Information"
---


<p>On this page, you can find different ways to find user support for KDE
software provided by the community.</p>

<div class="row">
<section class="col-md-6 col-12">
<h2>Documentation</h2>

<h3>Handbook</h3> 
<p><a href="https://docs.kde.org/">Program documentation</a> -
Documentation for many KDE applications.</p>
<a href="https://docs.kde.org/" class="learn-more button">Learn
more</a>

<h3>KDE for System Administrators</h3>
<p><a href="https://userbase.kde.org/KDE_System_Administration">
KDE for System Administrators</a> - Provides system administrators who
are deploying KDE software in their organisation with all the
information they need concerning KDE software.</p>
<a href="https://userbase.kde.org/KDE_System_Administration"
class="learn-more button">Learn more</a>

<h3>KDE Userbase</h3>
<p>Information on many KDE applications and products.
<a href="https://userbase.kde.org">Userbase</a> is the home for KDE
users and enthusiasts. It provides information for end users on how to
use KDE applications.</p>
<a href="https://userbase.kde.org/" class="learn-more button">Learn
more</a>
</section>

<section class="col-md-6 col-12">
<h2>Community Support</h2>

<p>These <a href="https://userbase.kde.org/Asking_Questions">tips on
asking questions</a> should help you to get useful answers.</p>

<h3>KDE Forums</h3>
<p>Chat with other KDE users who may be able to help</p>
<a href="https://forum.kde.org/" class="learn-more button">Learn more</a>

<h3>Chat Channels</h3>
<p>Community support is available in <a
href="https://webchat.kde.org/#/room/#kde:kde.org">the KDE Matrix
Room</a>. See the <a href="https://community.kde.org/Matrix#Rooms">List
of KDE Matrix Rooms</a> for more specific channels.</p>
<a href="https://community.kde.org/Matrix" class="learn-more button">
Learn more</a>

<h3>Mailing Lists</h3>
<p>Often a direct way to get in touch with the development community</p>
<a href="/support/mailinglists/"
class="learn-more button">Learn more</a>
</section>

<section class="col-md-6 col-12">
<h2>International</h2>
<h3>International Pages</h3>
<p>If you are interested in viewing the KDE Home page and other
attached pages in a language other than English, you can choose from
the list at <a href="international.php">KDE International</a>.</p>
<a href="/support/international.php"
class="button learn-more">Learn more</a>
</section>

<section class="col-md-6 col-12">
<h2>Security and Bug Reporting</h2>
<h3>Report an Issue or Bug</h4>
<p>Developers work best fixing bugs if accurate and detailed information
is provided</p>
<a href="https://community.kde.org/Get_Involved/Issue_Reporting"
class="learn-more button">Learn more</a>

<h3>Security Advisories</h3>
<p>Information for users and administrators with important security
information. It is highly advised you keep your software up-to-date.</p>
<a href="/info/security/" class="learn-more button">
Learn more</a></p>
</section>

<section class="col-md-6 col-12">
<h2>KDE e.V. Trusted IT Consulting Firms</h2>
<p>In KDE we take pride in being a free software community and having an
open, free and fair development process.</p>

<p>However, we also understand that sometimes companies and institutions
main priority is not learning the processes of our community and they
just want issue solved or a feature implemented.</p>

<p>For this reason, we are offering a list of consultants that have
expertise in dealing with the KDE Community and we know will help get
your project landed in KDE as best as possible.</p>

<a href="https://ev.kde.org/consultants/" class="learn-more button">
Learn more</a></p>
</section>
</div>

